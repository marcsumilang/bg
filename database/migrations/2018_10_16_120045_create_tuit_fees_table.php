<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuitFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuit_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course');
            $table->integer('year');
            $table->integer('semester');
            $table->integer('per_unit');
            $table->integer('cash_installment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuit_fees');
    }
}
