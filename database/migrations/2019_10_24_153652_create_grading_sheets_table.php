<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradingSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grading_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject_load_id'); 
            $table->string('student_id'); 
            $table->string('numerical'); 
            $table->string('remarks'); 
            $table->string('school_year'); 
            $table->string('semester'); 
            $table->string('status'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grading_sheets');
    }
}
