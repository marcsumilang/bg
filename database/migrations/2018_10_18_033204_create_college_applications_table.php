<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('college_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('application_status'); 
            $table->string('application_date');            
            $table->string('course');
            $table->integer('year');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('initial');
            $table->string('street');
            $table->string('barangay');
            $table->string('city');
            $table->string('province');
            $table->string('sex');
            $table->string('civil_status');
            $table->integer('contact');
            $table->string('email');
            $table->string('last_school');
            $table->string('year_graduated');
            $table->string('last_school_address');
            $table->string('father_name');
            $table->string('father_occupation');
            $table->string('father_address');
            $table->string('mother_name');
            $table->string('mother_occupation');
            $table->string('mother_address');
        
            $table->string('new_returnee_transferee');
            $table->string('regular_irregular');

            $table->string('requirements'); //complete or incomplete
            $table->text('picture');
            $table->text('card');
            $table->text('nso');
            $table->text('good_moral');
            $table->text('ncae');
            $table->text('interview_slip');
            
            $table->text('tor');
            $table->text('honorable_dismissal');
            $table->text('eog'); //evaluation of grades

            
            $table->text('clearance');
            $table->text('cog'); //copy of grade 

            $table->text('miscellaneous');
            $table->text('tuition'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('college_applications');
    }
}
