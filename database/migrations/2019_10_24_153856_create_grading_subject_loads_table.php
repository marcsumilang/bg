<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradingSubjectLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grading_subject_loads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject_id');
            $table->string('teacher_id');
            $table->string('section_id');
            $table->string('irreg_students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grading_subject_loads');
    }
}
