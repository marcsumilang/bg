<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradingStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grading_students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id'); 
            $table->string('last_name'); 
            $table->string('first_name'); 
            $table->string('middle_name'); 
            $table->string('regular'); 
            $table->string('section'); 
            $table->string('school_year'); 
            $table->string('semester'); 
            $table->string('active'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grading_students');
    }
}
