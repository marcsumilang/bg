<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradingSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grading_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course'); 
            $table->string('year'); 
            $table->string('section'); 
            $table->string('school_year'); 
            $table->string('semester'); 
            $table->text('students'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grading_sections');
    }
}
