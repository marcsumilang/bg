<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScholarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scholarships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_portal_id');
            $table->text('application_form');
            $table->text('card');
            $table->text('cor1');
            $table->text('cor2');
            $table->text('copy_id');
            $table->text('ref_id1');
            $table->text('ref_id2');

            $table->text('renewal_form');
            $table->text('letter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scholarships');
    }
}
