

require('./bootstrap');
require('./argon');

window.Vue = require('vue');
// import Vuetify from 'vuetify'; 
// Vue.use(Vuetify);
// import 'vuetify/dist/vuetify.min.css'; 



Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('vue-app', require('./components/subjects/index.vue'));
Vue.component('collegeapplication', require('./components/enrollment/college-application.vue'));
Vue.component('college-procedure', require('./components/enrollment/college-procedure.vue'));

//MIS
Vue.component('sys', require('./components/mis/sys.vue'));
Vue.component('verifyportal', require('./components/mis/verify-portal.vue'));

//Registrar
Vue.component('course', require('./components/registrar/course.vue'));
Vue.component('miscfee', require('./components/registrar/miscfee.vue'));
Vue.component('tuitfee', require('./components/registrar/tuitfee.vue'));
Vue.component('college-applications', require('./components/registrar/collegeApplication.vue'));
// Vue.component('enrollment-procedure', require('./components/registrar/enrollment-procedure.vue'));
Vue.component('view_misc_fee', require('./components/pages/view_misc_fee.vue'));
Vue.component('view_tuit_fee', require('./components/pages/view_tuit_fee.vue'));
Vue.component('view_subjects', require('./components/pages/view_subjects.vue'));

//students
Vue.component('sectioning', require('./components/student/sectioning.vue'));
Vue.component('students', require('./components/masterlist/students.vue'));
Vue.component('studentid', require('./components/mis/studentid.vue'));
// Vue.component('oncampus', require('./components/student/oncampus.vue'));


Vue.component('manage_student', require('./components/student/manage_student.vue'));



//instructors
Vue.component('instructor', require('./components/instructors/instructor.vue'));
Vue.component('criteria', require('./components/instructors/criteria.vue'));
Vue.component('grades', require('./components/instructors/grades.vue'));
//vpaf
Vue.component('room', require('./components/vpaf/room.vue'));

//vpaa
// Vue.component('scheduling', require('./components/vpaa/scheduling.vue'));
Vue.component('manage_subject_loads', require('./components/grading/manage_subject_loads.vue'));

//inventory
// Vue.component('books', require('./components/librarian/books.vue'));

const app = new Vue({
    el: '#app',
    app_url: 'http://localhost:14'
});
