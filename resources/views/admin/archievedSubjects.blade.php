@extends('multiauth::layouts.adminnav') 
@section('content')
    
<h2>Archieved Subjects</h2>

    <table class='table'>
      <tr>
        <th>Subject Code</th>
        <th>Title</th>
        <th>Units</th>
        <th>Description</th>
        <th>Prerequisite</th>
        <th>Date Archieved</th>
      </tr>
      @if ($subjects)
          @foreach ($subjects as $subject )
        <tr>

        <td>{{ $subject->code }}</td>
        <td>{{ $subject->title }}</td>
        <td>{{ $subject->units }}</td>
        <td>{{ $subject->description }}</td>
        <td>{{ $subject->prerequisite }}</td>
        <td>{{ $subject->updated_at }}</td>
        {{-- <td><button v-on:click="editSubject(subject)" class="btn btn-warning" >Edit</button></td>      
        <td><button v-on:click="deleteSubject(subject.id)" class="btn btn-danger" >Delete</button></td> --}}
      </tr>
    @endforeach
      @endif
        
      
    </table>
@endsection
