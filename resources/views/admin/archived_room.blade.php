@extends('multiauth::layouts.adminnav') 
@section('content')
    
<div id="app">

    <table class="table">
        <tr>
        <th>Room No</th>
        <th>Floor</th>
        <th>Building</th>
        <th>Description</th>
        <th>Date Archived</th>
        </tr>
    @foreach ($rooms as $room)
        <tr>
                <td>
                    {{$room->room_no}}
                </td>
                <td>
                    {{$room->floor}}
                </td>
                <td>
                    {{$room->building}}
                </td>
                <td>
                    {{$room->description}}
                </td>
                <td>
                    {{$room->updated_at}}
                </td>
        </tr>
        
    @endforeach
</table>
</div>


@endsection
