@extends('multiauth::layouts.adminnav') 
@section('content')
    
<div id="app">

    <table class="table">
        <tr>
        <th>Instructor ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Contacts</th>
        <th>Date Archived</th>
        </tr>
    @foreach ($instructors as $instructor)
        <tr>
                <td>
                    {{$instructor->instructors_id}}
                </td>
                <td>
                    {{$instructor->name}}
                </td>
                <td>
                    {{$instructor->email}}
                </td>
                <td>
                    {{$instructor->contact_admin}}
                </td>
                <td>
                    {{$instructor->updated_at}}
                </td>
        </tr>
        
    @endforeach
</table>
</div>


@endsection
