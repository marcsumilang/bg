@extends('multiauth::layouts.adminnav') 
@section('content')
    <div id="app">
            <table class="table table-hover">
                <thead class="bg-primary">
                        <th>
                            Name
                        </th>
                        <th>
                            Course/Year/Section
                        </th>
                        <th>
                            Email
                        </th>
                    <th>
                        Date
                    </th>
                    </thead>
           
        @if ($students)
            
        @foreach ($students as $item)
        <tr>
                <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                <td>{{ $item->course }} {{ $item->year }}{{ $item->section }}</td>
        <td><a href='view_application/{{ $item->email }}' class="" >{{ $item->email }}</a></td>
        <td>{{ $item->updated_at->diffForHumans() }}</td>
        </tr>
        @endforeach
        @endif    
    </table>
    </div>

@endsection
