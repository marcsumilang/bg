@extends('multiauth::layouts.adminnav') 
@section('content')
    <div id="app">
            <table class="table table-hover">
                <thead class="bg-primary">
                    <th>
                        Email
                    </th>
                    <th>
                        Date
                    </th>
                    </thead>
           
        @if ($students)
            
        @foreach ($students as $item)
        <tr>
        <td><a href='view_application/{{ $item->email }}' class="" >{{ $item->email }}</a></td>
        <td>{{ $item->updated_at->diffForHumans() }}</td>
        </tr>
        @endforeach
        @endif    
    </table>
    </div>

@endsection
