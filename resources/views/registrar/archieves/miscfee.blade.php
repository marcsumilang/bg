@extends('multiauth::layouts.adminnav') 
@section('content')
    
<h2>Archieved Miscallaneous Fees</h2>

    <table class='table'>
       <tr>
        <th>level</th>
        <th>name</th>
        <th>amount</th>
        <th>Date Archieved</th>
      </tr>
      @if ($misc_fees)
          @foreach ($misc_fees as $misc_fee )
        <tr>

        <td>{{ $misc_fee->level }}</td>
        <td>{{ $misc_fee->name }}</td>
        <td>{{ $misc_fee->amount }}</td>
        <td>{{ $misc_fee->updated_at }}</td>
        {{-- <td><button v-on:click="editSubject(subject)" class="btn btn-warning" >Edit</button></td>      
        <td><button v-on:click="deleteSubject(subject.id)" class="btn btn-danger" >Delete</button></td> --}}
      </tr>
    @endforeach
      @endif
      
    </table>
@endsection
