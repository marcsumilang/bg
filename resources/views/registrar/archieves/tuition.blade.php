@extends('multiauth::layouts.adminnav') 
@section('content')
    
<h2>Archieved Tuition Fees</h2>

    <table class='table'>
       <tr>
        <!-- <th>#</th> -->
        <th>Course</th>
        <th>Year</th>
        <th>Semester</th>
        <th>Per Unit</th>
        <th>Cash/Installment</th>
        <th>Date Archieved</th>
      </tr>
      @if ($tuit_fees)
          @foreach ($tuit_fees as $tuit_fee )
        <tr>

        <td>{{ $tuit_fee->course }}</td>
        <td>{{ $tuit_fee->year }}</td>
        <td>{{ $tuit_fee->semester }}</td>
        <td>{{ $tuit_fee->per_unit }}</td>
        <td>{{ $tuit_fee->cash_installment }}</td>
        <td>{{ $tuit_fee->updated_at }}</td>
        {{-- <td><button v-on:click="editSubject(subject)" class="btn btn-warning" >Edit</button></td>      
        <td><button v-on:click="deleteSubject(subject.id)" class="btn btn-danger" >Delete</button></td> --}}
      </tr>
    @endforeach
      @endif
      
    </table>
@endsection
