@extends('multiauth::layouts.adminnav') 
@section('content')
    <div>
        
  <h4> {{$sys->semester === '1' ? $sys->semester.'st': $sys->semester.'nd'}} Semester School Year {{$sys->start_year}} - {{$sys->end_year}} </h4>
  <div id='student'>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Student Info 
                        @if ($sys)
                            <b>{{ $sys->semester === 1 ? $sys->semester.'st' : $sys->semester.'nd' }} Semester &nbsp &nbsp &nbsp &nbsp School Year: {{ $sys->start_year }}-{{ $sys->end_year }} 
                                      </b>
                            @endif </div>
    
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                         @if($requirements)
                         <div class="row">                      
                            @if($requirements->picture != 'To be Submitted')
                            {{-- <div class="col-lg-2">  
                                <img src="/storage/requirements/thumbnail/{{$requirements->picture }}" style='width:100px;'>
                            </div> --}}
                            @endif
                            <div class="col-lg-5">   
                                @if($requirements->student_id != null)
                                <span><b>Student ID: {{$requirements->student_id}}</b></span><br>
                                @endif     
                                <span><b>{{$requirements->new_returnee_transferee}} Student ({{$requirements->regular_irregular}})</b></span><br>
                                <span><b>Enrollment Status: {{$requirements->application_status}} </b></span><br>
                                @if($requirements->application_status == 'Enrolled')
                                 <span><b>Date Enrolled: {{$requirements->updated_at}} </b></span><br>
                                @endif
                                <span><b>{{$requirements->first_name}} {{$requirements->initial}}. {{$requirements->last_name}}</b></span><br>
                                <span><b>{{$requirements->course}} {{$requirements->year}} {{$requirements->section == null ? '(No Section Yet)' : $requirements->section}}</b></span><br>
                                  
                                <span><b>Birthdate: {{$requirements->birthdate}}</b></span><br> 
                            </div>
                            <div class="col-lg-5">     
                                <span><b>Sex: {{$requirements->sex}} Civil Status: {{$requirements->civil_status}}</b></span><br>  
                                <span><b>Contacts: {{$requirements->contact}}</b></span><br>    
                                <span><b>Email: {{$requirements->email}}</b></span><br>               
                                <span><b>Address: {{$requirements->street}} {{$requirements->barangay}} {{$requirements->city}} {{$requirements->province}}</b></span><br>         
                            </div>
                         </div>
    
                         @endif
                    </div>
                </div>
            </div>
        </div> 
    <br>
    <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Other Information</div>
    
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                         @if($requirements)
                         <div class="row">
                            <div class="col-lg-12">   
                                <span><b>Last School Attended: {{$requirements->last_school}} </b></span><br>
                                <span><b>Year Graduated: {{$requirements->year_graduated}}</b></span><br>
                                <span><b>School Address: {{$requirements->last_school_address}}</b></span><br>
                            </div>
                         </div>
    
                         
                         @endif
                    </div>
                </div>
            </div>
        </div>
    
        
    <br>
    <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Parent's Information</div>
    
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                         @if($requirements)
                         <div class="row">
                            <div class="col-lg-6">     
                                <span><b>Father: {{$requirements->father_name}}</b></span><br>    
                                <span><b>Occupation: {{$requirements->father_occupation}}</b></span><br>
                                <span><b>Address: {{$requirements->father_address}}</b></span><br>               
                            </div>
                            <div class="col-lg-6">     
                                <span><b>Mother: {{$requirements->mother_name}}</b></span><br>    
                                <span><b>Occupation: {{$requirements->mother_occupation}}</b></span><br>
                                <span><b>Address: {{$requirements->mother_address}}</b></span><br>               
                            </div>
                         </div>
    
                         
                         @endif
                    </div>
                </div>
            </div>
        </div>
       
        <br>
        <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Guardian's Information</div>
        
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                             @if($requirements)
                             <div class="row">
                                <div class="col-lg-12">     
                                    <span><b>Guardian: {{$requirements->guardian_name}}</b></span><br>    
                                    <span><b>Occupation: {{$requirements->guardian_occupation}}</b></span><br>
                                    <span><b>Contacts: {{$requirements->guardian_contact}}</b></span><br>
                                    <span><b>Address: {{$requirements->guardian_address}}</b></span><br>               
                                </div>
                             </div>
        
                             
                             @endif
                        </div>
                    </div>
                </div>
            </div>

            @if ($requirements->new_returnee_transferee == "New")
            
            <br>
            <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Requirements</div>
            
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                 @if($requirements)
                                 <div class="row">
                                    <div class="col-lg-12">     
                                            <span><b>Picture: {{$requirements->picture == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>Card: {{$requirements->card == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>Nso: {{$requirements->nso == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>NCAE: {{$requirements->ncae == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>Envelope: {{$requirements->envelope == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>Interview Slip: {{$requirements->interview_slip == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                            <span><b>Tuition: {{$requirements->tuition == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br>   
                                            <span><b>Miscellaneous: {{$requirements->miscellaneous == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br>               
                                    </div>
                                 </div>
            
                                 
                                 @endif
                            </div>
                        </div>
                    </div>
                </div>
        @else
        <br>
        <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Requirements</div>
        
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                             @if($requirements)
                             <div class="row">
                                <div class="col-lg-12">     
                                        <span><b>Picture: {{$requirements->picture == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                        <span><b>clearance: {{$requirements->clearance == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                        <span><b>cog: {{$requirements->cog == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                        <span><b>eog: {{$requirements->eog == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br> 
                                        <span><b>Tuition: {{$requirements->tuition == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br>   
                                        <span><b>Miscellaneous: {{$requirements->miscellaneous == 1 ? 'Already Submitted': 'To be Submitted'}}</b></span><br>            
                                </div>
                             </div>
        
                             
                             @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
       
        
    </div>
    </div>

    <div id="app">  
        {{-- {{auth()->user()->email = $requirements->email}} --}}
        @if ($requirements->new_returnee_transferee == "New")
            
                    <application_form 
                    
                    :auth_user="{{ $requirements }}"
                >
            </application_form>
        @else
        <requirement_returnee 
        :auth_user="{{ $requirements }}"
    >
</requirement_returnee>
        @endif
              </div>
@endsection
