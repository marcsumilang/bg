@extends('layouts.studentnav')


 


@section('content')
@include('layouts.message')
<div id="app">
</div>
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="rigth">
                           
                        @if ($sys)
                        <b>{{ $sys->semester === 1 ? $sys->semester.'st' : $sys->semester.'nd' }} Semester &nbsp &nbsp &nbsp &nbsp School Year: {{ $sys->start_year }}-{{ $sys->end_year }} 
                                  </b>
                        @endif    
                        
                           
                        @if ($sys->semester == 2)
                                  <a class="btn btn-info" href="http://bulacanpolytechniccollege.org/enroll_again/{{Auth::user()->email}}">Enroll for the current Semester & School Year</a>
                        @endif  
                        </span>
                    </div>

                    <div class="card-body">
                            Student Portal Dashboard 
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--Card-->
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header bg-primary">
                        <span class="rigth">
                            My Schedules
                        </span>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive">
                            <tr>
                                <th>Subject</th>
                                <th>Instructor</th>
                                <th>Room</th>
                                <th>Day/s</th>
                                <th>Time</th>
                            </tr>
                            @if ($schedules)
                                @foreach ($schedules as $schedule)
                                    <tr>
                                    <td>{{$schedule->subject_code}} - {{$schedule->subject_description}}</td>
                                    <td>{{$schedule->instructor_name}}</td>
                                    <td>{{$schedule->room_building}} - {{$schedule->room_no}}{{$schedule->room_description}}</td>
                                    <td>{{$schedule->day}}</td>
                                    <td>{{$schedule->time}}</td>
                                    </tr>
                                @endforeach
                            @endif
                       
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
