<html lang="en">
<head>
<style>
/* table {
    counter-reset: rowNumber;
}

table tr {
    counter-increment: rowNumber;
}

table tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
} */
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2
}
</style>
</head>
<body>
    <center>
        <h2>Bulacan Polytechnic College</h2>
            <h4 style="margin-top:-12px;">Bulihan, City of Malolos Bulacan</h4>
        <h4 style="margin-top:-12px;">School Year {{$school_year}} {{$semester == 1 ? '1st': '2nd' }} Semester</h4>
    <br>
    <h3>Masterlist</h3>    
    <h1 style="margin-top:-12px;">{{$course}} {{$year}}{{$section}}</h1>
    <table class="table centered table-responsive">
        <tr>
        <th>#</th>
        <th>Fullname</th>
        </tr>  

        @foreach ($students as $student)
        <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{ $student->last_name . ', '. $student->first_name .' '. $student->initial . '.'}}</td>
  
    </tr>
        @endforeach
    </center>
    </body>

</html>