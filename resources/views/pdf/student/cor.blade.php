<html lang="en">
<head>
<style>
/* table {
    counter-reset: rowNumber;
}

table tr {
    counter-increment: rowNumber;
}

table tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
} */
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2
}
</style>
</head>
<body>
    <center>
        <h2>Bulacan Polytechnic College</h2>
            <h4 style="margin-top:-12px;">Bulihan, City of Malolos Bulacan</h4>
        <h4 style="margin-top:-12px;">School Year {{$school_year}} {{$semester == 1 ? '1st': '2nd' }} Semester</h4>
    <br>
    <h3>COR</h3>    
    <h1 style="margin-top:-12px;">{{$course}} {{$year}}{{$section}}</h1>
  
    </center>
    
<h2>Enrolled Subjects</h2>


@if ($course)
@if ($year)
@if ($semester)     
<h4>Course: {{ $course }} Year: {{ $year }} Semester: {{ $semester }}</h4>
@endif
@endif
@endif


<table class='table'>
 <tr>
   <th>Subject Code</th>
   <th>Title</th>
   <th>Units</th>
   <th>Description</th>
   <th>Prerequisite</th>
 </tr>
 @if ($mysubjects)
     @foreach ($mysubjects as $mysubject )
   <tr>
   {{-- <td>{{ subject.id }}</td> --}}
   {{-- <td>{{ $mysubject}}</td> --}}

   <td>{{ $mysubject->code }}</td>
   <td>{{ $mysubject->title }}</td>
   <td>{{ $mysubject->units }}</td>
   <td>{{ $mysubject->description }}</td>
   <td>{{ $mysubject->prerequisite }}</td>
   {{-- <td><button v-on:click="editSubject(subject)" class="btn btn-warning" >Edit</button></td>      
   <td><button v-on:click="deleteSubject(subject.id)" class="btn btn-danger" >Delete</button></td> --}}
 </tr>
@endforeach
   @if($amount)
     <tr>
       <td></td>
       <td></td>
       <td>Total Units: {{ $total_units }}</td>
       <td>Price Per Unit: {{ $per_unit }}</td>
       <td></td>
     </tr>
     <tr>
       <td></td>
       <td></td>
       <td>Total Tuition: P{{ $amount }} pesos</td>
       <td></td>
       <td></td>
     </tr>
   @endif
 @endif
   
 
</table>


    </body>

</html>