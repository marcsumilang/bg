<html lang="en">
<head>
<style>
/* table {
    counter-reset: rowNumber;
}

table tr {
    counter-increment: rowNumber;
}

table tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
} */
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2
}
</style>
</head>
<body>
    <center>
        {{-- <img src="http://bpcmis.org/img/BPC LOGO.png" style="width:100px;heigth:100px;" alt=""> --}}
        <h2>Bulacan Polytechnic College</h2>
            <h4 style="margin-top:-12px;">Bulihan, City of Malolos Bulacan</h4>
        <h4 style="margin-top:-12px;">School Year {{$school_year}} {{$semester == 1 ? '1st': '2nd' }} Semester</h4>
        
    </center>
    <br>
    <h3>Instructor Schedule</h3>    
    <h4>Name: {{ $instructor->name }} - Educational Attainment: {{ $instructor->educational_attainment }}</h4>
    <h4>Email: {{ $instructor->email }} Contact: {{ $instructor->contact_student }}</h4>
    <table class="table">
            <tr>
                <th>Subject</th>
                <th>Section</th>
                <th>Room</th>
                <th>Day/s</th>
                <th>Time</th>
            </tr>
        @foreach ($schedules as $schedule)
            <tr>
            <td>{{$schedule->subject_code}} - {{$schedule->subject_description}}</td>
            <td>{{$schedule->course}} - {{$schedule->year}}{{$schedule->section}}</td>
            <td>{{$schedule->room_building}} - {{$schedule->room_no}}{{$schedule->room_description}}</td>
            <td>{{$schedule->day}}</td>
            <td>{{$schedule->time}}</td>
            </tr>
        @endforeach
    </table>
    </body>

</html>