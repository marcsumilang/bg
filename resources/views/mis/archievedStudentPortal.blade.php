@extends('multiauth::layouts.adminnav') 
@section('content')
    
<h2>Archieved Student Portal</h2>

    <table class='table'>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Date Registered</th>
        <th>Date Archieved</th>
        <th></th>
      </tr>
      @if ($users)
          @foreach ($users as $user )
        <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->created_at->diffForHumans() }}</td>
        <td>{{ $user->updated_at->diffForHumans() }}</td>
        <td><a href='/admin/restore-user/{{ $user->id }}' class="btn btn-success" >Restore</a></td>
      </tr>
    @endforeach
      @endif
        
      
    </table>
@endsection
