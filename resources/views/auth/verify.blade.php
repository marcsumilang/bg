@extends('layouts.studentnav')

@section('content')
<div id="app"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@if (session('resent'))
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h3 >OR</h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">If You Are Not Recieving Any Email</div>

                    <div class="card-body">
                        Click Here to notify the admin of your  Portal Verification.
                        <a class='btn btn-primary' href="#">Notify</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Message</div>

                    <div class="card-body">
                       Wait For The Verification Of your Email. (Could last for 24 hours to verify you email)
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
