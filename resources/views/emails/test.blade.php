<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Success Enrollment Email</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <center>
    <img src="{{ asset('css/app.css') }}" style="height:100px; width:100px;" alt="BPC LOGO">    
    <h1>Bulacan Polytechnic College</h1>
    </center>
    <p> We are pleased to inform you that you are now enrolled. <br>
        You can Access your portal at <a class="btn brn-success" href="https://bulacanpolytechniccollege.org/login">Student Portal</a>
    </p>

   <b>  From:</b> Registrar

    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>