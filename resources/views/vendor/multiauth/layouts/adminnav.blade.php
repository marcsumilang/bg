<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>{{ config('app.name', 'BPC MIS') }}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css"> --}}
    
    @yield('styles')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper" id="">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">

    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>

    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        {{-- <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button> --}}
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      {{-- <img src="./img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Lara Start</span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
       
          {{-- <img src="./img/profile.png" class="img-circle elevation-2" alt="User Image"> --}}
        </div>
        <div class="info">
          
          <a href="/admin/home" class="d-block">
          <i class="nav-icon fas fa-user orange"></i>
              {{ auth('admin')->user()->name }}
          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->


               @admin('vpaf')
          
               <li class="nav-item">
               <a href="/admin/home" class="nav-link">
                   <i class="nav-icon fas fa-tachometer-alt blue"></i>
                   <p>
                   VPAF Dashboard
   
                   </p>
               </a>
               </li>
             <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                 <i class="nav-icon fa fa-pen green"></i>
                 <p>
                   Manage
                   <i class="right fa fa-angle-left"></i>
                 </p>
               </a>            
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/instructors" class="nav-link">
                      <i class="fas fa-users nav-icon"></i>
                      <p>Instructors</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/rooms" class="nav-link">
                      <i class="fas fa-home  nav-icon"></i>
                      <p>Rooms</p>
                    </a>
                  </li>
               </ul>
             </li>
             <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                 <i class="nav-icon fa fa-copy green"></i>
                 <p>
                   View Archieved
                   <i class="right fa fa-angle-left"></i>
                 </p>
               </a>            
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/admin/instructors/archived" class="nav-link">
                      <i class="fas fa-copy nav-icon"></i>
                      <p>Instructors</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/admin/rooms/archived" class="nav-link">
                      <i class="fas fa-copy nav-icon"></i>
                      <p>Rooms</p>
                    </a>
                  </li>
               </ul>
               
             @endadmin
    
              
          @admin('vpaa')
          
            <li class="nav-item">
            <a href="/admin/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                VPAA Dashboard

                </p>
            </a>
            </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pen green"></i>
              <p>
                Manage
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/admin/manage_student" class="nav-link">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Students</p>
                </a>
                <li class="nav-item">
                  <a href="{{ route('admin.subjects') }}" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>Subjects</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/admin/subject_loads" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>Subject Loads</p>
                  </a>
                </li>
                {{-- <li class="nav-item">
                  <a href="/admin/schedules" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>Schedules</p>
                  </a>
                </li> --}}
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-copy green"></i>
              <p>
                View Archieved
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.archievedSubjects') }}" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Subjects</p>
                </a>
              </li>
            </ul>
            
          @endadmin
    
          @admin('mis')
          
            <li class="nav-item">
            <a href="/admin/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                MIS Dashboard

                </p>
            </a>
            </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pen green"></i>
              <p>
                Manage
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('mis.sys') }}" class="nav-link">
                  <i class="fas fa-calendar-alt nav-icon"></i>
                  <p>School Year</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('mis.verify-portal') }}" class="nav-link">
                  <i class="fas fa-check nav-icon"></i>
                  <p>Verify Portal</p>
                </a>
              </li>
              <li class="nav-item">
                    <a href="/admin/students-id" class="nav-link">
                        <i class="nav-icon fas fa-eye"></i>
                        <p>
                            Students ID
                        </p>
                    </a>
             </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-copy green"></i>
              <p>
                View Archieved
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>           
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('mis.archievedStudentPortal') }}" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Student Portal</p>
                </a>
              </li>
            </ul>
          </li>
          
          @endadmin

          @admin('registrar')
          
            <li class="nav-item">
            <a href="/admin/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                Registrar Dashboard

                </p>
            </a>
            </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pen green"></i>
              <p>
                Manage
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('registrar.enrollmentProcedure') }}" class="nav-link">
                  <i class="fas fa-project-diagram nav-icon"></i>
                  <p>Enrollment Procedure</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/courses" class="nav-link">
                  <i class="fas fa-project-diagram nav-icon"></i>
                  <p>Courses</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('registrar.tuitfee') }}" class="nav-link">
                  <i class="fas fa-money-bill-alt nav-icon"></i>
                  <p>Tuition Fees</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('registrar.miscfee') }}" class="nav-link">
                  <i class="fas fa-money-bill-alt nav-icon"></i>
                  <p>Miscellaneous Fees</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/application_forms" class="nav-link">
                  <i class="fas fa-address-book nav-icon"></i>
                  <p>College Application</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/enrolled_students" class="nav-link">
                  <i class="fas fa-address-book nav-icon"></i>
                  <p>Errolled Students</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-copy green"></i>
              <p>
                View Archieved
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/admin/courses/archive" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Courses</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.archievedTuitions') }}" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Tuition Fee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.archievedMiscFee') }}" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Miscellaneous Fee</p>
                </a>
              </li>
            </ul>
          @endadmin
 
          @admin('scholarship')
          
            <li class="nav-item">
            <a href="/admin/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                Scholarship Dashboard

                </p>
            </a>
            </li>
         
          @endadmin

          @admin('inventory-custodian')
          
            <li class="nav-item">
            <a href="/admin/home" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                Inventory Custodian Dashboard

                </p>
            </a>
            </li>
         
          <li class="nav-item">
                <a href="/admin/manage_inventory" class="nav-link">
                    <i class="nav-icon fas fa-eye"></i>
                    <p>
                       Manage Inventory
                    </p>
                </a>
         </li>
          @endadmin
          @admin('librarian')
          <li class="nav-item">
             <a href="/admin/home" class="nav-link">
                 <i class="nav-icon fas fa-tachometer-alt blue"></i>
                 <p>
                 Librarian Dashboard
 
                 </p>
             </a>
             </li>
             <li class="nav-item">
                 <a href="/admin/home" class="nav-link">
                     <i class="nav-icon fas fa-tachometer-alt blue"></i>
                     <p>
                     Search Book
     
                     </p>
                 </a>
                 </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pen green"></i>
              <p>
                Manage
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>            
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/admin/books" class="nav-link">
                    <i class="fas fa-address-book nav-icon"></i>
                    <p>Penalty</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/admin/books" class="nav-link">
                    <i class="fas fa-address-book nav-icon"></i>
                    <p>Books</p>
                  </a>
                </li>
            </ul>
          </li>
          
          <li class="nav-item">
              <a href="/admin/home" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt blue"></i>
                  <p>
                  Lend Book
  
                  </p>
              </a>
              </li>
              <li class="nav-item">
                 <a href="/admin/home" class="nav-link">
                     <i class="nav-icon fas fa-tachometer-alt blue"></i>
                     <p>
                     Return Book
     
                     </p>
                 </a>
                 </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-copy green"></i>
              <p>
                View Archieved
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>           
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/admin/books/archived" class="nav-link">
                  <i class="fas fa-copy nav-icon"></i>
                  <p>Archived Books</p>
                </a>
              </li>
            </ul>
          </li>
          
          @endadmin
    


          <li class="nav-item">
                <a href="/admin/masterlist" class="nav-link">
                    <i class="nav-icon fas fa-user orange"></i>
                    <p>
                        Masterlist
                    </p>
                </a>
         </li>

          <li class="nav-item">
                <a href="/admin/change-pass" class="nav-link">
                    <i class="nav-icon fas fa-unlock-alt"></i>
                    <p>
                        Change Password
                    </p>
                </a>
         </li>


          <li class="nav-item">
                <a class="nav-link" href="/admin/logout"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                    <i class="nav-icon fa fa-power-off red"></i>
                    <p>
                        {{ __('Logout') }}
                    </p>
                 </a>

             <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
             <input type="text" name="admin_id" value="{{auth('admin')->user()->id}}">
                    @csrf
                </form>
        </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
     <main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
                
        @yield('content')
        </main>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018-2019 <a href="https://bulacanpolytechniccollege.org">BPC MIS</a>.</strong> All rights reserved.
  
    <strong>About the developers<a href="https://bulacanpolytechniccollege.org/about-developers">TEAMCAPSTONE</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->
        <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jscolor.js') }}"></script>
    @yield('scripts')
</body>
</html>
