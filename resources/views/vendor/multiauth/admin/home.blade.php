

    @extends('multiauth::layouts.adminnav') 
    @section('content')
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ ucfirst(config('multiauth.prefix')) }} Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif You are logged in to {{ config('multiauth.prefix') }} side!
                        <div id="app"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection



   
