

    @extends('multiauth::layouts.superadminnav') 
    @section('content')
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Super Admin Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif You are logged in to {{ config('multiauth.prefix') }} side!
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection



   
