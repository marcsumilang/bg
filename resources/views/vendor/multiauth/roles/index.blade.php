@extends('multiauth::layouts.superadminnav') 
@section('content')
<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-10">
            <div class="card">
                <div class="card-header bg-info text-white">
                    Roles
                    <span class="float-right">
                        {{-- <a href="{{ route('admin.role.create') }}" class="btn btn-sm btn-success">New Role</a> --}}
                    </span>
                </div>

                <div class="card-body">
                        @include('multiauth::message')
                    <ol class="list-group">
                        @foreach ($roles as $role)
                        <li class="list-group-item  justify-content-center">
                        <div class="row">
                            <div class="col-lg-5">{{ $role->name }}</div>
                            <div class="col-lg-5"><span class="badge badge-primary badge-pill">{{ $role->admins->count() }} {{ ucfirst(config('multiauth.prefix')) }}</span></div>
                            <div class="col-lg-4">
                            <div class="float-right">

                                {{-- @if(($role->admins->count()) == 0)
                                    <a href="" class="btn btn-sm btn-secondary mr-3" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $role->id }}').submit();">Delete</a>
                                    <form id="delete-form-{{ $role->id }}" action="{{ route('admin.role.delete',$role->id) }}" method="POST" style="display: none;">
                                        @csrf @method('delete')
                                    </form>
                                    <a href="{{ route('admin.role.edit',$role->id) }}" class="btn btn-sm btn-primary mr-3">Edit</a>
                                @endif --}}

                                </div>
                            </div>
                        </div>
                            
                            
                            
                        </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection