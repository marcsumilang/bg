@extends('layouts.studentnav') 
@section('content')

<div class="card">
  <div class="card-header">
      Submitted Requirements
      <span class="float-right">
          {{-- <a href="{{route('admin.register')}}" class="btn btn-sm btn-success">New {{ ucfirst(config('multiauth.prefix')) }}</a> --}}
      </span>
  </div>
  <div class="card-body">
  <ul class="list-group">  
      @if($requirements->picture != null)
      <li class="list-group-item  justify-content-between align-items-center">  
      <img src="/storage/requirements/thumbnail/{{$requirements->picture }}" style='width:100px;'>
      <span><b>1x1</b></span>
      </li>
      @endif
      @if($requirements->card != null)
      <li class="list-group-item  justify-content-between align-items-center">  
      <img src="/storage/requirements/thumbnail/{{$requirements->card }}" style='width:100px;'>
      <span><b>High School Card</b></span>
      </li>
      @endif
      @if($requirements->nso != null)
      <li class="list-group-item  justify-content-between align-items-center">  
      <img src="/storage/requirements/thumbnail/{{$requirements->nso }}" style='width:100px;'>
      <span><b>BC-NSO/PSA Authenticated</b></span>
      </li>
      @endif
      @if($requirements->good_moral != null)
      <li class="list-group-item  justify-content-between align-items-center">  
      <img src="/storage/requirements/thumbnail/{{$requirements->good_moral }}" style='width:100px;'>
      <span><b>Good Moral</b></span>
      </li>
      @endif
      @if($requirements->ncae != null)
      <li class="list-group-item  justify-content-between align-items-center">
      <img src="/storage/requirements/thumbnail/{{$requirements->ncae }}" style='width:100px;'>
      <span><b>National Career Assessment Exam</b></span>
      </li>
      @endif
      @if($requirements->interview_slip != null)
      <li class="list-group-item  justify-content-between align-items-center">  
      <img src="/storage/requirements/thumbnail/{{$requirements->interview_slip }}" style='width:100px;'>
      <span><b>Interview Slip</b></span>
      </li>
      @endif
      @if($requirements->tor != null)
      <li class="list-group-item  justify-content-between align-items-center"> 
      <img src="/storage/requirements/thumbnail/{{$requirements->tor }}" style='width:100px;'>
      <span><b>Transcript of Records</b></span>
      </li>
      @endif
      @if($requirements->honorable_dismissal != null)
      <li class="list-group-item  justify-content-between align-items-center"> 
      <img src="/storage/requirements/thumbnail/{{$requirements->honorable_dismissal }}" style='width:100px;'>
      <span><b>Honorable Dismissal</b></span>
      </li>
      @endif
      @if($requirements->eog != null)
      <li class="list-group-item  justify-content-between align-items-center"> 
      <img src="/storage/requirements/thumbnail/{{$requirements->eog }}" style='width:100px;'>
      <span><b>Evaluation of Grades</b></span>
      </li>
      @endif
      @if($requirements->clearance != null)
      <li class="list-group-item  justify-content-between align-items-center"> 
      <img src="/storage/requirements/thumbnail/{{$requirements->clearance }}" style='width:100px;'>
      <span><b>Clearance</b></span>
      </li>
      @endif
      @if($requirements->cog != null)
      <li class="list-group-item  justify-content-between align-items-center"> 
      <img src="/storage/requirements/thumbnail/{{$requirements->cog }}" style='width:100px;'>
      <span><b>Certificate of Grades</b></span>
      </li>
      @endif
</div></div>
<br><br>

@if($requirements->new_returnee_transferee == 'New')
<div class="row"><br>
<h4>High School Graduates Requirements Only</h4>
</div>
@endif
@if($requirements->new_returnee_transferee == 'Transferee')
<div class="row"><br>
<h4>Transferee Requirements Only</h4>
</div>
@endif
@if($requirements->new_returnee_transferee == 'Returnee')
<div class="row"><br>
<h4>Returnee Requirements Only</h4>
</div>
@endif

<form class="" style='' action="{{ url('api/student/requirement') }}" method="post" enctype="multipart/form-data">
{{-- {{ method_field('PUT') }} --}}
        <input type="text" value='{{ Auth::user()->id }}' name="student_portal_id" id="student_portal_id" hidden>





<div class="card">
  <div class="card-header">
      List
      <span class="float-right">
          {{-- <a href="{{route('admin.register')}}" class="btn btn-sm btn-success">New {{ ucfirst(config('multiauth.prefix')) }}</a> --}}
      </span>
  </div>
  <div class="card-body">
  
  <ul class="list-group">  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="picture">1x1 Picture</label></div>
              <div class="col-lg-4"><input id="picture" type="file" name="picture"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>
          @if($requirements->new_returnee_transferee == 'New')  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="card">High School Card</label></div>
              <div class="col-lg-4"><input id="card" type="file" name="card"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="nso">BC-NSO/PSA Authenticated</label></div>
              <div class="col-lg-4"><input id="nso" type="file" name="nso"></div>
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="good_moral">Good Moral</label></div>
              <div class="col-lg-4"><input id="good_moral" type="file" name="good_moral">  </div>  
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="ncae">National Career Assessment Exam</label></div>
              <div class="col-lg-4"><input id="ncae" type="file" name="ncae"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="interview_slip">Interview Slip</label></div>
              <div class="col-lg-4"><input id="interview_slip" type="file" name="interview_slip"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>
        @endif
        


          @if($requirements->new_returnee_transferee == 'Transferee')
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="tor">Transcript of Records</label></div>
              <div class="col-lg-4"><input id="tor" type="file" name="tor"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="honorable_dismissal">Honorable Dismissal</label></div>
              <div class="col-lg-4"><input id="honorable_dismissal" type="file" name="honorable_dismissal"></div>
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="eog">Evaluation of Grades</label></div>
              <div class="col-lg-4"><input id="eog" type="file" name="eog"></div>  
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li> 
        @endif

        
          @if($requirements->new_returnee_transferee == 'Returnee')
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="clearance">Students Clearance</label></div>
              <div class="col-lg-4"><input id="clearance" type="file" name="clearance"></div>    
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li>  
          <li class="list-group-item  justify-content-between align-items-center">  
            <div class="row">            
              <div class="col-lg-4"><label for="cog">Copy of Grades</label></div>
              <div class="col-lg-4"><input id="cog" type="file" name="cog"></div>
              <div class="col-lg-4 float-right"> {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Submit</button></div>            
            </div>   
          </li> 
        @endif
   </ul>
</form>
</div>
</div>

@endsection