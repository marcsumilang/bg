@extends('layouts.studentnav') 
@section('content')

@include('layouts.message')
{{-- <td>{{ $message }}</td> --}}

<h2>Enrolled Subjects</h2>


     @if ($course)
     @if ($year)
     @if ($semester)     
    <h4>Course: {{ $course }} Year: {{ $year }} Semester: {{ $semester }}</h4>
     @endif
     @endif
     @endif
  

    <table class='table'>
      <tr>
        <th>Subject Code</th>
        <th>Title</th>
        <th>Units</th>
        <th>Description</th>
        <th>Prerequisite</th>
      </tr>
      @if ($mysubjects)
          @foreach ($mysubjects as $mysubject )
        <tr>
        {{-- <td>{{ subject.id }}</td> --}}
        {{-- <td>{{ $mysubject}}</td> --}}

        <td>{{ $mysubject->code }}</td>
        <td>{{ $mysubject->title }}</td>
        <td>{{ $mysubject->units }}</td>
        <td>{{ $mysubject->description }}</td>
        <td>{{ $mysubject->prerequisite }}</td>
        {{-- <td><button v-on:click="editSubject(subject)" class="btn btn-warning" >Edit</button></td>      
        <td><button v-on:click="deleteSubject(subject.id)" class="btn btn-danger" >Delete</button></td> --}}
      </tr>
    @endforeach
        @if($amount)
          <tr>
            <td></td>
            <td></td>
            <td>Total Units: {{ $total_units }}</td>
            <td>Price Per Unit: {{ $per_unit }}</td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td>Total Tuition: P{{ $amount }} pesos</td>
            <td></td>
            <td></td>
          </tr>
        @endif
      @endif
        
      
    </table>

    <div id="app">

    

  

        
    </div>

@endsection
{{-- http://bpc.org/enrollment/college/application-form --}}