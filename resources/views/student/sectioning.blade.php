@extends('layouts.studentnav') 
@section('content')

<div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <span class="rigth">
                        @if ($sys)
                        <b>{{ $sys->semester === '1' ? $sys->semester.'st' : $sys->semester.'nd' }} Semester &nbsp &nbsp &nbsp &nbsp School Year: {{ $sys->start_year }}-{{ $sys->end_year }} 
                                  </b>
                        @endif        
                    </span>
                </div>

                <div id="app" class="card-body">
                        <sectioning 
                        @if(auth()->check())
                                :auth_user="{{ auth()->user() }}"
                            @endif
                            > </sectioning>
                </div>
            </div>
        </div>
    </div>

@endsection
{{-- http://bpc.org/enrollment/college/application-form --}}