@extends('layouts.studentnav') 
@section('content')


    <div id="app">
    {{-- @include('layouts.message') --}}
        <collegeapplication 
          @if(auth()->check())
                :auth_user="{{ auth()->user() }}"
            @endif
            > </collegeapplication>
    </div>

@endsection
{{-- http://bpc.org/enrollment/college/application-form --}}