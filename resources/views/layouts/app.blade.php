<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Bulacan Polytechnic College - Your Partner to Reach the World</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Favicon -->
        {{-- <link href="{{ asset('img/brand/favicon') }}" rel="icon" type="image/png"> --}}
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <link rel="shortcut icon" href="{{ asset('/Bulacan Polytechnic College icon.ico') }}">

        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('/css/argon.css?v=1.0.1') }}" rel="stylesheet">
        <link href="{{ asset('lib/animate/animate.css') }}" rel="stylesheet">
        <!-- Docs CSS -->
        {{-- <link type="text/css" href="{{ asset('/css/docs.min.css') }}" rel="stylesheet"> --}}
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138541223-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-138541223-1');
</script>
@yield('styles')
</head>

<body>
  <div id="app"></div>
  <header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
      <div class="container">
        <a class="navbar-brand mr-lg-5" href="/" style="font-size:50px;">
          <img src="{{ asset('/Bulacan Polytechnic College icon.ico') }}" style="width:50px; height:50px;" alt="">
          BPC
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar_global">
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
            <li class="nav-item dropdown">
              <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
                <i class="ni ni-ui-04 d-lg-none"></i>
                <span class="nav-link-inner--text">About </span> <i class="fa fa-caret-down"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl">
                <div class="dropdown-menu-inner">
                  <a href="enrollment/college" class="media d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
                      <i class="ni ni-spaceship"></i>
                    </div>
                    <div class="media-body ml-3">
                      <h6 class="heading text-primary mb-md-1">Enrollement Procedure</h6>
                      <p class="description d-none d-md-inline-block mb-0">About the procedures of Enrollement and requirements</p>
                    </div>
                  </a>
                  {{-- <a href="about-developers" class="media d-flex align-items-center">
                    <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                      <i class="ni ni-palette"></i>
                    </div>
                    <div class="media-body ml-3">
                      <h6 class="heading text-primary mb-md-1">About </h6> 
                      <p class="description d-none d-md-inline-block mb-0">About the developers</p>
                    </div>
                  </a> --}}
                </div>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
                <i class="ni ni-collection d-lg-none"></i>
                <span class="nav-link-inner--text">Services </span> <i class="fa fa-caret-down"></i>
              </a>
              <div class="dropdown-menu">
                <a href="/login" class="dropdown-item">Student Portal</a>
                <a href="/register" class="dropdown-item">Portal Registration</a>
                <a href="/admin" class="dropdown-item">Admin Portal</a>
                <a href="/instructors/login" class="dropdown-item">Instructors Portal</a>
                {{-- <a href="../examples/register.html" class="dropdown-item">Schedules</a> --}}
              </div>
            </li>
          </ul>
          <ul class="navbar-nav align-items-lg-center ml-lg-auto">
            <li class="nav-item">
              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Like us on Facebook">
                <i class="fa fa-facebook-square"></i>
                <span class="nav-link-inner--text d-lg-none">Facebook</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Follow us on Twitter">
                <i class="fa fa-twitter-square"></i>
                <span class="nav-link-inner--text d-lg-none">Twitter</span>
              </a>
            </li>
            <li class="nav-item d-none d-lg-block ml-lg-4">
              <a href="/register" target="_blank" class="btn btn-neutral btn-icon">
                <span class="btn-inner--icon">
                  <i class="fa fa-cloud-download mr-2"></i>
                </span>
                <span class="nav-link-inner--text">Register</span>
              </a>
            </li>
          </ul>
          
          {{-- <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">Portal</span>
            </a>
            <div class="dropdown-menu">
              <a href="/login" class="dropdown-item">Login</a>
              <a href="/register" class="dropdown-item">Register</a>
            </div>
          </li> --}}
        </div>
      </div>
    </nav>
  </header>
 
  @yield('content')
    

  <footer class="footer has-cards">
    {{-- <div class="container container-lg">
      <div class="row">
        <div class="col-md-6 mb-5 mb-md-0">
          <div class="card card-lift--hover shadow border-0">
            <a href="../examples/landing.html" title="Landing Page">
              <img src="{{ asset('img/theme/landing.jpg') }}" class="card-img">
            </a>
          </div>
        </div>
        <div class="col-md-6 mb-5 mb-lg-0">
          <div class="card card-lift--hover shadow border-0">
            <a href="../examples/profile.html" title="Profile Page">
              <img src="{{ asset('img/theme/profile.jpg') }}" class="card-img">
            </a>
          </div>
        </div>
      </div>
    </div> --}}
    <div class="container">
      <div class="row row-grid align-items-center my-md">
        <div class="col-lg-3">
          <img src="{{ asset('Bulacan Polytechnic College Logo.png') }}" style="width:150px; height:150px;" alt="">
        </div>
        <div class="col-lg-6">
          <h3 class="text-primary font-weight-light mb-2"> Bulacan Polytechnic College </h3>
          <h4 class="mb-0 font-weight-light">Your Partner to Reach the World</h4>
        </div>
        <div class="col-lg-3 text-lg-center btn-wrapper">
          <a target="_blank" href="/" class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
            <i class="fa fa-twitter"></i>
          </a>
          <a target="_blank" href="/" class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg" data-toggle="tooltip" data-original-title="Like us">
            <i class="fa fa-facebook-square"></i>
          </a>
        </div>
      </div>
      <hr>
      <div class="row align-items-center justify-content-md-between">
        <div class="col-md-6">
          <div class="copyright">
            &copy; 2019
            <a href="https://bulacanpolytechniccollege.org" target="_blank">Bulacan Polytechnic College</a>.
          </div>
        </div>
        <div class="col-md-6">
          <ul class="nav nav-footer justify-content-end">
            <li class="nav-item">
              <a href="/about-developers" class="nav-link" target="_blank">About Us</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
          
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('vendor/headroom/headroom.min.js') }}"></script>
</body>

</html>