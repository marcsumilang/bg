@if (session()->has('message'))
    {{-- <div class="alert alert-success">{{ session()->get('message') }}</div> --}}

    <div class="alert alert-dismissible alert-success white">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4 class="alert-heading">Success!</h4>
    <p class="mb-0">{{ session()->get('message') }}</p>
    </div>
@endif

@if ($errors->count() > 0)
    @foreach ($errors->all() as $error)
        {{-- <div class="alert alert-danger">{{ $error }}</div> --}}

    <div class="alert alert-dismissible alert-danger white">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4 class="alert-heading">Error!</h4>
    <p class="mb-0">{{ $error }}</p>
    </div>
    @endforeach
@endif

@if (session()->has('error'))
    {{-- <div class="alert alert-danger">{{ session()->get('error') }}</div> --}}

    <div class="alert alert-dismissible alert-danger white">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4 class="alert-heading">Error!</h4>
    <p class="mb-0">{{ session()->get('error') }}</p>
    </div>
@endif


@if (session()->has('warning'))
    {{-- <div class="alert alert-warning">{{ session()->get('warning') }}</div> --}}
    
<div class="alert alert-dismissible alert-warning white">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Warning!</h4>
  <p class="mb-0">{{ session()->get('warning') }}</p>
</div>
@endif