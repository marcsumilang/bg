  @extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <h1>Bulacan Polytechnic College</h1>
                  <hr>
                  <h3>Your Partner to Reach the World</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
