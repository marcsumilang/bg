@extends('layouts.nav_instructor')


 


@section('content')
@include('layouts.message')
<div id="app">
</div>
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="rigth">
                            My DTR
                        </span>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Month/Day/Year</th>
                                <th>Time In</th>
                                <th>Time Out</th>
                            </tr>
                        @foreach ($schedules as $schedule)
                            <tr>
                            <td>{{$schedule->month}}/{{$schedule->day}}/{{$schedule->year}}</td>
                            <td>{{$schedule->time_in}}</td>
                            <td>{{$schedule->time_out}}</td>
                            </tr>
                        @endforeach
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
