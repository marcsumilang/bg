@extends('layouts.nav_instructor')


 


@section('content')
@include('layouts.message')
<div id="app">
</div>
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="rigth">
                           
                        @if ($sys)
                        <b>{{ $sys->semester === 1 ? $sys->semester.'st' : $sys->semester.'nd' }} Semester &nbsp &nbsp &nbsp &nbsp School Year: {{ $sys->start_year }}-{{ $sys->end_year }} 
                                  </b>
                        @endif     
                        </span>
                    </div>

                    <div class="card-body">
                        
<?php
// session_start();
echo  $_SESSION['email'];
?>
                            Instructors Portal Dashboard 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <span class="rigth">
                            My Loads
                        </span>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Final Grade</th>
                                {{-- <th>Room</th>
                                <th>Day/s</th>
                                <th>Time</th> --}}
                            </tr>

                            @if ($students)
                                @foreach ($students as $student)
                                    <tr>
                                        <form action="" method="post"></form>
                                    <td><a href="/gradesheet/{{$student['id']}}"> {{$student['last_name']}}, {{$student['first_name']}} {{$student['middle_name']}}</a></td>
                                    <td><input name="{{$student['id']}}" disabled type="number"></td>
                                    {{-- <td><input name="submit" type="submit" value="submit"></td> --}}
                                    {{-- <td>{{$schedule->room_building}} - {{$schedule->room_no}}{{$schedule->room_description}}</td>
                                    <td>{{$schedule->day}}</td>
                                    <td>{{$schedule->time}}</td> --}}
                                    </tr>
                                @endforeach
                            @endif
                        
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
