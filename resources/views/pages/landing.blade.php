@extends('layouts.app') 
@section('styles')

@endsection
@section('content')
<main>
  <div class="position-relative">
    <!-- shape Hero -->
    <section class="section section-lg section-shaped pb-250">
      <div class="shape shape-style-1 shape-default">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="container py-lg-md d-flex">
        <div class="col px-0">
          <div class="row">
            <div class="col-lg-7">
              <h1 class="display-3  wow fadeInUp text-white">Bulacan Polytechnic College
                <span>Your Partner to Reach the World</span>
              </h1>
              <p class="lead  text-wite" style="color:whitesmoke;">Equip Students with the necessary technological & intelectual capacity to face fast changing demands of modern Technology. </p>
              <div class="btn-wrapper  wow fadeInUp"  data-wow-delay="0.2s">
                <a href="#contacts" class="btn btn-info btn-icon mb-3 mb-sm-0">
                  <span class="btn-inner--icon"><i class="fa fa-code"></i></span>
                  <span class="btn-inner--text">Contacts</span>
                </a>
                <a href="enrollment/college" class="btn btn-white btn-icon mb-3 mb-sm-0">
                  <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                  <span class="btn-inner--text">Enrollment</span>
                </a>
              </div>
            </div>
            <div class="col-lg-5">
               
            </div>
          </div>
        </div>
      </div>
      <!-- SVG separator -->
      <div class="separator separator-bottom separator-skew">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </section>
    <!-- 1st Hero Variation -->
  </div>



  <section class="section section-lg pt-lg-0 mt--200">
    <div class="container  wow fadeInUp">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="row row-grid">
            {{-- <div class="col-lg-4">
              <div class="card card-lift--hover shadow border-0">
                <div class="card-body py-5">
                  <div class="icon icon-shape icon-shape-primary rounded-circle mb-4">
                    <i class="ni ni-check-bold"></i>
                  </div>
                  <h6 class="text-primary text-uppercase">Parents</h6>
                  <div>
                    <span class="badge badge-pill badge-primary">Grades</span>
                    <span class="badge badge-pill badge-primary">Reports</span>
                    <span class="badge badge-pill badge-primary">Contacts</span>
                  </div>
                  <a href="#" class="btn btn-primary mt-4">Learn more</a>
                </div>
              </div>
            </div> --}}
            <div class="col-lg-6">
              <div class="card card-lift--hover shadow border-0">
                <div class="card-body py-5">
                  <div class="icon icon-shape icon-shape-success rounded-circle mb-4">
                    <i class="ni ni-istanbul"></i>
                  </div>
                  <h6 class="text-success text-uppercase">Students</h6>
                  {{-- <p class="description mt-3">Argon is a great free UI package based on Bootstrap 4 that includes the most important components and features.</p> --}}
                  <div>
                    <span class="badge badge-pill badge-success">Enrollment</span>
                    <span class="badge badge-pill badge-success">My Portal</span>
                    <span class="badge badge-pill badge-success">Schedules</span>
                    <span class="badge badge-pill badge-success">Subjects</span>
                    <span class="badge badge-pill badge-success">Instructors</span>
                    <span class="badge badge-pill badge-success">Library</span>
                  </div>
                  <a href="#" class="btn btn-success mt-4">Learn more</a>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card card-lift--hover shadow border-0">
                <div class="card-body py-5">
                  <div class="icon icon-shape icon-shape-warning rounded-circle mb-4">
                    <i class="ni ni-planet"></i>
                  </div>
                  <h6 class="text-warning text-uppercase">Faculty & Staff</h6>
                  {{-- <p class="description mt-3">Argon is a great free UI package based on Bootstrap 4 that includes the most important components and features.</p> --}}
                  <div>
                    <span class="badge badge-pill badge-warning">Subject Loads</span>
                    <span class="badge badge-pill badge-warning">DTR</span>
                    <span class="badge badge-pill badge-warning">Grading</span>
                    <span class="badge badge-pill badge-warning">Master List</span>
                    <span class="badge badge-pill badge-warning">Events</span>
                    <span class="badge badge-pill badge-warning">Meeting</span>
                  </div>
                  <a href="#" class="btn btn-warning mt-4">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-lg">
    <div class="container">
      <div class="row row-grid align-items-center">
        <div class="col-md-6 order-md-2">
          {{-- <img src="{{ asset('img/theme/promo-1.png') }}" class="img-fluid floating"> --}}
          <iframe width="450" height="400" src="https://www.youtube.com/embed/zH7uU394c0w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-6 order-md-1">
          <div class="pr-md-5">
            {{-- <div class="icon icon-lg icon-shape icon-shape-success shadow rounded-circle mb-5">
              <i class="ni ni-settings-gear-65"></i>
            </div> --}}
            <h3>Vision</h3>
            <p>The Bulacan Polytechnic College envisions to become a lead provider of quality and affordable technical-vocational, entrepreneurial and technological education, and a producer of highly competent and productive human resource.
            </p>
            <ul class="list-unstyled mt-5">
              <li class="py-2">
                <div class="d-flex align-items-center">
                  <div>
                    <div class="badge badge-circle badge-success mr-3">
                      <i class="fas fa-graduation-cap"></i>
                    </div>
                  </div>
                  <div>
                    <h6 class="mb-0">Quality Education</h6>
                  </div>
                </div>
              </li>
              <li class="py-2">
                <div class="d-flex align-items-center">
                  <div>
                    <div class="badge badge-circle badge-success mr-3">
                      <i class="ni ni-satisfied"></i>
                    </div>
                  </div>
                  <div>
                    <h6 class="mb-0">Highly Competent</h6>
                  </div>
                </div>
              </li>
              <li class="py-2">
                <div class="d-flex align-items-center">
                  <div>
                    <div class="badge badge-circle badge-success mr-3">
                      <i class="fa fa-money"></i>
                    </div>
                  </div>
                  <div>
                    <h6 class="mb-0">Affordable</h6>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

  
  <section class="section section-lg bg-gradient-default">
    <div class="container pt-lg pb-300">
      <div class="row">
         {{-- transform-perspective-right --}}
         <div class="rounded shadow-lg overflow-hidden ">
            <div id="carousel_example" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carousel_example" data-slide-to="0" class="active"></li>
                <li data-target="#carousel_example" data-slide-to="1"></li>
                <li data-target="#carousel_example" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="img-fluid" src="{{ asset('/images/freshmen2019_2020.jpg') }}" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="img-fluid" src="{{ asset('/images/BPC_movehere.jpg') }}" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img class="img-fluid" src="{{ asset('/images/BPC_free2019.jpg') }}" alt="Third slide">
                </div>
              </div>
              <a class="carousel-control-prev" href="#carousel_example" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel_example" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
      </div>
      <div class="row text-center justify-content-center">
        <div class="col-lg-10">
          <h2 class="display-3 text-white">Bulacan Polytechnic College</h2>
          <p class="lead text-white">Bpc is consistently making difference in giving quality and affordable education to all bulacenios from senior high school to technical vocational courses to degree courses so you’ll have a lot of programs to choose from.</p>
        </div>
      </div>
      
      <div class="row row-grid mt-5 justify-content-center">
        <div class="col-lg-4">
          <div class="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle text-primary">
            <i class="ni ni-settings text-primary"></i>
          </div>
          <h5 class="text-white mt-3">Degree Courses</h5>
          <p class="text-white mt-3"></p>
        </div>
        <div class="col-lg-4">
          <div class="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle text-primary">
            <i class="ni ni-ruler-pencil text-primary"></i>
          </div>
          <h5 class="text-white mt-3">Technical-Vocational</h5>
          <p class="text-white mt-3"></p>
        </div>
        <div class="col-lg-4">
          <div class="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle text-primary">
            <i class="ni ni-atom text-primary"></i>
          </div>
          <h5 class="text-white mt-3">Senior High School</h5>
          <p class="text-white mt-3"></p>
        </div>
      </div>
    </div>
    <!-- SVG separator -->
    <div class="separator separator-bottom separator-skew zindex-100">
      <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
      </svg>
    </div>
  </section>
  
  
  <section id="contacts" class="section section-lg pt-lg-0 section-contact-us wow bounceInUp">
      <div class="container">
        <div class="row justify-content-center mt--300">
          <div class="col-lg-8">
            <div class="card bg-gradient-secondary shadow">
              <div class="card-body p-lg-5">
                <h4 class="mb-1">Have a project in mind?</h4>
  
    <h6><i class="fa fa-envelope" aria-hidden="true"></i>  Email: bpc@bulacan.gov.ph / bulpolycol@gmail.com /
      bulpolycol@yahoo.com</h6>
    <h6><i class="fa fa-phone" aria-hidden="true"></i> Telefax: 044-791-3048</h6>
    <h6><i class="fa fa-map-marker" aria-hidden="true"></i> BPC Main Campus, Bulihan, City of Malolos, Bulacan, Philippines 300</h6>
                <div class="form-group mt-5">
                      <p class="mt-0">Or Send Me a Message.</p>
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-user-run"></i></span>
                    </div>
                    <input class="form-control" placeholder="Your name" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email address" type="email">
                  </div>
                </div>
                <div class="form-group mb-4">
                  <textarea class="form-control form-control-alternative" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
                </div>
                <div>
                  <button type="button" class="btn btn-default btn-round btn-block btn-lg">Send Message</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>
@endsection