@extends('layouts.app') 
@section('styles')

@endsection
@section('content')
<main>
    <div class="position-relative"> <div class="position-relative">
        <!-- shape Hero -->
        <section class="section section-lg section-shaped pb-250">
          <div class="shape shape-style-1 shape-default">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div class="container py-lg-md d-flex">
            <div class="col px-0">
              <div class="row">
                <div class="col-lg-7">
                  <h1 class="display-3  text-white">Explore our Journey</span>
                  </h1>
                  <p class="lead  text-wite" style="color:whitesmoke">As we develop and fight for our Capstone... Giving our best shot to every things that we do.</p>
                  {{-- <div class="btn-wrapper">
                    <a href="#" class="btn btn-info btn-icon mb-3 mb-sm-0">
                      <span class="btn-inner--icon"><i class="fa fa-code"></i></span>
                      <span class="btn-inner--text">Reach Us</span>
                    </a>
                    <a href="enrollment/college" class="btn btn-white btn-icon mb-3 mb-sm-0">
                      <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                      <span class="btn-inner--text">Enrollment</span>
                    </a>
                  </div> --}}
                </div>
              </div>
            </div>
          </div>
          <!-- SVG separator -->
          <div class="separator separator-bottom separator-skew">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
              <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
            </svg>
          </div>
        </section>
    <section class="section bg-secondary ">
      <div class="container ">
        <div class="row row-grid align-items-center">
          <div class="col-md-6">
            <div class="card bg-default shadow border-0">
              <img src="{{ asset('img/theme/IMG_20190310_150205.jpg') }}" class="card-img-top">
              <blockquote class="card-blockquote">
                <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 583 95" class="svg-bg">
                  <polygon points="0,52 583,95 0,95" class="fill-default" />
                  <polygon points="0,42 583,95 683,0 0,95" opacity=".2" class="fill-default" />
                </svg>
                <h4 class="display-3 font-weight-bold text-white"></h4>
                <p class="lead text-italic text-white"></p>
              </blockquote>
            </div>
          </div>
          <div class="col-md-6">
            <div class="pl-md-5">
              {{-- <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle mb-5">
                <i class="ni ni-settings"></i>
              </div> --}}
              <h3>BPC MIS</h3>
              <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                <i class="fa fa-twitter"></i>
              </a>
              <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                <i class="fa fa-facebook"></i>
              </a>
              <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                <i class="fa fa-dribbble"></i>
              </a>
              <h5>Bachelor of Science in Information Systems</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section section-lg">
        <div class="container">
          <div class="row row-grid align-items-center">
            <div class="col-md-6 order-md-2">
              <img src="{{ asset('img/theme/IMG_20190310_145927.jpg') }}" class="img-fluid floating">
            </div>
            <div class="col-md-6 order-md-1">
              <div class="pr-md-5">
                <div class="icon icon-lg icon-shape icon-shape-success shadow rounded-circle mb-5">
                  <i class="ni ni-settings-gear-65"></i>
                </div>
                <h3>Picture with adviser & panelist</h3>
                <p>Feel the excitement and savor the victory.</p>
                <ul class="list-unstyled mt-5">
                  <li class="py-2">
                    <div class="d-flex align-items-center">
                      <div>
                        <div class="badge badge-circle badge-success mr-3">
                          <i class="ni ni-settings-gear-65"></i>
                        </div>
                      </div>
                      <div>
                        <h6 class="mb-0">Carefully</h6>
                      </div>
                    </div>
                  </li>
                  <li class="py-2">
                    <div class="d-flex align-items-center">
                      <div>
                        <div class="badge badge-circle badge-success mr-3">
                          <i class="ni ni-html5"></i>
                        </div>
                      </div>
                      <div>
                        <h6 class="mb-0">Amazingly</h6>
                      </div>
                    </div>
                  </li>
                  <li class="py-2">
                    <div class="d-flex align-items-center">
                      <div>
                        <div class="badge badge-circle badge-success mr-3">
                          <i class="ni ni-satisfied"></i>
                        </div>
                      </div>
                      <div>
                        <h6 class="mb-0">Happy</h6>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>  
      <section class="section section-lg pb-300 mb-50" style="">
        <div class="container">
          <div class="row justify-content-center text-center mb-lg">
            <div class="col-lg-8">
                <img src="{{ asset('Bulacan Polytechnic College bsis logo.png') }}" style="width:150px; height:150px;" alt="">
              <h2 class="display-3">The Amazing Team</h2>
              <p class="lead text-muted">From the Proud Section C of BSIS Department.</p>
              <a href="/about-developers" class="btn btn-info btn-icon mb-3 mb-sm-0">
                <span class="btn-inner--icon"><i class="fa fa-code"></i></span>
                <span class="btn-inner--text">About Us</span>
              </a>
            </div>
          </div>
          <div class="row">
            
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
              <div class="px-4">
                <img src="{{ asset('img/theme/dr-rosemarie-s-guirre - Copy.jpg') }}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                <div class="pt-4 text-center">
                  <h5 class="title">
                    <span class="d-block mb-1">Dr. Rosemarie S. Guirre</span>
                    <small class="h6 text-muted">The Thesis Adviser</small>
                  </h5>
                  <div class="mt-3">
                    <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.facebook.com/esor19" class="btn btn-success btn-icon-only rounded-circle">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-success btn-icon-only rounded-circle">
                      <i class="fa fa-dribbble"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
              <div class="px-4">
                <img src="{{ asset('img/theme/marc-jherico-sumilang - Copy.jpg') }}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                <div class="pt-4 text-center">
                  <h5 class="title">
                    <span class="d-block mb-1">Marc Jherico Sumilang</span>
                    <small class="h6 text-muted">The Developer</small>
                  </h5>
                  <div class="mt-3">
                    <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.facebook.com/jhericosumilang26" class="btn btn-warning btn-icon-only rounded-circle">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon-only rounded-circle">
                      <i class="fa fa-dribbble"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
              <div class="px-4">
                <img src="{{ asset('img/theme/cris-del-valle-taleon - Copy.jpg') }}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                <div class="pt-4 text-center">
                  <h5 class="title">
                    <span class="d-block mb-1">Cresanto DV. Taleon</span>
                    <small class="h6 text-muted">The Data Analyst</small>
                  </h5>
                  <div class="mt-3">
                    <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.facebook.com/crestsandtoe" class="btn btn-primary btn-icon-only rounded-circle">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-primary btn-icon-only rounded-circle">
                      <i class="fa fa-dribbble"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
              <div class="px-4">
                <img src="{{ asset('img/theme/michaela-marie-reyes copy.jpg') }}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 200px;">
                <div class="pt-4 text-center">
                  <h5 class="title">
                    <span class="d-block mb-1">Michaela Marie Reyes</span>
                    <small class="h6 text-muted">The Documentarist</small>
                  </h5>
                  <div class="mt-3">
                    <a href="#" class="btn btn-info btn-icon-only rounded-circle">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.facebook.com/michaela.b.reyes" class="btn btn-info btn-icon-only rounded-circle">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-info btn-icon-only rounded-circle">
                      <i class="fa fa-dribbble"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <br>
      </section>
      
  
  <section id="contacts" class="section section-lg pt-lg-0 section-contact-us wow bounceInUp">
      <div class="container">
        <div class="row justify-content-center mt--300">
          <div class="col-lg-8">
            <div class="card bg-gradient-secondary shadow">
              <div class="card-body p-lg-5">
                <h4 class="mb-1">Have a project in mind?</h4>
  
    <h6><i class="fa fa-envelope" aria-hidden="true"></i>  Email: bpc@bulacan.gov.ph / bulpolycol@gmail.com /
      bulpolycol@yahoo.com</h6>
    <h6><i class="fa fa-phone" aria-hidden="true"></i> Telefax: 044-791-3048</h6>
    <h6><i class="fa fa-map-marker" aria-hidden="true"></i> BPC Main Campus, Bulihan, City of Malolos, Bulacan, Philippines 300</h6>
                <div class="form-group mt-5">
                      <p class="mt-0">Or Send Me a Message.</p>
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-user-run"></i></span>
                    </div>
                    <input class="form-control" placeholder="Your name" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email address" type="email">
                  </div>
                </div>
                <div class="form-group mb-4">
                  <textarea class="form-control form-control-alternative" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
                </div>
                <div>
                  <button type="button" class="btn btn-default btn-round btn-block btn-lg">Send Message</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>
@endsection
