<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course' => 'required',
            'year' => 'required|max:1',
            'semester' => 'required|max:1',
            'code' => 'required|unique:subject',
            'title' => 'required',
            'description' => 'required',
            'units' => 'required|max:1',
            'prerequisite' => 'required',
        ];
    }
}
