<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use App\Model\Instructors\Instructor;
use App\Model\VPAF\Room;
use App\Model\VPAA\Schedule;
use App\Model\MIS\SchoolYear;
class PdfController extends Controller
{
 
    public function viewPdf(){
        try{
            $data = ['name'=>'jake'];
            return view('pdf.form', compact('data')); 
        }catch(Exception $e){
            return 'Failed to Download';
        }
    }

    public function print_instructor_scedule($name){
        
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;

        $instructor = Instructor::where('name', $name)->first();
        // return $instructor;
        
        $schedules = Schedule::where('instructor_name', $name)->orderBy('id', 'DESC')->get();
        // return $schedules;

        $pdf = PDF::loadView('pdf.instructor.schedules', compact('instructor', 'schedules', 'school_year', 'semester'));
        $filename = $name.'.pdf';
        return $pdf->stream($filename);

    }
    
    public function downloadPdf(){
        try{
            $data = ['name'=>'jake'];
            $pdf = PDF::loadView('pdf.form', compact('data'));
            // return $pdf->stream('pdf.form'); 
            // return $pdf->download('pdf.form'); 
            return $pdf->setPaper('a4', 'landscape')->download('pdf.form'); 
            return 'PDF Downloaded';
        }catch(Exception $e){
            return 'Failed to Download';
        }
    }
    
}
