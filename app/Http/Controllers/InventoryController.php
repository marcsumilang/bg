<?php

namespace App\Http\Controllers;

use App\Model\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $items = Inventory::orderBy('id', 'DESC')->get();
        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Inventory();
        $item->item_name = $request->item_name;
        $item->brand_name = $request->brand_name;
        $item->qty = $request->qty;
        $item->status = $request->status;
        $item->description = $request->description;
        $item->last_location = $request->last_location;
        $item->new_location = $request->new_location;
        $item->save();


        // return response()->json(['success' => 'success', 'msg' => 'item Added']);
        // return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inventory = Inventory::find($id);
        $inventory->item_name = $request->item_name;
        $inventory->brand_name = $request->brand_name;
        $inventory->qty = $request->qty;
        $inventory->status = $request->status;
        $inventory->description = $request->description;
        $inventory->last_location = $request->last_location;
        $inventory->new_location = $request->new_location;
        $inventory->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $inventory = Inventory::find($id);
        $inventory->status = 'Not Working';
        $inventory->save();
    }

    public function filter($item_name = 'Monitor', $brand_name = '', $status = '', $description = 'Quina', $last_location = '', $new_location = ''){
        
        if($item_name != ''){
            if($brand_name != ''){
                if($status != ''){
                    if($description != ''){
                        if($last_location != ''){
                            if($new_location != ''){
            
        $items = Inventory::orderBy('id', 'DESC')->where('item_name', $item_name)->where('brand_name', $brand_name)->where('status', $status)->where('description', $description)->where('last_location', $last_location)->where('new_location', $new_location)->get();

                            }
                        }
                    }
                }
            }   
        }
        $items = Inventory::orderBy('id', 'DESC')->where('item_name', $item_name)->where('description', $description)->get();
        return $items;
    }

}

