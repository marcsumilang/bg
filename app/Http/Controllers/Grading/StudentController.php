<?php

namespace App\Http\Controllers\Grading;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Grading\Grading_student;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use URL;
use File;
// use App\Http\Controllers\Grading\Input;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Grading_student::where('active', "TRUE")->orderBy('created_at', 'desc')->get();
    }
    
    public function uploadCsv(Request $request){
        
        $file = $request->file('select_file')->getRealPath();
        $csv= file_get_contents($file);
        $array = array_map("str_getcsv", explode("\n", $csv));
        $json = json_encode($array);
        
        //  print_r($json);
        $students = json_decode( $json, true );

        foreach ($students as $key => $new_student) {
            $student = new Grading_student();
            $student->student_id =  strtoupper($new_student[0]);
            $student->first_name =  strtoupper($new_student[1]);
            $student->middle_name =  strtoupper($new_student[2]);
            $student->last_name =  strtoupper($new_student[3]);
            $student->regular =  strtoupper($new_student[4]);
            $student->course =  strtoupper($new_student[5]);
            $student->year =  strtoupper($new_student[6]);
            $student->section =  strtoupper($new_student[7]);
            $student->school_year =  strtoupper($new_student[8]);
            $student->semester =  strtoupper($new_student[9]);
            $student->save();
        }
        return redirect('admin/manage_student');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = new Grading_student();
        $student->student_id =  strtoupper($request['student_id']);
        $student->first_name =  strtoupper($request['first_name']);
        $student->middle_name =  strtoupper($request['middle_name']);
        $student->last_name =  strtoupper($request['last_name']);
        $student->regular =  strtoupper($request['regular']);
        $student->course =  strtoupper($request['course']);
        $student->year =  strtoupper($request['year']);
        $student->section =  strtoupper($request['section']);
        $student->school_year =  strtoupper($request['school_year']);
        $student->semester =  strtoupper($request['semester']);
        $student->save();
        //INSERT INTO `grading_students` (`id`, `student_id`, `last_name`, `first_name`, `middle_name`, `regular`, `created_at`, `updated_at`) VALUES (NULL, '1', '1', '1', '1', '1', NULL, NULL);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Grading_student::where('id',$id)->first();
        $student->student_id =  strtoupper($request['student_id']);
        $student->first_name =  strtoupper($request['first_name']);
        $student->middle_name =  strtoupper($request['middle_name']);
        $student->last_name =  strtoupper($request['last_name']);
        $student->regular =  strtoupper($request['regular']);
        $student->course =  strtoupper($request['course']);
        $student->year =  strtoupper($request['year']);
        $student->section =  strtoupper($request['section']);
        $student->school_year =  strtoupper($request['school_year']);
        $student->semester =  strtoupper($request['semester']);
        $student->save();
        
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Grading_student::where('id',$id)->first();
        $student->active = "FALSE";
        $student->save();
        return "";
    }
}
