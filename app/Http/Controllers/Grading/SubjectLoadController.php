<?php

namespace App\Http\Controllers\Grading;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Grading\Grading_subject_load;
use App\Model\Admin\Subject;
use App\Model\Instructors\Instructor;
use App\Model\MIS\SchoolYear;

class SubjectLoadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subject_loads = Grading_subject_load::orderBy('updated_at', 'DESC')->where('archived', null)->get();
        $loads = [];
        foreach ($subject_loads as $key => $subject_load) {
            $subject = Subject::where('id', $subject_load->subject_id)->first();
            $instructor = Instructor::where('instructors_id', $subject_load->instructor_id)->first();
            $loads[] = [
                'id' => $subject_load->id,
                'instructor_id' => $subject_load->instructor_id,
                'instructor_name' => $instructor->name,
                'code' => $subject->code,
                'title' => $subject->title,
                'cys' => $subject_load->course . " " . $subject_load->year . "" . $subject_load->section
            ];
        }
        return $loads;

    }
 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ins = explode(',',$request->instructor);
        $sub = explode('-',$request->subject);
        $subject = Subject::where('code', $sub[0])->first();
        // return $sub[0];
        $instructor_id = $ins[0];


        //validation if exist to avoid duplicate
        $checker = Grading_subject_load::where('instructor_id' , $instructor_id)->where('subject_id' , $subject->id)->where('archived', null)->first();

        $checker = Grading_subject_load::where('course' , $request->course)->where('year' , $request->year)->where('section' , $request->section)->where('subject_id' , $subject->id)->where('archived', null)->first();
        

        if($checker == ""){
            $sys = SchoolYear::latest()->first();
            $school_year = $sys->start_year ."-". $sys->end_year;
            $subject_load = new Grading_subject_load();
            $subject_load->course = $request->course;
            $subject_load->year = $request->year;
            $subject_load->section = $request->section;
            $subject_load->subject_id = $subject->id;
            $subject_load->instructor_id = $instructor_id;
            $subject_load->school_year = $school_year;
            $subject_load->save();
    
            return response()->json(true);
        }else{  
            return response()->json('Failed: Duplicated of record');
        }


      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ins = explode(',',$request->instructor);
        $sub = explode('-',$request->subject);
        $subject = Subject::where('code', $sub[0])->first();
        // return $sub[0];
        $instructor_id = $ins[0];


        //validation if exist to avoid duplicate
        $checker = Grading_subject_load::where('instructor_id' , $instructor_id)->where('subject_id' , $subject->id)->where('archived', null)->first();

        $checker = Grading_subject_load::where('course' , $request->course)->where('year' , $request->year)->where('section' , $request->section)->where('subject_id' , $subject->id)->where('archived', null)->first();
        

        if($checker == ""){
            $subject_load = Grading_subject_load::where('id', $id)->first();
            $subject_load->course = $request->course;
            $subject_load->year = $request->year;
            $subject_load->section = $request->section;
            $subject_load->subject_id = $subject->id;
            $subject_load->instructor_id = $instructor_id;
            $subject_load->save();

            return response()->json(true);
        }else{  
            return response()->json('Failed: Duplicated of record');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        $subject_load = Grading_subject_load::where('id', $id)->first();
        $subject_load->archived = true;
        $subject_load->save();

        return response()->json(true);
    }
}
