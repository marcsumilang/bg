<?php

namespace App\Http\Controllers\Registrar;

use App\Model\Registrar\EnrollmentProcedure;
use App\Model\MIS\SchoolYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Log;
use Bitfumes\Multiauth\Model\Admin;

class EnrollmentProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procedure = EnrollmentProcedure::latest()->first();
        // return $sys;
        return view('registrar.enrollmentProcedure')->with('procedure', $procedure);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $procedure = new EnrollmentProcedure;
        $procedure->level = 'College';
        $procedure->description = $request->procedure;
        $procedure->save();
        
        $admin = Admin::find($request->admin_id);   
                  
        $log = new Log();
        $log->admin_id = $admin->id;
        $log->action = 'UPDATED COLLEGE PROCEDURES';
        $log->details = "NAME: " . $admin->name;
        $log->save();

        $procedure = EnrollmentProcedure::latest()->first();
        // return $procedure;
        return redirect('admin/enrollment-procedure'); 
        // view('registrar.enrollmentProcedure')->with('procedure', $procedure);
        // return 'Updated';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Registrar\EnrollmentProcedure  $enrollmentProcedure
     * @return \Illuminate\Http\Response
     */
    public function show(EnrollmentProcedure $enrollmentProcedure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Registrar\EnrollmentProcedure  $enrollmentProcedure
     * @return \Illuminate\Http\Response
     */
    public function edit(EnrollmentProcedure $enrollmentProcedure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Registrar\EnrollmentProcedure  $enrollmentProcedure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnrollmentProcedure $enrollmentProcedure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Registrar\EnrollmentProcedure  $enrollmentProcedure
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnrollmentProcedure $enrollmentProcedure)
    {
        //
    }
}
