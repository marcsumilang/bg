<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Registrar\College_application;
use App\Model\MIS\SchoolYear;
use App\Model\VPAA\Schedule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $semester = $sys->semester; 
        $user_id = auth()->user()->id;
        $email = auth()->user()->email;
        // return $user_id;
        try {
            $requirements = College_application::orderBy('date_enrolled', 'DESC')->where('email', $email)->first();
        } catch (\Throwable $th) {
            $requirements = '';
        }

        // return $requirements;

        // $log = new Instructors_log();
        // $log->instructors_id = "INSTRUCTOR";
        // $log->action = 'LOGGED IN';
        // $log->details = "Name: " . $instructor->name;
        // $log->save();
try {
    $my_schedules = Schedule::where('course', $requirements->course)->where('year', $requirements->year)->where('section', $requirements->section)->where('semester', $requirements->semester)->where('school_year', $requirements->school_year)->get();
} catch (\Throwable $th) {
    $my_schedules  = null;
}
  
    // return $my_schedules;

    // return view('instructors.home')->with('instructor', $instructor)->with('sys', $sys)->with('schedules', $my_schedules);

       
        return view('home')->with('requirements', $requirements)->with('sys', $sys)->with('schedules', $my_schedules);
    }
}
