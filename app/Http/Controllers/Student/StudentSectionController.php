<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registrar\College_application;
use App\Model\MIS\SchoolYear;
use App\User;
use Illuminate\Support\Facades\Auth;
use PDF;


class StudentSectionController extends Controller
{

    public function index(){      
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $semester = $sys->semester; 
        $current_user = Auth::user();
        try {
            // return $current_user->id;
            $student_info = College_application::where('email', $current_user->email)->where('semester', $semester)->where('school_year', $school_year)->first();
                // return $student_info;
            if($student_info == null){
                return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
            }else{
                if($student_info->application_status == 'Ongoing'){
                    return redirect('/home')->with('warning', 'You are not Enrolled Yet. Your applications status is On Process.')->with('sys', $sys);
                }

                return view('student.sectioning')->with('sys', $sys);
            }
        } catch (\Throwable $th) {
            return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
        }
       
    }

    public function getMyInfo($email){     
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $semester = $sys->semester;
        // $user = User::find($email);
        $student_info = College_application::where('email', $email)->where('semester', $semester)->where('school_year', $school_year)->get();
        return $student_info;
    }

    public function viewBatchSections($course, $year, $section, $school_year, $semester){       

    try {
        $batch_sections = College_application::where('course', $course)->where('year', $year)->where('section', $section)->where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $semester)->get();
        
        return $batch_sections;
        // if($batch_sections){
        // }
        // return null;
    } catch (\Throwable $th) {
        return "NO STUDENTS ENROLLED";
    }
            
    }

    
    public function pdfBatchSections($course, $year, $section, $school_year, $semester){       
        // $sys = SchoolYear::latest()->first();
        // $school_year = $sys->start_year . '-' . $sys->end_year;
    // return $school_year;
            $students = College_application::where('course', $course)->where('year', $year)->where('section', $section)->where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $semester)->get();
            
            $pdf = PDF::loadView('pdf.student.student_list', compact('students', 'course', 'year', 'section', 'school_year', 'semester'));
            return $pdf->download('student_list.pdf');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateSection($id, $section){       
          // return $request->id;
          $student_info = College_application::where('id', $id)->first();
          $student_info->section = $section;
          $student_info->save();
          // return $student_info;
          $data = array(
              'my_info' => $student_info,
              'section' => $student_info->section
          );
           return $data;  

        // $student_info = College_application::where('id', $id)->get();
        // $student_info->section = $section;
        // // return $student_info->section;
        // $student_info->save();
        // $data = array(

        // );
        //  return $student_info;  
            
    }


}
