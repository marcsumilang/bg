<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registrar\College_application;
use Illuminate\Support\Facades\Auth;
use App\Model\MIS\SchoolYear;

class StudentCollegeApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
            $sys = SchoolYear::latest()->first();
            $school_year = $sys->start_year . '-' . $sys->end_year;
            $semester = $sys->semester;            
            $id = auth()->user()->email;
            
            // $school_year ='2018-2019';
            // $semester = '1'; 
            // $id = '6';
            
            $requirements = College_application::where('email', $id)->where('school_year', $school_year)->where('semester', $semester)->first();

            // return $id;

            // return $requirements->email;
            if($requirements != null){
                return redirect('/home')->with('warning', 'you already submitted an application form');
            }else{
                return view('student.application-form');
            }
            
        
      
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  \App\Model\Registrar\College_application  $College_application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $section)
    {
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
