<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registrar\College_application;
use App\Model\Admin\Subject;
use App\Model\MIS\SchoolYear;
use App\Model\Registrar\TuitFee;

use PDF;
class StudentSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sys = SchoolYear::latest()->first();
        $semester = $sys->semester;
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $user_id = auth()->user()->id;
        $current_user = auth()->user();
       
        
        // try {
            $student = College_application::where('email', $current_user->email)->where('semester', $semester)->where('school_year', $school_year)->first();
            // return $student;
            if($student == null || ''){
                return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
            }
            
            if($student->application_status == 'Ongoing'){
                return redirect('/home')->with('warning', 'You are not Enrolled Yet. Check again when you receive an email. Thank you!')->with('sys', $sys);
            }
            
            $tuitfee = TuitFee::where('course',  $student->course)->where('year',  $student->year)->where('semester',  $sys->semester)->where('archived', null)->get();           
            // return $tuitfee;
            
            $cys = array(
                'course' => $student->course,
                'year' => $student->year,
                'semester' => $sys->semester
                
            );
            $mysubjects = Subject::where('course',  $student->course)->where('year',  $student->year)->where('semester',  $sys->semester)->get();
            
            // return $mysubjects;
    
            $total_units = 0;
    
            if($mysubjects){
                foreach($mysubjects as $mysubject){
                   $total_units += (int)$mysubject->units;
                }
            }
    
            // return $tuitfee[0]->per_unit;
            if($tuitfee){
                $per_unit = $tuitfee[0]->per_unit;
                $tuition = $tuitfee[0]->per_unit * $total_units;
            }
    
            $tuition_fee = array(
                'total_units' => $total_units,
                'per_unit' => $per_unit,
                'amount' => $tuition
            );
            // return 'Tuition' . $tuition;
    
            if($student->regular_irregular === 'Regular'){
    
                
                // return $cys;
                // return $mysubjects;
                return view('student.mysubject')->with('course', $student->course)->with('mysubjects', $mysubjects)->with($cys)->with($tuition_fee);
    
            }
            elseif($student->regular_irregular === 'Irregular'){
    
                // $mysubjects = Subject::where('course',  $student->course)->where('year',  $student->year)->get();           
                // return $mysubjects;
                return view('student.mysubject')->with('message', 'Irregular')->with('course', $student->course)->with('mysubjects', $mysubjects)->with($cys)->with($tuition_fee);
    
            }
            return view('student.mysubject')->with('message', 'Irregular')->with('course', $student->course)->with('mysubjects', $mysubjects)->with($cys)->with($tuition_fee);
        
        // } catch (\Throwable $th) {
        //     return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
        // }
        
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
