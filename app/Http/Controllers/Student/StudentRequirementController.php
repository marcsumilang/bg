<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registrar\College_application;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Image;

class StudentRequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        // return $user_id;
        $requirements = College_application::where('student_portal_id', $user_id)->first();
        return view('student.requirements')->with('requirements', $requirements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $user_id = auth()->user()->id;
        // return $user_id;
        
        $student_portal_id = $request->student_portal_id;
        
        $requirements = College_application::where('student_portal_id', $student_portal_id)->first();
        // return view('student.requirements')->with('requirements', $requirements);
        // return public_path('images/thumbnail');
        if($request->hasFile('picture')) {
            
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();
           
            //get filename with extension
            $filenamewithextension = $request->file('picture')->getClientOriginalName();
     
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
     
            //get file extension
            $extension = $request->file('picture')->getClientOriginalExtension();
     
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
     
            //Upload File
            $request->file('picture')->storeAs('public/requirements', $filenametostore);
            $request->file('picture')->storeAs('public/requirements/thumbnail', $filenametostore);
     
            //Resize image here
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            $requirements->picture = $filenametostore;
        }
 
        
        if($request->hasFile('card')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('card')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('card')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('card')->storeAs('public/requirements', $filenametostore);
            $request->file('card')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->card = $filenametostore;
        }
        if($request->hasFile('nso')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('nso')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('nso')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('nso')->storeAs('public/requirements', $filenametostore);
            $request->file('nso')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->nso = $filenametostore;
        }
        if($request->hasFile('good_moral')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('good_moral')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('good_moral')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('good_moral')->storeAs('public/requirements', $filenametostore);
            $request->file('good_moral')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->good_moral = $filenametostore;
        }
        if($request->hasFile('ncae')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('ncae')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('ncae')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('ncae')->storeAs('public/requirements', $filenametostore);
            $request->file('ncae')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->ncae = $filenametostore;
        }
        if($request->hasFile('interview_slip')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('interview_slip')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('interview_slip')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('interview_slip')->storeAs('public/requirements', $filenametostore);
            $request->file('interview_slip')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->interview_slip = $filenametostore;
        }
        if($request->hasFile('tor')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('tor')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('tor')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('tor')->storeAs('public/requirements', $filenametostore);
            $request->file('tor')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->tor = $filenametostore;
        }
        if($request->hasFile('honorable_dismissal')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('honorable_dismissal')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('honorable_dismissal')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('honorable_dismissal')->storeAs('public/requirements', $filenametostore);
            $request->file('honorable_dismissal')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->honorable_dismissal = $filenametostore;
        }
        if($request->hasFile('eog')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('eog')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('eog')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('eog')->storeAs('public/requirements', $filenametostore);
            $request->file('eog')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->eog = $filenametostore;
        }
        if($request->hasFile('clearance')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('clearance')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('clearance')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('clearance')->storeAs('public/requirements', $filenametostore);
            $request->file('clearance')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->clearance = $filenametostore;
        }
        if($request->hasFile('cog')) {
            $requirements = College_application::where('student_portal_id', $student_portal_id)->first();           
            $filenamewithextension = $request->file('cog')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('cog')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $request->file('cog')->storeAs('public/requirements', $filenametostore);
            $request->file('cog')->storeAs('public/requirements/thumbnail', $filenametostore);
            $thumbnailpath = public_path('storage/requirements/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $requirements->cog = $filenametostore;
        }
       

        $requirements->save();
        return redirect('http://bpc.org/requirements')->with('success', "Image uploaded successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($email)
    {
        return 'update '.$email;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
