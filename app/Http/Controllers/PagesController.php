<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Registrar\EnrollmentProcedure;
use App\Model\MIS\SchoolYear;
use App\Model\Registrar\College_application;
use App\User;
use Bitfumes\Multiauth\Model\Admin;
use Illuminate\Support\Facades\Hash;
use App\Model\Registrar\MiscFee;
use App\Model\Registrar\TuitFee;
use App\Model\Admin\Subject;
use App\Model\Log;
use Mail;
use PDF;
use Nexmo;
use App\Model\Instructors\Instructor;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class PagesController extends Controller
{
    
    public function view_enrolled(){        
         
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;        
        $students = College_application::where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $semester)->get();
        // return $students;
        return view('registrar.enrolled_students')->with('sys',$sys)->with('students', $students);
     }
     public function view_application_form(){        
          
         $sys = SchoolYear::latest()->first();
         $school_year = $sys->start_year . '-' . $sys->end_year;
         $semester = $sys->semester;        
         $students = College_application::where('application_status', 'Ongoing')->where('school_year', $school_year)->where('semester', $semester)->get();
         // return $students;
         return view('registrar.application_forms')->with('sys',$sys)->with('students', $students);
      }
    public function view_application($email){        
         
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;        
        $student = College_application::where('email', $email)->where('school_year', $school_year)->where('semester', $semester)->first();
        // $student->new_returnee_transferee == "Returnee";
        // return $student;
        return view('registrar.student_application_form')->with('sys',$sys)->with('requirements', $student);
     }

    public function CollegeEnrollment(){
        $procedure = EnrollmentProcedure::latest()->first();
        // return $procedure;
        $sys = SchoolYear::latest()->first();
        
        $data = array(
            'procedure' => $procedure,
            'sys' => $sys
    );

        // return $sys;
        return view('enrollment.collegeProcedure')->with($data);
    }
    public function CollegeApplication(){

        return view('enrollment.collegeApplication');
    }
    
    public function studentInfo(){
        $sys = SchoolYear::latest()->first();
        $semester = $sys->semester;
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $user_id = auth()->user()->id;
        $email = auth()->user()->email;
    
            // return $current_user->id;
            $student_info = College_application::where('email', $email)->where('school_year', $school_year)->where('semester', $semester)->first();
            if($student_info == null){
                return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
            }else{

                $requirements = $student_info;
                
            // $pdf = PDF::loadView('pdf.student.student_list', compact('sys','requirements'));
            // return $pdf->download('info.pdf');
                // $requirements = College_application::where('email', $email)->first();        
                return view('student.student-info')->with('requirements', $student_info)->with('sys', $sys);
            }
      
    }

    public function changePass(){
        $user_id = auth()->user()->id;
        $pass = auth()->user()->password;
        // $user = User::find($user_id);
        
        return view('student.change-pass')->with('message', '');
    }

    public function adminChangePass(){
        return view('changepass.admin')->with('message', '');
    }

    public function superAdminChangePass(){
        return view('changepass.superadmin')->with('message', '');
    }

    public function updatePass(Request $request){
        $user_id = auth()->user()->id;
        $pass = auth()->user()->password;

        $request->validate([
            'new_password' => 'required|min:6',
            'password_confirmation' => 'required|min:6'
        ]);

        // return $request;
        if (Hash::check($request->old_password, $pass)) {
            if($request->new_password == $request->password_confirmation){
                if ($request->new_password == $request->old_password) {
                    return redirect('/change-pass')->with('error', 'Submit a New Password!');
                }
                // redirect('admin/enrollment-procedure'); 
                $user = User::find($user_id);
                $user->password = Hash::make($request->new_password);
                $user->save();

                

                return redirect('/change-pass')->with('message', 'Password Updated!');
            }else{
                return redirect('/change-pass')->with('error', 'New Password & Confirmation Does not Match!');
            }
        }else{
            return redirect('/change-pass')->with('error', 'Invalid Old Password!');
        }       
    }
    


    public function adminUpdatePass(Request $request){
        $admin_id = $request->id;
        $hashedoldpassword = $request->oldpassword;

        $request->validate([
            'new_password' => 'required|min:6',
            'password_confirmation' => 'required|min:6'
        ]);

        // return $request;
        if (Hash::check($request->old_password, $hashedoldpassword)) {
            if($request->new_password == $request->password_confirmation){
                if ($request->new_password == $request->old_password) {
                    if($admin_id == 1){
                        return redirect('/admin/super/change-pass')->with('error', 'Submit a New Password!');
                    }
                    return redirect('/admin/change-pass')->with('error', 'Submit a New Password!');
                }
                // redirect('admin/enrollment-procedure'); 
                $admin = Admin::find($admin_id);
                $admin->password = Hash::make($request->new_password);
                $admin->save();

                
                $log = new Log();
                $log->admin_id = $admin_id;
                $log->action = 'CHANGED PASSWORD';
                $log->details = "Name: " . $admin->name;
                $log->save();


                if($admin_id == 1){
                    return redirect('/admin/super/change-pass')->with('message', 'Password Updated!');
                }
                return redirect('/admin/change-pass')->with('message', 'Password Updated!');
            }else{
                if($admin_id == 1){
                    return redirect('/admin/super/change-pass')->with('error', 'New Password & Confirmation Does not Match!');
                }
                return redirect('/admin/change-pass')->with('error', 'New Password & Confirmation Does not Match!');
            }
        }else{
            if($admin_id == 1){
                return redirect('/admin/super/change-pass')->with('error', 'Invalid Old Password!');
            }
            return redirect('/admin/change-pass')->with('error', 'Invalid Old Password!');
        }       
    }

    public function miscFees(){
        $sys = SchoolYear::latest()->first();
        
        $data = array(
            'sys' => $sys
          );

        $misc_fees = MiscFee::all();
        return view('pages.miscfee')->with($data);
    }

    
    public function tuitFees(){
        $sys = SchoolYear::latest()->first();
        
        $data = array(
            'sys' => $sys
          );
   
        return view('pages.tuitfee')->with($data);
    }

    
    public function subjects(){
        $sys = SchoolYear::latest()->first();
        
        $data = array(
            'sys' => $sys
          );
   
        return view('pages.subjects')->with($data);
    }


    
    
    public function sendTestMail($email){
        
        try{
            $user = 'BPC MIS';
            $data = array('name' => 'Marc Jherico Sumilang', 'email' => 'marcjhericosumilang@gmail.com');
            Mail::send('emails.test',$data ,  function($message) use ($data){
                $message->to('marcjhericosumilang@gmail.com', 'Marc Jherico Sumilang')->subject('Mailgun and Laravel are awesome!');
            });
            return 'EMAIL SENT';
        }catch(Exception $e){
            return 'Sending Failed';
        }
    }
    
    public function viewInventory(){        
       return view('inventory.manage_inventory');
    }
    
    public function viewInstructors(){        
        return view('admin.instructor');
     }
     public function viewRooms(){        
        return view('admin.room');
     }
     public function viewSchedules(){        
        return view('vpaa.scheduling');
     }
     public function viewManageStudent(){        
        return view('vpaa.manage_student');
     }
     public function viewManageSubjectLoads(){        
        return view('vpaa.manage_subject_loads');
     }
     public function viewCourses(){        
        return view('registrar.course');
     }
     public function viewBooks(){        
        return view('librarian.books');
     }
     public function oncampus(){     
        $sys = SchoolYear::latest()->first();
        $semester = $sys->semester;
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $user_id = auth()->user()->id;
        $email = auth()->user()->email;
    
            $student_info = College_application::where('email', $email)->where('school_year', $school_year)->where('semester', $semester)->first();
            if($student_info == null){
                return redirect('/home')->with('warning', 'You Must Submit an Application Form First.')->with('sys', $sys);
            }else{
                return view('student.oncampus');
            }   
     }
     public function sendSMS(){        
        Nexmo::message()->send([
            'to'   => '09559371500',
            'from' => '09067612358',
            'text' => 'Using the facade to send a message.'
        ]);
     }

     



}
