<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Admin\Subject;
use App\Model\Registrar\TuitFee;
use App\Model\Registrar\College_application;
use App\Model\Registrar\MiscFee;
use App\Model\MIS\SchoolYear;
use App\Model\VPAA\Schedule;

class SearchController extends Controller
{
    //
   
    public function student_searchInstructor()
    { 
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;  
        $semester = $sys->semester; 
        $user_id = auth()->user()->id;
        $email = auth()->user()->email;
        // return $user_id;
           
        try {
            $requirements = College_application::where('email', $email)->first();
        } catch (\Throwable $th) {
            $requirements = null;
        }
        // return $requirements;
        if($requirements == null){
            $my_schedules  = null;
            return  redirect('/home')->with('warning', 'You are not Enrolled.')->with('sys', $sys)->with('schedules', $my_schedules);
        }
        return view('student.search_instructor');
    }
   
    public function searchSubjectCourse($course, $year)
    {
        $subject = Subject::where('course', $course)->where('year', $year)->where('archieved', null)->get();
        return $subject;
    }
    public function searchSubjectCourseYearSemester($course, $year, $semester)
    {
        $mysubjects = Subject::where('course', $course)->where('year', $year)->where('semester', $semester)->where('archieved', null)->get();
        $tuitfee = TuitFee::where('course',  $course)->where('year',  $year)->where('semester',  $semester)->where('archieved', null)->get();           
        // return $tuitfee;

      
        $total_units = 0;

        if($mysubjects){
            foreach($mysubjects as $mysubject){
               $total_units += (int)$mysubject->units;
            }
        }

        // return $tuitfee[0]->per_unit;
        if($tuitfee){
            $per_unit = $tuitfee[0]->per_unit;
            $tuition = $tuitfee[0]->per_unit * $total_units;
        }

        $data = array(
            'subjects' => $mysubjects,
            'total_units' => $total_units,
            'per_unit' => $per_unit,
            'amount' => $tuition
        );
        return $data;
        return $subject;
    }

    
    public function searchTuitionCourseYearSemester($course, $year, $semester)
    {
        $tuitionfees = TuitFee::where('course', $course)->where('year', $year)->where('semester', $semester)->where('archieved', null)->get();
        return $tuitionfees;
    }
    
    public function searchMiscFeeLevel($level)
    {
        $miscfees = MiscFee::where('level', $level)->where('archieved', null)->get();
        $total_amount = 0;
        foreach($miscfees as $miscfee){
            $total_amount += $miscfee->amount;
        }
        $data = array(
            'miscfees' => $miscfees,
            'total_amount'=> $total_amount
        );
        return $data;
        return $miscfee;
    }

    
    public function searchCollegeApplicationsByDate($date)
    {
        $College_application = College_application::where('date', $date)->get();
        return $College_application;
    }

    
    public function searchCollegeApplicationsByCourseYear($course, $year)
    {
        $College_application = College_application::where('course', $course)->where('year', $year)->where('application_status', 'Ongoing')->get();
        return $College_application;
    }

    public function searchCollegeApplicationsBySYS($semester, $school_year)
    {
        $College_application = College_application::where('semester', $semester)->where('school_year', $school_year)->where('application_status', 'Ongoin')->get();
        return $College_application;
    }
}
