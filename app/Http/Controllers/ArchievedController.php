<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Admin\Subject;
use App\Model\Registrar\TuitFee;
use App\Model\Registrar\MiscFee;
use App\User;

class ArchievedController extends Controller
{
    
    public function searchSubjectArchieved()
    {
        $subjects = Subject::where('archieved', 'Archieved')->get();
        // return $subjects;
        return view('admin.archievedSubjects')->with('subjects', $subjects);
    }

    public function searchTuitionArchieved()
    {
        $tuit_fees = TuitFee::where('archieved', 'Archieved')->get();
        // return $subjects;
        return view('registrar.archieves.tuition')->with('tuit_fees', $tuit_fees);
    }

    public function searchMiscFeeArchieved()
    {
        $misc_fees = MiscFee::where('archieved', 'Archieved')->get();
        // return $subjects;
        return view('registrar.archieves.miscfee')->with('misc_fees', $misc_fees);
    }

    public function searchStudentPortal()
    {
        $users = User::where('archieved', 'Archieved')->get();
        // return $subjects;
        return view('mis.archievedStudentPortal')->with('users', $users);
    }
}
