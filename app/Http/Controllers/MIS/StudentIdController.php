<?php

namespace App\Http\Controllers\MIS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentIdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mis.studentid');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    public function viewBatchSections($course, $year, $section){       
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
    // return $school_year;
            $batch_sections = College_application::where('course', $course)->where('year', $year)->where('section', $section)->where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $sys->semester)->get();
            
            return $batch_sections;
    }


    
}
