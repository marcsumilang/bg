<?php

namespace App\Http\Controllers\MIS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class VerifyPortalController extends Controller
{
    public function index(){
        return view('mis.verify-portal');
    }

    public function getUsers(){
        $users = User::orderBy('updated_at', 'DESC')->where('archieved', null)->where('email_verified_at', null)->get();
        return $users;
    }

    public function verifyUser($id){
        $user = User::find($id);
        $user->email_verified_at =  date('Y-m-d h:m:s');
        $user->verified_by = 'MIS';
        $user->save();
    }

    public function archieveUser($id){
        $users = User::find($id);
        $users->archieved = 'Archieved';
        $users->save();
    }

    public function restore($id){
        $users = User::find($id);
        $users->archieved = null;
        $users->save();
        return redirect('/admin/verify-portal');
    }

}
