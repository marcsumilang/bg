<?php

namespace App\Http\Controllers\Api\Admin;

use App\Model\Admin\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubjectResource;
use App\Http\Resources\SubjectCollection;

class SubjectController extends Controller
{


    public function __construct()
    {
        // $this->middleware('Auth:api')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::orderBy('id', 'DESC')->where('archived', null)->get();
        return $subjects;
        
       return SubjectResource::collection($subjects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subject = new Subject();
        $subject->course = $request->course;
        $subject->year = $request->year;
        $subject->semester = $request->semester;
        $subject->code = $request->code;
        $subject->title = $request->title;
        $subject->description = $request->description;
        $subject->units = $request->units;
        $subject->prerequisite = $request->prerequisite;
        $subject->save();

        $subject = Subject::orderBy('id', 'DESC')->where('archived', null)->get();

        // return response()->json(['success' => 'success', 'msg' => 'Subject Added']);
        return $subject;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return $subject;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $subject->course = $request->course;
        $subject->year = $request->year;
        $subject->semester = $request->semester;
        $subject->code = $request->code;
        $subject->title = $request->title;
        $subject->description = $request->description;
        $subject->units = $request->units;
        $subject->prerequisite = $request->prerequisite;
        $subject->save();

        $subject = Subject::orderBy('id', 'DESC')->where('archived', null)->get();

        // return response()->json(['success' => 'success', 'msg' => 'Subject Added']);
        return $subject;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $subject->archieved = 'Archived';
        $subject->save();
    }


}
