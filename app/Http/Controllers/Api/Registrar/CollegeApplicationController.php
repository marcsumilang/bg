<?php

namespace App\Http\Controllers\Api\Registrar;

use Illuminate\Support\Facades\Auth;
use App\Model\Registrar\College_application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Carbon\Carbon;
// use Auth;
// use App\User;
use App\Model\MIS\SchoolYear;
use Mail;
use App\Model\Log;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// require 'vendor/autoload.php';
// use Intervention\Image\ImageManagerStatic as Image;
// use Intervention\Image\ImageManager;

class CollegeApplicationController extends Controller
{

    // public static $current_user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $college_application = College_application::where('application_status', 'Ongoing')->get();
        return $college_application;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        // $request->course = "BSIS";

 
         $sys = SchoolYear::latest()->first();
         $school_year = $sys->start_year . '-' . $sys->end_year;
             if($request->course === null){
                 return ['message'=>'error', 'field'=>'course'];
             }else if($request->year === null){
                 return ['message'=>'error', 'field'=>'year'];
             }else if($request->old_new_student === null){
                 return ['message'=>'error', 'field'=>'old_new_student'];
             }else if($request->regular_iregular === null){
                 return ['message'=>'error', 'field'=>'regular_iregular'];
             }else if($request->firstname === null){
                 return ['message'=>'error', 'field'=>'firstname'];
             }else if($request->middlename === null){
                 return ['message'=>'error', 'field'=>'middlename'];
             }else if($request->lastname === null){
                 return ['message'=>'error', 'field'=>'lastname'];
             }else if($request->initial === null){
                 return ['message'=>'error', 'field'=>'initial'];
             }else if($request->street === null){
                 return ['message'=>'error', 'field'=>'street'];
             }else if($request->barangay === null){
                 return ['message'=>'error', 'field'=>'barangay'];
             }else if($request->municipality === null){
                 return ['message'=>'error', 'field'=>'municipality'];
             }else if($request->province === null){
                 return ['message'=>'error', 'field'=>'province'];
             }else if($request->sex === null){
                 return ['message'=>'error', 'field'=>'sex'];
             }else if($request->civilstatus === null){
                 return ['message'=>'error', 'field'=>'civilstatus'];
             }else if($request->contact === null){
                 return ['message'=>'error', 'field'=>'contact'];
             }else if($request->email === null){
                 return ['message'=>'error', 'field'=>'email'];
             }else if($request->last_school_attended === null){
                 return ['message'=>'error', 'field'=>'last_school_attended'];
             }else if($request->year_graduated === null){
                 return ['message'=>'error', 'field'=>'year_graduated'];
             }else if($request->last_school_address === null){
                 return ['message'=>'error', 'field'=>'last_school_address'];
             }else if($request->father === null){
                 return ['message'=>'error', 'field'=>'father'];
             }else if($request->father_occupation === null){
                 return ['message'=>'error', 'field'=>'father_occupation'];
             }else if($request->father_address === null){
                 return ['message'=>'error', 'field'=>'father_address'];
             }else if($request->mother === null){
                return ['message'=>'error', 'field'=>'mother'];
            }else if($request->mother_occupation === null){
                return ['message'=>'error', 'field'=>'mother_occupation'];
            }else if($request->mother_address === null){
                return ['message'=>'error', 'field'=>'mother_address'];
            }else if($request->guardian === null){
                return ['message'=>'error', 'field'=>'guardian'];
            }else if($request->guardian_occupation === null){
                return ['message'=>'error', 'field'=>'guardian_occupation'];
            }else if($request->guardian_address === null){
                return ['message'=>'error', 'field'=>'guardian_address'];
            }else if($request->guardian_contacts === null){
                return ['message'=>'error', 'field'=>'guardian_contacts'];
            }else{
 
                 
 
         // $user_id = Auth::id();
         // Image::configure(array('driver' => 'gd'));
         $college_application = new College_application;
         $college_application->application_status =  'Ongoing';
         $college_application->user_id = $request->user_id;
         
         $college_application->application_date = $request->application_date;
         $college_application->course = $request->course;
         $college_application->year = (int)$request->year;
         $college_application->first_name = $request->firstname;
         $college_application->middle_name = $request->middlename;
         $college_application->last_name = $request->lastname;
         $college_application->initial = $request->initial;
         $college_application->street = $request->street;
         $college_application->barangay = $request->barangay;
         $college_application->city = $request->municipality;
         $college_application->zipcode = $request->zipcode;
         $college_application->province = $request->province;
         $college_application->sex = $request->sex;
         $college_application->birthdate = $request->birthdate;
         $college_application->civil_status = $request->civilstatus;
         $college_application->contact = $request->contact;
         $college_application->email = $request->email;
         $college_application->last_school = $request->last_school_attended;
         $college_application->year_graduated = $request->year_graduated;
         $college_application->last_school_address = $request->last_school_address;
         $college_application->father_name = $request->father;
         $college_application->father_occupation = $request->father_occupation;
         $college_application->father_address = $request->father_address;
         $college_application->mother_name = $request->mother;
         $college_application->mother_occupation = $request->mother_occupation;
         $college_application->mother_address = $request->mother_address;
         $college_application->guardian_name = $request->guardian;
         $college_application->guardian_occupation = $request->guardian_occupation;
         $college_application->guardian_address = $request->guardian_address;
         $college_application->guardian_contact = $request->guardian_contacts;
 
         $college_application->new_returnee_transferee = $request->old_new_student;
         $college_application->regular_irregular = $request->regular_iregular;
 
         $college_application->requirements = 'incomplete';
         
         $college_application->picture = 'To be Submitted';
 
         $college_application->card = 'To be Submitted';
         $college_application->good_moral = 'To be Submitted';
         $college_application->nso = 'To be Submitted';
         $college_application->ncae = 'To be Submitted';
         $college_application->interview_slip = 'To be Submitted';
 
         $college_application->tor = 'To be Submitted';
         $college_application->honorable_dismissal = 'To be Submitted';
         $college_application->eog = 'To be Submitted';
         
         $college_application->clearance = 'To be Submitted';
         $college_application->cog = 'To be Submitted';
 
         $college_application->miscellaneous = 'To be Submitted';
         $college_application->tuition = 'To be Submitted';
         $college_application->semester = $sys->semester;
         $college_application->school_year = $school_year;
         $college_application->save();
         
        
             }
 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Registrar\College_application  $college_application
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //getting the last id number and new Student ID number
        $last_enrolled_student = College_application::orderBy('updated_at', 'DESC')->where('application_status', 'Enrolled')->first();        
        $last_id = explode('-',$last_enrolled_student->student_id);
        $sy = $last_id[0];
        $last_id = (int)$last_id[1] + 1;
        $sys = SchoolYear::latest()->first();
        return $sys->start_year . '-' . $last_id;

        $college_application = College_application::find($id);
        return $college_application;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Registrar\College_application  $college_application
     * @return \Illuminate\Http\Response
     */
    public function update($email)
    {
      
        //getting the last id number and new Student ID number
        
        // $student_info = College_application::where('id', $id)->first();
        // $student_info->section = $section;
        // $student_info->save();

        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year.'-'. $sys->end_year;        
        $semester = $sys->semester;
        // $school_year = "2018-2019";
        // $semester = "2";
        
        // i need to check if 1st year student
        
        // return $last_enrolled_student;
        $college_application = College_application::where('email', $email)->first();
        if($college_application->year == '1'){
            $last_enrolled_student = College_application::orderBy('date_enrolled', 'DESC')->where('application_status', 'Enrolled')->where('semester', $semester)->where('school_year', $school_year)->where('year', '1')->first();  

            if($last_enrolled_student == null){
                $student_id = $sys->start_year . '-0001';
            } else{
                $last_student_id = $last_enrolled_student->student_id;   
                $last_id = explode('-',$last_student_id);
                $last_id = (int)$last_id[1] + 1;
                $student_id = $sys->start_year . '-' . $last_id;
            }

            
            $enrolled = College_application::where('section', 'A')->get();
            $count = count($enrolled);
            $section = 'A';
            if($count == 40){
                $enrolled = College_application::where('section', 'B')->get();
                $section = 'B';
            }
    
            $time = date("Y-m-d h:i:s");
            $college_application = College_application::where('email', $email)->where('semester', $semester)->where('school_year', $school_year)->first();
            $college_application->application_status = 'Enrolled';
            $college_application->section = $section;
            $college_application->student_id = $student_id;
            $college_application->date_enrolled = $time; 
// return $email;
                // $email = $college_application->email;
                
            try{
                $fullname = $college_application->firstname . ' ' . $college_application->initial . ' ' . $college_application->surname;

                $user = 'BPC MIS';
                $data = array('name' => $fullname, 'email' => $email);

                Mail::send('emails.test',$data ,  function($message) use ($data){
                    $message->to($data['email'], $data['name'])->subject('Enrollment Process');
                });

                // Mail::send('emails.test',$data ,  function($message) use ($data){
                //     $message->to('marcjhericosumilang@gmail.com', 'Marc Jherico Sumilang')->subject('Mailgun and Laravel are awesome!');
                // });

            $college_application->save(); 
            
        $log = new Log();
        $log->admin_id = 'REGISTRAR';
        $log->action = 'ENROLLED A STUDENT';
        $log->details = "Email: " . $email;
        $log->save();

                return 'EMAIL SENT Enrolled';
            }catch(Exception $e){
                return 'Sending Failed';
            }


        }
        
        // get last student id
        // if 1st yr 2nd sem student above get the existing id

        // get the data of last enrolled student 1st semester
      
           
        // return ;

        
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Registrar\College_application  $college_application
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $college_application = College_application::find($id);
        $college_application->delete();
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitRequirements(Request $request)
    {
        if($request->hasFile('profile_image')) {
            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
     
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
     
            //get file extension
            $extension = $request->file('profile_image')->getClientOriginalExtension();
     
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
     
            //Upload File
            $request->file('profile_image')->storeAs('public/profile_images', $filenametostore);
            $request->file('profile_image')->storeAs('public/profile_images/thumbnail', $filenametostore);
     
            //Resize image here
            $thumbnailpath = public_path('storage/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
     
            return redirect('images')->with('success', "Image uploaded successfully.");
        }
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitApplicationForm(Request $request)
    {

       
            // return redirect('/home')->with('success', "Application Submitted.");
       
        // return  ['success' => 'Application Submitted'];
    }
    public function enrollAgain($email)
    {

        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        
        $old = College_application::where('email', $email)->orderBy('date_enrolled', 'DESC')->first();
        // return $old;

        $college_application = new College_application;
        $college_application->application_status =  'Ongoing';
        $college_application->user_id = $old->user_id;
        $college_application->student_id = $old->student_id;
        
        $college_application->application_date = $old->application_date;
        $college_application->course = $old->course;
        $college_application->section = $old->section;
        $college_application->year = (int)$old->year;
        $college_application->first_name = $old->first_name;
        $college_application->middle_name = $old->middle_name;
        $college_application->last_name = $old->last_name;
        $college_application->initial = $old->initial;
        $college_application->street = $old->street;
        $college_application->barangay = $old->barangay;
        $college_application->city = $old->city;
        $college_application->zipcode = $old->zipcode;
        $college_application->province = $old->province;
        $college_application->sex = $old->sex;
        $college_application->birthdate = $old->birthdate;
        $college_application->civil_status = $old->civil_status;
        $college_application->contact = $old->contact;
        $college_application->email = $old->email;
        $college_application->last_school = $old->last_school;
        $college_application->year_graduated = $old->year_graduated;
        $college_application->last_school_address = $old->last_school_address;
        $college_application->father_name = $old->father_name;
        $college_application->father_occupation = $old->father_occupation;
        $college_application->father_address = $old->father_address;
        $college_application->mother_name = $old->mother_name;
        $college_application->mother_occupation = $old->mother_occupation;
        $college_application->mother_address = $old->mother_address;
        $college_application->guardian_name = $old->guardian_name;
        $college_application->guardian_occupation = $old->guardian_occupation;
        $college_application->guardian_address = $old->guardian_address;
        $college_application->guardian_contact = $old->guardian_contact;

        $college_application->new_returnee_transferee = $old->new_returnee_transferee;
        $college_application->regular_irregular = $old->regular_irregular;
        $college_application->semester = $sys->semester;
        $college_application->school_year = $school_year;
        $time = date("Y-m-d h:i:s");
        $college_application->date_enrolled = $time;
        $college_application->save();
        return redirect('/home')->with('success', "Application for next semester/ school year is saved.");
       
    }

    
    







    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addStudent(Request $request)
    {
       
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
            if($request->course === null){
                return ['message'=>'error', 'field'=>'course'];
            }else if($request->year === null){
                return ['message'=>'error', 'field'=>'year'];
            }else if($request->old_new_student === null){
                return ['message'=>'error', 'field'=>'old_new_student'];
            }else if($request->regular_irregular === null){
                return ['message'=>'error', 'field'=>'regular_irregular'];
            }else if($request->firstname === null){
                return ['message'=>'error', 'field'=>'firstname'];
            }else if($request->middlename === null){
                return ['message'=>'error', 'field'=>'middlename'];
            }else if($request->lastname === null){
                return ['message'=>'error', 'field'=>'lastname'];
            }else if($request->initial === null){
                return ['message'=>'error', 'field'=>'initial'];
            }else if($request->street === null){
                return ['message'=>'error', 'field'=>'street'];
            }else if($request->barangay === null){
                return ['message'=>'error', 'field'=>'barangay'];
            }else if($request->municipality === null){
                return ['message'=>'error', 'field'=>'municipality'];
            }else if($request->province === null){
                return ['message'=>'error', 'field'=>'province'];
            }else if($request->sex === null){
                return ['message'=>'error', 'field'=>'sex'];
            }else if($request->civilstatus === null){
                return ['message'=>'error', 'field'=>'civilstatus'];
            }else if($request->contact === null){
                return ['message'=>'error', 'field'=>'contact'];
            }else if($request->email === null){
                return ['message'=>'error', 'field'=>'email'];
            }else if($request->last_school_attended === null){
                return ['message'=>'error', 'field'=>'last_school_attended'];
            }else if($request->year_graduated === null){
                return ['message'=>'error', 'field'=>'year_graduated'];
            }else if($request->last_school_address === null){
                return ['message'=>'error', 'field'=>'last_school_address'];
            }else if($request->father === null){
                return ['message'=>'error', 'field'=>'father'];
            }else if($request->father_occupation === null){
                return ['message'=>'error', 'field'=>'father_occupation'];
            }else if($request->father_address === null){
                return ['message'=>'error', 'field'=>'father_address'];
            }else if($request->mother === null){
               return ['message'=>'error', 'field'=>'mother'];
           }else if($request->mother_occupation === null){
               return ['message'=>'error', 'field'=>'mother_occupation'];
           }else if($request->mother_address === null){
               return ['message'=>'error', 'field'=>'mother_address'];
           }else if($request->guardian === null){
               return ['message'=>'error', 'field'=>'guardian'];
           }else if($request->guardian_occupation === null){
               return ['message'=>'error', 'field'=>'guardian_occupation'];
           }else if($request->guardian_address === null){
               return ['message'=>'error', 'field'=>'guardian_address'];
           }else if($request->guardian_contacts === null){
               return ['message'=>'error', 'field'=>'guardian_contacts'];
           }else{

                

        // $user_id = Auth::id();
        // Image::configure(array('driver' => 'gd'));
        $college_application = new College_application;
        $college_application->application_status =  'Ongoing';
        $college_application->user_id = $request->user_id;
        
        $college_application->application_date = $request->application_date;
        $college_application->course = $request->course;
        $college_application->year = (int)$request->year;
        $college_application->first_name = $request->firstname;
        $college_application->middle_name = $request->middlename;
        $college_application->last_name = $request->lastname;
        $college_application->initial = $request->initial;
        $college_application->street = $request->street;
        $college_application->barangay = $request->barangay;
        $college_application->city = $request->municipality;
        $college_application->zipcode = $request->zipcode;
        $college_application->province = $request->province;
        $college_application->sex = $request->sex;
        $college_application->birthdate = $request->birthdate;
        $college_application->civil_status = $request->civilstatus;
        $college_application->contact = $request->contact;
        $college_application->email = $request->email;
        $college_application->last_school = $request->last_school_attended;
        $college_application->year_graduated = $request->year_graduated;
        $college_application->last_school_address = $request->last_school_address;
        $college_application->father_name = $request->father;
        $college_application->father_occupation = $request->father_occupation;
        $college_application->father_address = $request->father_address;
        $college_application->mother_name = $request->mother;
        $college_application->mother_occupation = $request->mother_occupation;
        $college_application->mother_address = $request->mother_address;
        $college_application->guardian_name = $request->guardian;
        $college_application->guardian_occupation = $request->guardian_occupation;
        $college_application->guardian_address = $request->guardian_address;
        $college_application->guardian_contact = $request->guardian_contacts;

        $college_application->new_returnee_transferee = $request->old_new_student;
        $college_application->regular_irregular = $request->regular_irregular;

        $college_application->requirements = 'incomplete';
        
        $college_application->picture = 'To be Submitted';

        $college_application->card = 'To be Submitted';
        $college_application->good_moral = 'To be Submitted';
        $college_application->nso = 'To be Submitted';
        $college_application->ncae = 'To be Submitted';
        $college_application->interview_slip = 'To be Submitted';

        $college_application->tor = 'To be Submitted';
        $college_application->honorable_dismissal = 'To be Submitted';
        $college_application->eog = 'To be Submitted';
        
        $college_application->clearance = 'To be Submitted';
        $college_application->cog = 'To be Submitted';

        $college_application->miscellaneous = 'To be Submitted';
        $college_application->tuition = 'To be Submitted';
        $college_application->semester = $sys->semester;
        $college_application->school_year = $school_year;
        $college_application->save();
        
            return "success";
            }
    }

}
