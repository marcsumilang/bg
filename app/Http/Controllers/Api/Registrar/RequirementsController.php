<?php

namespace App\Http\Controllers\Api\Registrar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MIS\SchoolYear;
use App\Model\Registrar\College_application;
use App\User;
use App\Model\Log;
use Mail;

class RequirementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

  
    public function update(Request $request, $email)
    {
        
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;        
        
        $last_enrolled_student = College_application::orderBy('date_enrolled', 'DESC')->where('application_status', 'Enrolled')->where('semester', $semester)->where('school_year', $school_year)->where('year', '1')->first();  
    
        if($last_enrolled_student == null){
            $student_id = $sys->start_year . '-0001';
        } else{
            $last_student_id = $last_enrolled_student->student_id;   
            $last_id = explode('-',$last_student_id);
            $last_id = (int)$last_id[1] + 1;
            $student_id = $sys->start_year . '-' . $last_id;
        }

        
        $enrolled = College_application::where('section', 'A')->get();
        $count = count($enrolled);
        $section = 'A';
        if($count == 40){
            $enrolled = College_application::where('section', 'B')->get();
            $section = 'B';
        }

        $time = date("Y-m-d h:i:s");

        $student = College_application::where('email', $email)->where('school_year', $school_year)->where('semester', $semester)->first();

        if($request->type == "New"){;
            $student->section = $section;
            $student->student_id = $student_id;
            $student->date_enrolled = $time; 
            $student->picture = $request->picture;
            $student->card = $request->card;
            $student->nso = $request->nso;
            $student->good_moral = $request->good_moral;
            $student->ncae = $request->ncae;
            $student->envelope = $request->envelope;
            $student->interview_slip = $request->interview_slip;
            $student->tuition = $request->tuition;
            $student->miscellaneous = $request->miscellaneous;
            $student->application_status = 'Enrolled';
            $student->save();
        }else{
            $student->section = $section;
            $student->student_id = $student_id;
            $student->date_enrolled = $time; 
            $student->picture = $request->picture;
            $student->card = $request->card;
            $student->nso = $request->nso;
            $student->good_moral = $request->good_moral;
            $student->tuition = $request->tuition;
            $student->miscellaneous = $request->miscellaneous;
            $student->application_status = 'Enrolled';
            $student->save();
      
        }
        $log = new Log();
        $log->admin_id = 'REGISTRAR';
        $log->action = 'ENROLLED A STUDENT';
        $log->details = "Email: " . $email;
        $log->save();

        // $college_application = College_application::where('email', $email)->where('semester', $semester)->where('school_year', $school_year)->first();
           
        $fullname = $student->firstname . ' ' . $student->initial . ' ' . $student->surname;

        $user = 'BPC MIS';
        $data = array('name' => $fullname, 'email' => $email);

        Mail::send('emails.test',$data ,  function($message) use ($data){
            $message->to($data['email'], $data['name'])->subject('Enrollment Process');
        });
        return 'Updated';
        return 'update '.$request->type;
       return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
