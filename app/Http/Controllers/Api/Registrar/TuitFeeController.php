<?php

namespace App\Http\Controllers\Api\Registrar;

use App\Model\Registrar\TuitFee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Log;
use Bitfumes\Multiauth\Model\Admin;

class TuitFeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tuitFee = TuitFee::orderBy('course', 'DESC')->where('archived', null)->get();
        return $tuitFee;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return auth('admin')->user();
        $tuitFee = new TuitFee();
        $tuitFee->course = $request->course;
        $tuitFee->year = $request->year;
        $tuitFee->semester = $request->semester;
        $tuitFee->per_unit = $request->per_unit;
        $tuitFee->cash_installment = $request->cash_installment;
        $tuitFee->save();

         
        $admin = Admin::find($request->admin_id);   
                  
        $log = new Log();
        $log->admin_id = $admin->id;
        $log->action = 'UPDATED COLLEGE PROCEDURES';
        $log->details = "NAME: " . $admin->name;
        $log->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Registrar\TuitFee  $tuitFee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tuitFee = TuitFee::find($id);
        return $tuitFee;
    }

 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $tuitFee = TuitFee::find($request->id);
        $tuitFee->course = $request->course;
        $tuitFee->year = $request->year;
        $tuitFee->semester = $request->semester;
        $tuitFee->per_unit = $request->per_unit;
        $tuitFee->cash_installment = $request->cash_installment;
        $tuitFee->save();

    }

  
    public function destroy($id)
    {
        $tuitFee = TuitFee::find($id);
        $tuitFee->archieved = 'Archieved';
        $tuitFee->save();
        // $tuitFee->delete();
    }
}
