<?php

namespace App\Http\Controllers\Api\Registrar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Registrar\Course;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $course = Course::orderBy('id', 'DESC')->where('archived', null)->get();
        return $course;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $course = new Course();
        $course->course_code = $request->course_code;
        $course->description = $request->description;
        $course->year = $request->year;
        $course->sections = $request->sections;
        $course->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
        $course = Course::find($id);       
        $course->course_code = $request->course_code;
        $course->description = $request->description;
        $course->year = $request->year;
        $course->sections = $request->sections;
        $course->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $course = Course::find($id);
        $course->archived = 'ARCHIVED';
        $course->save();
    }
    public function viewArchived()
    {
        $courses = Course::where('archived', 'ARCHIVED')->get();
        return view('registar.archived_room')->with('courses',$courses);
    }
}
