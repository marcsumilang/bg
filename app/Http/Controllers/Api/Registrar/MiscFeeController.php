<?php

namespace App\Http\Controllers\Api\Registrar;

use App\Model\Registrar\MiscFee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MiscFeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $misc_fees = MiscFee::orderBy('level', 'DESC')->where('archieved', null)->get();
        $total_amount = 0;
        foreach($misc_fees as $miscfee){
            $total_amount += $miscfee->amount;
        }
        $data = array(
            'miscfees' => $misc_fees,
            'total_amount'=> $total_amount
        );
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $miscfee = new MiscFee();
        $miscfee->level = $request->level;
        $miscfee->name = $request->name;
        $miscfee->amount = $request->amount;
        $miscfee->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Registrar\MiscFee  $miscFee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $miscFee = MiscFee::find($id);
        return $miscFee;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Registrar\MiscFee  $miscFee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $miscFee = MiscFee::find($request->id);
        $miscFee->level = $request->level;
        $miscFee->name = $request->name;
        $miscFee->amount = $request->amount;
        $miscFee->save();
        return $miscFee;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Registrar\MiscFee  $miscFee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $miscFee = MiscFee::find($id);
        $miscFee->archieved = 'Archieved';
        $miscFee->save();
    }
}
