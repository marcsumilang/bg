<?php

namespace App\Http\Controllers\Api\MIS;

use App\Model\MIS\SchoolYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SYSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sys = SchoolYear::latest()->first();
        // $subject = Subject::orderBy('id', 'DESC')->get();
        return $sys;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MIS\SchoolYear  $sYS
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolYear $sYS)
    {
        
        $sYS->end_year = $request->end_year;
        $sYS->start_year = $request->start_year;
        $sYS->semester = $request->semester;
        $sYS->save();

        // $sYS = SchoolYear::all();

        // return $sYS;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sysAll()
    {
        $sys = SchoolYear::all();
        return $sys;
    }
   
    
}
