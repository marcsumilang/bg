<?php

namespace App\Http\Controllers\Api\VPAF;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Instructors\Instructor;
use App\Model\VPAF\Room;
use App\Model\VPAA\Schedule;
use App\Model\MIS\SchoolYear;
use App\Model\Date_time_record;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class DTRController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtr = Date_time_record::orderBy('updated_at', 'DESC')->get();
        return $dtr;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current = Carbon::now('Asia/Manila');
        
        $year = $current->year;
        $month = $current->month;
        $day = $current->day;
        $time_in = $current->toDateTimeString();
        

        try {
            $instructor = Instructor::where('instructors_id', $request->instructors_id)->first();
// return $request->password;
            if (Hash::check($request->password, $instructor->password)) {
                try {
                    
                    $dtr = Date_time_record::where('instructors_id', $request->instructors_id)->where('day', $day)->where('month', $month)->where('year', $year)->first();

                    if($dtr->time_out == null ){
                        $dtr->time_out = $time_in;
                        $dtr->save();
                        return 'timeout';
                    }else{
                        return 'Sorry.. You already have records for today! Try again tomorrow.';
                    }

                } catch (\Throwable $th) {
                    $dtr = new Date_time_record();
                    $dtr->instructors_id = $request->instructors_id;
                    $dtr->instructors_name = $instructor->name;
                    $dtr->time_in = $time_in;
                    $dtr->day = $day;
                    $dtr->month = $month;
                    $dtr->year = $year;
                    $dtr->save();
                    return 'timein';
                }

            }else{
                return "INVALID CREDENTIALS";
            }
        } catch (\Throwable $th) {
            return "DSS INVALID CREDENTIALS";
        }
        

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       

        $dtr = Date_time_record::where('instructors_id', $id)->first();
        return $dtr;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
