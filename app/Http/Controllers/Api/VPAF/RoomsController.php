<?php

namespace App\Http\Controllers\Api\VPAF;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VPAF\Room;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::orderBy('id', 'DESC')->where('archived', null)->get();
        return $rooms;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $room = new Room();
        $room->room_no = $request->room_no;
        $room->building = $request->building;
        $room->floor = $request->floor;
        $room->description = $request->description;
        $room->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $room = Room::find($id);       
        $room->room_no = $request->room_no;
        $room->building = $request->building;
        $room->floor = $request->floor;
        $room->description = $request->description;
        $room->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $room = Room::find($id);
        $room->archived = 'ARCHIVED';
        $room->save();
    }
    public function viewArchived()
    {
        $rooms = Room::where('archived', 'ARCHIVED')->get();
        return view('admin.archived_room')->with('rooms',$rooms);
    }
}
