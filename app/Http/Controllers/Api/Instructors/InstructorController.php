<?php

namespace App\Http\Controllers\Api\Instructors;

use App\Model\Instructors\Instructor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Bitfumes\Multiauth\Model\Role;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Bitfumes\Multiauth\Http\Requests\AdminRequest;
use App\Model\Log;
use App\Model\Instructors_log;
use App\Model\MIS\SchoolYear;
use App\Model\Registrar\College_application;
use Illuminate\Support\Facades\Hash;
use App\Model\VPAA\Schedule;
use App\Model\Date_time_record;
use App\Model\Grading\Grading_subject_load;
use App\Model\Grading\Grading_student;
use App\Model\Admin\Subject;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_dtr()
    {
        session_start();
        $email =  $_SESSION['email'];
        $instructor = Instructor::where('email', $email)->first();
        $instructors_id = $instructor->instructors_id;
        
        $instructor = Date_time_record::where('instructors_id', $instructors_id)->get();
        
        return view('instructors.my_dtr')->with('schedules', $instructor);
        return $instructor;
    }
    public function get_subject_loads($instructor_id)
    {
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   
        $schedule_by_instructor = Schedule::where('instructor_id', $instructor_id)->where('school_year', $school_year)->where('semester', $semester)->get();
        return $schedule_by_instructor;
    }

    public function view_by_instructor($instructor_id)
    {
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   
        $schedule_by_instructor = Schedule::where('instructor_id', $instructor_id)->where('school_year', $school_year)->where('semester', $semester)->get();

        return $schedule_by_instructor;
    }
    public function index()
    {
       
        $instructors = Instructor::orderBy('id', 'DESC')->where('archived', null)->get();
        return $instructors;
    }
    public function viewLogin(){   
       return view('instructors.login_instructor');
    }
    public function logout(){   
        session_start();
        $email =  $_SESSION['email'];
        $instructor = Instructor::where('email', $email)->first();
        session_destroy();
        $log = new Instructors_log();
        $log->instructors_id = "INSTRUCTOR";
        $log->action = 'LOGGED OUT';
        $log->details = "Name: " . $instructor->name;
        $log->save();
       return redirect( '/instructors/login' );
    }
    public function gradeSheet($id){   
        $sys = SchoolYear::latest()->first();
        session_start();

        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   


        try {
            $email =  $_SESSION['email'];
            $instructor = Instructor::where('email', $email)->first();
            if($email){
                $sys = SchoolYear::latest()->first();

        $subject_load = Grading_subject_load::where('archived', null)->where('id', $id)->first();
        $load;
            $subject = Subject::where('id', $subject_load->subject_id)->first();
            // $instructor = Instructor::where('instructors_id', $subject_load->instructor_id)->first();
            $load = [
                'id' => $subject_load->id,
                'instructor_id' => $subject_load->instructor_id,
                'instructor_name' => $instructor->name,
                'code' => $subject->code,
                'title' => $subject->title,
                'cys' => $subject_load->course . " " . $subject_load->year . "" . $subject_load->section
            ];
        $students = Grading_student::where('active', 'TRUE')->where('course', $subject_load->course)->where('year', $subject_load->year)->where('section', $subject_load->section)->get();
        // return $students;
        return view('instructors.gradesheet')->with('instructor', $instructor)->with('sys', $sys)->with('load', $load)->with('students', $students);
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!' . $th);
        }
    }

    public function view_home(){   
        $sys = SchoolYear::latest()->first();
        session_start();

        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   


        try {
            $email =  $_SESSION['email'];
            $instructor = Instructor::where('email', $email)->first();
            if($email){
                $sys = SchoolYear::latest()->first();
                
        $log = new Instructors_log();
        $log->instructors_id = "INSTRUCTOR";
        $log->action = 'LOGGED IN';
        $log->details = "Name: " . $instructor->name;
        $log->save();

        $subject_loads = Grading_subject_load::orderBy('updated_at', 'DESC')->where('archived', null)->where('instructor_id', $instructor->instructors_id)->get();
        $loads = [];
        foreach ($subject_loads as $key => $subject_load) {
            $subject = Subject::where('id', $subject_load->subject_id)->first();
            // $instructor = Instructor::where('instructors_id', $subject_load->instructor_id)->first();
            $loads[] = [
                'id' => $subject_load->id,
                'instructor_id' => $subject_load->instructor_id,
                'instructor_name' => $instructor->name,
                'code' => $subject->code,
                'title' => $subject->title,
                'cys' => $subject_load->course . " " . $subject_load->year . "" . $subject_load->section
            ];
        }
        // return $loads;
        return view('instructors.home')->with('instructor', $instructor)->with('sys', $sys)->with('loads', $loads);
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!' . $th);
        }
    }
    public function view_masterlist(){   
        session_start();
        try {
            $email =  $_SESSION['email'];
            if($email){
              return view('instructors.masterlist');
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!');
        }
    }
    public function view_criteria(){   
        session_start();
        try {
            $email =  $_SESSION['email'];
            if($email){
              return view('instructors.criteria');
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!');
        }
    }
    public function view_class_records(){   
        session_start();
        try {
            $email =  $_SESSION['email'];
            if($email){
              return view('instructors.grades');
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!');
        }
    }
    public function view_change_pass(){   
        session_start();
        try {
            $email =  $_SESSION['email'];
            if($email){
              return view('instructors.change-pass');
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!');
        }
    }

    public function validate_login(){   
        $sys = SchoolYear::latest()->first();
        // return 'okay';
        $email = $_POST['email'];
        $password = $_POST['password'];
        // return Hash::make($password);
        // return $email .' - '.$password;
        try {
            $instructor = Instructor::where('email', $email)->where('archived', null)->first();
            // return $instructor->password;
            //          // plain-text     hashed-old-pass
            if(Hash::check($password ,$instructor->password)){                
                session_start();
                $_SESSION['email'] = $email;
                // return $_SESSION['email'];
                return redirect( '/instructors/home' );
            }else{
                return redirect( '/instructors/login' )->with('error','Invalid Email or Password');
            }

            // return $instructor;
            if($instructor == null){
                return redirect( '/instructors/login' )->with('error','Invalid Email or Password');
            }elseif($instructor == '[]'){
                return redirect( '/instructors/login' )->with('error','Invalid Email or Password');
            }
            // return "valid";
           
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('error','Invalid Email or Password');
        }

   
         
    }

    
    

    public function viewBatchSections($course, $year, $section, $school_year, $semester){       

        try {
            $batch_sections = College_application::where('course', $course)->where('year', $year)->where('section', $section)->where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $semester)->get();
            
            return $batch_sections;
            // if($batch_sections){
            // }
            // return null;
        } catch (\Throwable $th) {
            return "NO STUDENTS ENROLLED";
        }
                
        }
    
        
        public function pdfBatchSections($course, $year, $section, $school_year, $semester){       
            // $sys = SchoolYear::latest()->first();
            // $school_year = $sys->start_year . '-' . $sys->end_year;
        // return $school_year;
                $students = College_application::where('course', $course)->where('year', $year)->where('section', $section)->where('application_status', 'Enrolled')->where('school_year', $school_year)->where('semester', $semester)->get();
                
                $pdf = PDF::loadView('pdf.student.student_list', compact('students', 'course', 'year', 'section', 'school_year', 'semester'));
                return $pdf->download('student_list.pdf');
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $instructors = new Instructor();
        $instructors->instructors_id = $request->instructors_id;
        $instructors->email = $request->email;
        $instructors->name = $request->name;
        $instructors->address = $request->address;
        $instructors->birthdate = $request->birthdate;
        $birthdate = $request->birthdate;
        $yearToday = date('Y');
        $birthdate = explode('/',$birthdate);
        $month = $birthdate[0];
        $day = $birthdate[1];
        $year = $birthdate[2];
        // $age = $yearToday - $year;
        // $instructors->age = $age;
        $instructors->contact_admin = $request->contact_admin;
        $instructors->contact_student = $request->contact_student;
        $instructors->educational_attainment = $request->educational_attainment;
        $instructors->description = $request->description;

        $instructors->employment_type = $request->employment_type;
        if($request->employment_type == 'PART-TIME'){
            $instructors->time = 'PART-TIME';
        }else{
            $instructors->time = $request->time;
        }
        //                       2019 1998
        $pass = $yearToday . $year;
        // return $pass;
        $instructors->password = Hash::make($pass);
        $instructors->save();

        
        // $log = new Instructors_log();
        // $log->instructors_id = $instructor->instructors_id;
        // $log->action = 'CREATED AN INSTRUCTOR';
        // $log->details = "Name: " . $request->name;
        // $log->save();
        
        $log = new Log();
        $log->admin_id = 'VPAF';
        $log->action = 'CREATED AN INSTRUCTOR';
        $log->details = "Name: " . $request->name;
        $log->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        
        $instructor = Instructor::where('name', $name)->orWhere('name', 'like', '%' . $name . '%')->get();
        return $instructor;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instructors = Instructor::find($id);       
        // $instructors->instructors_id = $request->instructors_id;
        $instructors->email = $request->email;
        $instructors->name = $request->name;
        $instructors->address = $request->address;
        $instructors->birthdate = $request->birthdate;
        $instructors->contact_admin = $request->contact_admin;
        $instructors->contact_student = $request->contact_student;
        $instructors->educational_attainment = $request->educational_attainment;
        $instructors->description = $request->description;
        $instructors->employment_type = $request->employment_type;
        if($request->employment_type == 'PART-TIME'){
            $instructors->time = 'PART-TIME';
        }else{
            $instructors->time = $request->time;
        }
        $instructors->save();
        
        $log = new Log();
        $log->admin_id = 'VPAF';
        $log->action = 'UPDATED AN INSTRUCTOR';
        $log->details = "Name: " . $request->name;
        $log->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instructors = Instructor::find($id);
        $instructors->archived = 'ARCHIVED';
        $instructors->save();

        $log = new Log();
        $log->admin_id = 'VPAF';
        $log->action = 'ARCHIVED AN INSTRUCTOR';
        $log->details = "Name: " . $instructors->name;
        $log->save();
    }
    public function viewInstructorsArchived()
    {
        $instructors = Instructor::where('archived', 'ARCHIVED')->get();
        return view('admin.archived_instructor')->with('instructors',$instructors);
    }
    public function updatePass(Request $request){
        session_start();
        try {
            $email =  $_SESSION['email'];
            if($email){
              
            $instructor = Instructor::where('email', $email)->where('password', $password)->where('archived', null)->first();
                $pass = $instructor->password;
        
            }
        } catch (\Throwable $th) {
            return redirect( '/instructors/login' )->with('warning','Permission Denied!');
        }

        $request->validate([
            'new_password' => 'required|min:6',
            'password_confirmation' => 'required|min:6'
        ]);

        // return $request;
        if (Hash::check($request->old_password, $pass)) {
            if($request->new_password == $request->password_confirmation){
                if ($request->new_password == $request->old_password) {
                    return redirect('/instructors/change-pass')->with('error', 'Submit a New Password!');
                }
                // redirect('admin/enrollment-procedure'); 
                $user = User::find($user_id);
                $user->password = Hash::make($request->new_password);
                $user->save();

                

                return redirect('/instructors/change-pass')->with('message', 'Password Updated!');
            }else{
                return redirect('/instructors/change-pass')->with('error', 'New Password & Confirmation Does not Match!');
            }
        }else{
            return redirect('/instructors/change-pass')->with('error', 'Invalid Old Password!');
        }       
    }
}
