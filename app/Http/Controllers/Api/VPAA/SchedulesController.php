<?php

namespace App\Http\Controllers\Api\VPAA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Instructors\Instructor;
use App\Model\VPAF\Room;
use App\Model\VPAA\Schedule;
use App\Model\MIS\SchoolYear;
use Illuminate\Support\Facades\DB;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::where('archive', null)->orderBy('id', 'DESC')->get();
        return $schedules;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $time_span = "";  
        $time_raw = "";
        $day_span = "";
        $instructor = explode(',',$request->instructor);
        $subject = explode('-',$request->subject);
        $room = explode('-',$request->room);
        $valid = 1;

        foreach ($request->time as $key => $value) {
            if($key == 0){
                $time_span = $value['name'];
            }else{
                $time_span .=  "," . $value['name']   ;
            }
        }
        foreach ($request->time as $key => $value) {
            if($key == 0){
                $time_raw = $value['raw'];
            }else{
                $time_raw .=  "," . $value['raw']   ;
            }
        }
        foreach ($request->day as $key => $value) {
            if($key == 0){
                $day_span = $value;
            }else{
                $day_span .=  "," . $value   ;
            }
        }

        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   


        
        $add_by_subject = $request->course.$request->year.$request->section.$subject[0].$school_year.$semester;
        $add_data = $request->course.$request->year.$request->section.$instructor[0].$subject[0].$school_year.$semester;


        $same_day = 0;
        $same_room = 0;


        //Validate Saved Schedules of the Section per time conflic
        $schedule_by_section = Schedule::where('course', $request->course)->where('year', $request->year)->where('section', $request->section)->where('school_year', $school_year)->where('semester', $semester)->get();
        foreach ($schedule_by_section as $key => $schedule) {
            $days = explode(',', $schedule->day);
             //iterate per day of data in database
             foreach ($days as $day) {
                foreach ($request->day as $r_value) {
                    if($day == $r_value){
                        $time = explode(',', $schedule->time_raw);
                        //iterate per time of day
                        foreach ($time as $value1) {
                            //search for occupied time of the day
                            foreach ($request->time as $value2) {
                                    if($value1 == $value2['raw']){
                                        $valid = 0;
                                        return 'This Section already have a schedule for this Day & Time.';
                                    }
                            }
                        }
                    }
                }
            }
        }
        // return 'TRUE';
        
        //Validate Saved Schedules of the Instructors per time conflic
        $schedule_by_instructor = Schedule::where('instructor_id', $instructor[0])->where('school_year', $school_year)->where('semester', $semester)->get();
        
        foreach ($schedule_by_instructor as $key => $schedule) {
            $days = explode(',', $schedule->day);
             //iterate per day of data in database
             foreach ($days as $day) {
                foreach ($request->day as $r_value) {
                    if($day == $r_value){
                        $time = explode(',', $schedule->time_raw);
                        //iterate per time of day
                        foreach ($time as $value1) {
                            //search for occupied time of the day
                            foreach ($request->time as $value2) {
                                    if($value1 == $value2['raw']){
                                        $valid = 0;
                                        return 'This Instructor already have a schedule for this Day & Time.';
                                    }
                            }
                        }
                    }
                }
            }
        }
        // return $schedule_by_instructor;

        //check per schedule
        $check = Schedule::where('archive', null)->get();
        foreach ($check as $key => $schedule) {
            $data = $schedule->course.$schedule->year.$schedule->section.$schedule->subject_code.$schedule->school_year.$schedule->semester;
            if($data == $add_by_subject){
                $valid = 0;
                return 'This Section already have a schedule for this subject.';
            }
            $data = $schedule->course.$schedule->year.$schedule->section.$schedule->instructor_id.$schedule->subject_code.$schedule->school_year.$schedule->semester;

            if($data == $add_data){
                $valid = 0;
                return 'Invalid Section/Instructor/Subject';
            }

            $days = explode(',', $schedule->day);
            $num_of_days =  count($days);
            //if same building and room
            if(($room[0] . $room[1]) == ($schedule->room_building . $schedule->room_no)){
                 //iterate per day of data in database
                foreach ($days as $day) {
                    foreach ($request->day as $r_value) {
                        if($day == $r_value){
                            $same_day++;   
                            $time = explode(',', $schedule->time_raw);
                            //iterate per time of day
                            foreach ($time as $value1) {
                                //search for occupied time of the day
                                foreach ($request->time as $value2) {
                                        if($value1 == $value2['raw']){
                                            $valid = 0;
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }



        if($valid == 1){
            $schedules = new Schedule();
            $schedules->course = $request->course;
            $schedules->year = $request->year;
            $schedules->section = $request->section;
    
            $schedules->subject_code = $subject[0];
            $schedules->subject_description = $subject[1];
    
            $schedules->semester = $semester;
            $schedules->school_year = $school_year;
    
            $schedules->room_building = $room[0];
            $schedules->room_no = $room[1];
            $schedules->room_description = $room[2];
    
            $schedules->instructor_id = $instructor[0];
            $schedules->instructor_name = $instructor[1];
            $schedules->time = $time_span;
            $schedules->time_raw = $time_raw;
            $schedules->day = $day_span;
            $schedules->color = $request->color;
            $schedules->save();
            return "TRUE";
        }else{
            return "This Schdule is not avialable. The Room is Occupied.";
        }

      
        return $schedules;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;
        $schedules = Schedule::where('instructor_name', $name)->orWhere('instructor_name', 'like', '%' . $name . '%')->get();
        return $schedules;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $time_span = "";  
        $time_raw = "";
        $day_span = "";
        $instructor = explode(',',$request->instructor);
        $subject = explode('-',$request->subject);
        $room = explode('-',$request->room);
        $valid = 1;

        foreach ($request->time as $key => $value) {
            if($key == 0){
                $time_span = $value['name'];
            }else{
                $time_span .=  "," . $value['name']   ;
            }
        }
        foreach ($request->time as $key => $value) {
            if($key == 0){
                $time_raw = $value['raw'];
            }else{
                $time_raw .=  "," . $value['raw']   ;
            }
        }
        foreach ($request->day as $key => $value) {
            if($key == 0){
                $day_span = $value;
            }else{
                $day_span .=  "," . $value   ;
            }
        }

        $sys = SchoolYear::latest()->first();
        $school_year = $sys->start_year . '-' . $sys->end_year;
        $semester = $sys->semester;   


        
        $add_by_subject = $request->course.$request->year.$request->section.$subject[0].$school_year.$semester;
        $add_data = $request->course.$request->year.$request->section.$instructor[0].$subject[0].$school_year.$semester;


        $same_day = 0;
        $same_room = 0;


        //Validate Saved Schedules of the Section per time conflic
        $schedule_by_section = Schedule::where('course', $request->course)->where('year', $request->year)->where('section', $request->section)->where('school_year', $school_year)->where('semester', $semester)->get();
        foreach ($schedule_by_section as $key => $schedule) {
            $days = explode(',', $schedule->day);
             //iterate per day of data in database
             foreach ($days as $day) {
                foreach ($request->day as $r_value) {
                    if($day == $r_value){
                        $time = explode(',', $schedule->time_raw);
                        //iterate per time of day
                        foreach ($time as $value1) {
                            //search for occupied time of the day
                            foreach ($request->time as $value2) {
                                    if($value1 == $value2['raw']){
                                        $valid = 0;
                                        return 'This Section already have a schedule for this Day & Time.';
                                    }
                            }
                        }
                    }
                }
            }
        }
        // return 'TRUE';
        
        //Validate Saved Schedules of the Instructors per time conflic
        $schedule_by_instructor = Schedule::where('instructor_id', $instructor[0])->where('school_year', $school_year)->where('semester', $semester)->get();
        
        foreach ($schedule_by_instructor as $key => $schedule) {
            $days = explode(',', $schedule->day);
             //iterate per day of data in database
             foreach ($days as $day) {
                foreach ($request->day as $r_value) {
                    if($day == $r_value){
                        $time = explode(',', $schedule->time_raw);
                        //iterate per time of day
                        foreach ($time as $value1) {
                            //search for occupied time of the day
                            foreach ($request->time as $value2) {
                                    if($value1 == $value2['raw']){
                                        $valid = 0;
                                        return 'This Instructor already have a schedule for this Day & Time.';
                                    }
                            }
                        }
                    }
                }
            }
        }
        // return $schedule_by_instructor;

        //check per schedule
        $check = Schedule::where('archive', null)->get();
        foreach ($check as $key => $schedule) {
            $data = $schedule->course.$schedule->year.$schedule->section.$schedule->subject_code.$schedule->school_year.$schedule->semester;
            if($data == $add_by_subject){
                $valid = 0;
                return 'This Section already have a schedule for this subject.';
            }
            $data = $schedule->course.$schedule->year.$schedule->section.$schedule->instructor_id.$schedule->subject_code.$schedule->school_year.$schedule->semester;

            if($data == $add_data){
                $valid = 0;
                return 'Invalid Section/Instructor/Subject';
            }

            $days = explode(',', $schedule->day);
            $num_of_days =  count($days);
            //if same building and room
            if(($room[0] . $room[1]) == ($schedule->room_building . $schedule->room_no)){
                 //iterate per day of data in database
                foreach ($days as $day) {
                    foreach ($request->day as $r_value) {
                        if($day == $r_value){
                            $same_day++;   
                            $time = explode(',', $schedule->time_raw);
                            //iterate per time of day
                            foreach ($time as $value1) {
                                //search for occupied time of the day
                                foreach ($request->time as $value2) {
                                        if($value1 == $value2['raw']){
                                            $valid = 0;
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }



        if($valid == 1){
            $schedules = Schedule::find($id);
            $schedules->course = $request->course;
            $schedules->year = $request->year;
            $schedules->section = $request->section;
    
            $schedules->subject_code = $subject[0];
            $schedules->subject_description = $subject[1];
    
            $schedules->semester = $semester;
            $schedules->school_year = $school_year;
    
            $schedules->room_building = $room[0];
            $schedules->room_no = $room[1];
            $schedules->room_description = $room[2];
    
            $schedules->instructor_id = $instructor[0];
            $schedules->instructor_name = $instructor[1];
            $schedules->time = $time_span;
            $schedules->time_raw = $time_raw;
            $schedules->day = $day_span;
            $schedules->color = $request->color;
            $schedules->save();
            return "TRUE";
        }else{
            return "This Schdule is not avialable. The Room is Occupied.";
        }

        // $schedules->time = $time_span;
        // $schedules->time_raw = $time_raw;
        // $schedules->day = $day_span;
        // $schedules->color = $request->color;
        // $schedules->save();
        // return $schedules;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedules = Schedule::find($id);
        $schedules->archived = 'ARCHIVED';
        $schedules->save();
    }
}
