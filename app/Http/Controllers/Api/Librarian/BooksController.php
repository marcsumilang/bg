<?php

namespace App\Http\Controllers\Api\Librarian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Log;
use App\Model\Librarian\Book;

use App\Model\MIS\SchoolYear;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::where('archived', null)->orderBy('id', "DESC")->get();
        return $books;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book();
        $book->book_code = $request->book_code;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author = $request->author;
        $book->category = $request->category;
        $book->publisher = $request->publisher;
        $book->date_published = $request->date_published;
        $book->qty = $request->qty;
        $book->no_of_stock = $request->qty;

        $book->no_of_borrowed = '0';
        $book->no_of_damaged = '0';
        $book->no_of_views = '0';
        $book->times_borrowed = '0';
        $book->save();
        // return "book"
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = new Book($id);
        return $book;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $book = Book::find($id);
        $book->book_code = $request->book_code;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author = $request->author;
        $book->category = $request->category;
        $book->publisher = $request->publisher;
        $book->date_published = $request->date_published;

        $book->no_of_stock = (int)$book->no_of_stock + (int)$request->qty;

        $book->qty = (int)$book->qty + (int)$request->qty;
        $book->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->archived = "ARCHIVED";
        $book->save();
    }
}
