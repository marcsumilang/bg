<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'course' => $this->course,
            'year' => $this->year,
            'semester' => $this->semester,
            'code' => $this->code,
            'title' => $this->title,
            'description' => $this->description,
            'units' => $this->units,
            'prerequisite' => $this->prerequisite
        ];
    }
}
