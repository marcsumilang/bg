-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2019 at 12:21 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super@admin.com', 9067612358, '$2y$10$j2eg4DGMq0Of07VIMdzUP.RJ2766sNwClO9qI7R7x6Gy0yIEheJJa', 'XBsOOuZ7foEwX94cg8j9fTC49L54l0BZwopGQJP7rqTnrzjDqVamlMoCw2Xv', '2019-03-07 15:32:53', '2019-03-07 16:41:28'),
(3, 'Maam Anna', 'registrar@bulacanpolytechniccollege.org', 9559371500, '$2y$10$j2eg4DGMq0Of07VIMdzUP.RJ2766sNwClO9qI7R7x6Gy0yIEheJJa', 'aIROCRgm8TFUNzBiJxjj5HEfcX5WWNfTF9UjJNXdjIZPUBEVKjVvNujbJL0v', '2019-03-07 16:34:48', '2019-03-07 19:14:46'),
(4, 'Dr. Rosemarie S. Guirre', 'mis@bulacanpolytechniccollege.org', 9559371500, '$2y$10$eEUyJonyiof0NY.3dSg4b.MS6FWVR/.aBw6thTiUzP8aw99qmZRkK', 'p4NeBFeoZoJnxSFuUi2P9qY0kNKDjaAemL5qItJtgLK8mXdTxvYxrB2n75Mo', '2019-03-07 16:35:33', '2019-03-07 16:40:22'),
(5, 'Engr. Arman Giron', 'vpaf@bulacanpolytechniccollege.org', 9559371500, '$2y$10$iI2smMNFocu2/jHu1Er4TOW4nozxvCw0yP2U/K9PYcLqRXtVX/P5i', 'YKcYQWLttrc3l7V66axJaTVBwiVfsgZeqWhhcegmFZ5wvN4jOuvoeK0IDo1w', '2019-03-11 21:47:05', '2019-03-11 21:47:05'),
(6, 'Mam Beth', 'vpaa@bulacanpolytechniccollege.org', 9559371500, '$2y$10$bFaAHO8.sB.fpF5mE0cXyevLUbiC90ZuuUNwfqKcKCgg9TaEHhjG2', 'GQufYhh4qN3JCaUufjBGvmRtMR7tRIZYkXkVoiAF4jgXJRDkeDa79Zjyx1Gd', '2019-03-11 21:47:46', '2019-03-11 21:47:46'),
(9, 'Minerva Magbitang', 'inventory@bulacanpolytechniccollege.org', 9559371500, '$2y$10$2mOJ5sTQ7zcDZ9rx7q6Qt.M53XAZ00JC1/jRMxykmuid3ee/ixnp6', NULL, '2019-05-24 19:14:23', '2019-05-24 19:14:23'),
(10, 'Jazmine Sumilang', 'czerenah@bpc.org', 9559371500, '$2y$10$24v.9xjldHosZbBQFflXJ.eoXEp2bbOfTIwCkRO4/6ruCTKhyWZd2', NULL, '2019-05-24 19:14:51', '2019-05-24 19:14:51'),
(11, 'Rebecca Pascual', 'bpc_admin@gmail.com', 9559371500, '$2y$10$HV59U4Pyq4sAgbca9WJw9uwt7Ckq.u9oNQQCLUz5Ej45HVM.j/Sym', NULL, '2019-05-24 19:15:29', '2019-05-24 19:15:29'),
(12, 'Jeff Raymundo', 'jeff@bulacanpolytechniccollege.org', 9559371500, '$2y$10$cVvFyMPc3/l9MqfcOoqv3en3VtNEBPg2/daQDDWNWBW/q89xgniW2', NULL, '2019-05-24 19:16:16', '2019-05-24 19:16:16'),
(13, 'Edguardo Villiafuerte', 'ed@bulacanpolytechniccollege.org', 9559371500, '$2y$10$4n00dwiJFf/u4cr3DXl21eTeB1I4XGB/boG02chydSaaoo5lxvaym', NULL, '2019-05-24 19:17:11', '2019-05-24 19:17:11');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `role_id`, `admin_id`) VALUES
(1, 1, 1),
(3, 2, 3),
(5, 3, 4),
(6, 5, 5),
(7, 4, 6),
(10, 7, 9),
(11, 6, 10),
(12, 8, 11),
(13, 11, 12),
(14, 13, 13);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_published` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_stock` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_borrowed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_damaged` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_views` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `times_borrowed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `book_code`, `title`, `description`, `author`, `publisher`, `date_published`, `category`, `qty`, `no_of_stock`, `no_of_borrowed`, `no_of_damaged`, `no_of_views`, `times_borrowed`, `picture`, `archived`, `created_at`, `updated_at`) VALUES
(1, 'qwe1', 'qwe1', 'qwe1', 'qwe1', 'qwe1', 'qwe1', 'qwe1', '1231', NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED', '2019-03-16 00:18:58', '2019-03-16 00:51:21'),
(2, 'qwe', 'qwe', NULL, 'qwe', 'qwe', 'qwe', 'qwe', '14', '13', '0', '0', '0', '0', NULL, NULL, '2019-03-16 00:20:59', '2019-03-16 01:51:30');

-- --------------------------------------------------------

--
-- Table structure for table `college_applications`
--

CREATE TABLE `college_applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_school` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_graduated` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_school_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_returnee_transferee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regular_irregular` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nso` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `good_moral` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ncae` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interview_slip` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `honorable_dismissal` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eog` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clearance` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cog` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `miscellaneous` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuition` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `envelope` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_enrolled` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `college_applications`
--

INSERT INTO `college_applications` (`id`, `user_id`, `application_status`, `application_date`, `student_id`, `course`, `year`, `section`, `first_name`, `middle_name`, `last_name`, `initial`, `birthdate`, `street`, `barangay`, `city`, `zipcode`, `province`, `sex`, `civil_status`, `contact`, `email`, `last_school`, `year_graduated`, `last_school_address`, `father_name`, `father_occupation`, `father_address`, `mother_name`, `mother_occupation`, `mother_address`, `guardian_name`, `guardian_occupation`, `guardian_address`, `guardian_contact`, `new_returnee_transferee`, `regular_irregular`, `requirements`, `picture`, `card`, `nso`, `good_moral`, `ncae`, `interview_slip`, `tor`, `honorable_dismissal`, `eog`, `clearance`, `cog`, `miscellaneous`, `tuition`, `envelope`, `semester`, `school_year`, `date_enrolled`, `created_at`, `updated_at`) VALUES
(5, '5', 'Enrolled', '3/9/2019', '2018-1002', 'BSIS', '1', 'B', 'Naruto', 'Raymundo', 'Uzumaki', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'marcjhericosumilang@gmail.com2', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-08 23:49:15', '2019-03-08 22:41:01', '2019-03-08 23:49:09'),
(6, '6', 'Enrolled', '3/9/2019', '2018-1001', 'BSIS', '1', 'C', 'Itachi', 'Raymundo', 'Uchiha', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'marcjhericosumilang@gmail.com41', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 00:17:05', '2019-03-08 23:11:42', '2019-03-10 00:17:05'),
(7, '7', 'Enrolled', '3/9/2019', '2018-1001', 'BSIS', '1', 'C', 'Marc', 'Raymundo', 'Sumilang', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'marcjhericosumilang@gmail.comzx', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 08:29:27', '2019-03-09 00:11:08', '2019-03-10 00:29:27'),
(9, '1', 'Enrolled', '3/9/2019', '2018-1003', 'BSIS', '1', 'C', 'Sasuke', 'Raymundo', 'Uchiha', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'Sasuke@gmail.com', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 00:17:05', '2019-03-08 23:11:42', '2019-03-10 00:17:05'),
(10, '2', 'Enrolled', '3/9/2019', '2018-1004', 'BSIS', '1', 'C', 'Sakura', 'Raymundo', 'Uchiha', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'Sakura@gmail.com', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 00:17:05', '2019-03-08 23:11:42', '2019-03-10 00:17:05'),
(11, '3', 'Enrolled', '3/9/2019', '2018-1005', 'BSIS', '1', 'C', 'Gaara', 'Raymundo', 'Uchiha', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'Gaara@gmail.com', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 00:17:05', '2019-03-08 23:11:42', '2019-03-10 00:17:05'),
(12, '4', 'Ongoing', '3/9/2019', '2018-1006', 'BSIS', '1', 'C', 'Shikamaru', 'Raymundo', 'Uchiha', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'Shikamaru@gmail.com', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '2018-2019', '2019-03-10 00:17:05', '2019-03-08 23:11:42', '2019-03-10 00:17:05'),
(13, '10', 'Enrolled', '3/18/2019', '2019-0001', 'BSIS', '1', 'A', 'Marc', 'Rabang', 'Sumilang', 'R', '06/26/1998', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos City', '3000', 'Bulacan', 'male', 'single', '09559371500', 'marcjhericosumilang@gmail.com212', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', NULL, NULL, NULL, NULL, NULL, NULL, 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '0', '1', '1', '2019-2020', '2019-03-18 04:49:44', '2019-03-17 15:38:31', '2019-03-18 17:02:16'),
(14, '21', 'Enrolled', '3/19/2019', '2019-0003', 'BSIS', '1', 'A', 'Marc', 'Raymundo', 'Sumilang', 'R', '1998-06-26T03:22:00.000Z', 'Blk 5 lot 16 Regata Subd', 'Sumpang Matanda', 'Malolos', '3000', 'Bulacan', 'male', 'single', '09559371500', 'marcjhericosumilang@gmail.com', 'Mary the Queen School of Malolos', '2014', 'Sto Cristo Malolos Bulacan', 'Enrique Sumilang III', 'SAP Administrator', 'same', 'Marian Sumilang', 'None', 'same', 'Marian Sumilang', 'None', 'same', '09067612358', 'New', 'Regular', 'incomplete', '1', '1', '1', NULL, NULL, NULL, 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '0', '0', NULL, '1', '2019-2020', '2019-03-19 08:08:04', '2019-03-19 07:24:42', '2019-03-19 08:08:04'),
(15, '22', 'Enrolled', '3/19/2019', '2019-5', 'BSIS', '1', 'A', 'Jeremiah', 'Samson', 'Fernandez', 'S.', '1979-06-06T03:20:00.000Z', 'Sitio 5', 'Pungo', 'Calumpit', '3303', 'Bulacan', 'male', 'single', '09559364209', 'jeremiahfern06@gmail.com', 'CanioGan High School', '2015', 'Caniogan, Calumpit, Bulacan', 'MarCelo R. Fernandez', 'None', 'Pungo, Calumpit, Bulacan', 'RhOdora S. FernAndez', 'None', 'Pungo, CAluMpit, BULACAN', 'RHODORA', 'None', 'PUNGO', '079484838$8##\";$-#-#-*', 'New', 'Regular', 'incomplete', '1', '1', '1', NULL, NULL, NULL, 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '0', '0', NULL, '1', '2019-2020', '2019-03-19 08:48:13', '2019-03-19 07:30:07', '2019-03-19 08:48:13'),
(19, '24', 'Enrolled', '3/19/2019', '2019-6', 'BSIS', '1', 'A', 'Beatrisse Julia', 'Cuenca', 'Bantug', 'c', '1998-07-15T04:53:00.000Z', 'Raymundo Subd.', 'Sto. Nino', 'Hagonoy', '3002', 'Bulacan', 'female', 'single', '09262514244', 'juliabeatrisse98@gmail.com', 'Saint Anne\'s Catholic School', '2014', 'Poblacion Hagonoy Bulacan', 'Ferdinand Bantug', 'Security Guard', 'Hagonoy Bulacan', 'Carmina Bantug', 'House Wife', 'Hagonoy Bulacan', 'Ferdinand Bantug', 'Security Guard', 'Hagonoy Bulacan', '09056889224', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-03-19 08:58:58', '2019-03-19 08:57:47', '2019-03-19 08:58:59'),
(20, '25', 'Ongoing', '3/19/2019', NULL, 'BSIS', '1', NULL, 'Joshua', 'Logrosa', 'Mangali', 'L.', '1998-10-13T04:57:00.000Z', 'khgdsjkgl', 'San Pablo', 'Malolos', '3000', 'Bulacan', 'male', 'single', '09654333922', 'logrosajoshua1013@gmail.com', 'Meycauayan National Highschool', '2015', 'Camalig, Meycauayan, Bulacan', 'Reginald R. Mangali', 'Deceased', 'Malolos, Bulacan', 'Goldamier S. Logrosa', 'Sales Lady', 'Malolos, Bulacan', 'Goldamier S. Logrosa', 'Sales Lady', 'Malolos, Bulacan', '09655467821', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', NULL, '1', '2019-2020', NULL, '2019-03-19 09:01:20', '2019-03-19 09:01:20'),
(21, '26', 'Enrolled', '3/19/2019', '2019-11', 'BSIS', '1', 'A', 'Roanne', 'Tancinco', 'Capule', 'T.', '1998-07-21T05:19:00.000Z', '174 Sevilla St.', 'Atlag', 'Hagonoy', '3000', 'Bulacan', 'female', 'single', '09356701894', 'rcapule049410@gmail.com', 'Nuestra Senora del Carmen Institute', '2018', 'Km. 38 Pulong Buhangin, Sta. Maria, Bulacan', 'Robert Capule', 'Carpenter', '174 Sevilla St. Atlag, Malolos, Bulacan', 'Marianne Capule', 'MR Staff', '049 Partida Highway, Norzagaray, Bulacan', 'Robert Capule', 'Carpenter', '174 Sevilla St. Atlag, Malolos, Bulacan', '09363463157', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-05-24 07:01:49', '2019-03-19 09:27:35', '2019-05-24 19:01:49'),
(22, '26', 'Ongoing', '3/19/2019', NULL, 'BSIS', '1', NULL, 'Roanne', 'Tancinco', 'Capule', 'T.', '1998-07-21T05:19:00.000Z', '174 Sevilla St.', 'Atlag', 'Hagonoy', '3000', 'Bulacan', 'female', 'single', '09356701894', 'rcapule049410@gmail.com', 'Nuestra Senora del Carmen Institute', '2018', 'Km. 38 Pulong Buhangin, Sta. Maria, Bulacan', 'Robert Capule', 'Carpenter', '174 Sevilla St. Atlag, Malolos, Bulacan', 'Marianne Capule', 'MR Staff', '049 Partida Highway, Norzagaray, Bulacan', 'Robert Capule', 'Carpenter', '174 Sevilla St. Atlag, Malolos, Bulacan', '09363463157', 'New', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', NULL, '1', '2019-2020', NULL, '2019-03-19 09:27:35', '2019-03-19 09:27:35'),
(23, '28', 'Enrolled', '3/19/2019', '2019-7', 'BSIS', '1', 'A', 'Paulo', 'Agustin', 'Victoria', 'A.', '1995-04-02T05:57:00.000Z', '61 Purok 6', 'Kapitangan', 'Paombong', '3001', 'Bulacan', 'male', 'married', '09439164305', 'vpaulo1312@gmail.com', 'bpc', '2004', 'bulihan', 'conrado', 'deceased', 'same', 'feliza', 'wala', 'same', 'same', 'wala', 'same', '0943000002211222223222', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-03-19 10:17:07', '2019-03-19 10:09:30', '2019-03-19 10:17:07'),
(24, '29', 'Enrolled', '3/19/2019', '2019-8', 'BSIS', '1', 'A', 'Minerva', 'Villafuerte', 'Magbitang', 'V.', '1987-09-15T05:58:00.000Z', '469', 'Sta. Elena', 'Hagonoy', '3002', 'Bulacan', 'female', 'married', '09367619507', 'magbitangminerva@gmail.com', 'Bulacan State University', '2011', 'Malolos Bulacan', 'Fulgencio Magbitang', 'n/a', 'Sta. Elena Hagonoy Bulacan', 'Leonida', 'Deceased', 'Sta. Elena Hagonoy Bulacan', 'Guardian Angel', 'Security Guard', 'Sta. Elena Hagonoy Bulacan', '09367619507', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-03-19 10:20:09', '2019-03-19 10:09:41', '2019-03-19 10:20:09'),
(25, '30', 'Enrolled', '4/12/2019', '2019-9', 'BSIS', '1', 'A', 'Marc Jefferson', 'Raymundo', 'Sumilang', 'R', '06/26/1999', 'Blk 5 Lot 16 Regata Subd', 'Sumapang Matanda', 'Malolos', '3000', 'Bulacan', 'male', 'single', '09067612358', 'swordsmanmarc@gmail.com', 'BPC', '2019', 'Bulihan Malolos Bulacan', 'Enrique Sumilang III', 'SAP Admin', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', 'Marian Sumilang', 'None', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', 'Marian Sumilang', 'None', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', '09559371500', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-04-12 07:05:06', '2019-04-12 07:01:33', '2019-04-12 07:05:06'),
(26, '39', 'Enrolled', '4/12/2019', '2019-10', 'BSIS', '1', 'A', 'Marc Jefferson', 'Raymundo', 'Sumilang', 'R', '12/12/1999', 'Blk 5 Lot 16 Regata Subd', 'Sumapang Matanda', 'Malolos', '3000', 'Bulacan', 'male', 'single', '09067612358', 'sumilangmarc1999@gmail.com', 'BPC', '2019', 'Bulihan Malolos Bulacan', 'Enrique Sumilang III', 'SAP Admin', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', 'Marian Sumilang', 'None', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', 'Marian Sumilang', 'None', 'Blk 5 Lot 16 Regata Subd Sumapang Matanda, Malolos, Bulacan 3000', '09559371500', 'New', 'Regular', 'incomplete', '1', '1', '1', '1', '1', '1', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', '1', '1', '1', '1', '2019-2020', '2019-04-12 08:55:25', '2019-04-12 08:54:16', '2019-04-12 08:55:25'),
(27, '48', 'Ongoing', '8/9/2019', NULL, 'BSIS', '4', NULL, 'Charles', 'Francisco', 'Baltazar', 'F.', '10/11/91', 'Bonifacio St.', 'Barihan', 'Malolos', '3000', 'Bulacan', 'male', 'married', '09433359287', 'popeyeyel@gmail.com', 'Marcelo H del Pilar National High School', '2008', 'City of Malolos, Bulacan', 'Carlo Baltazar', 'Electrician', 'Bonifacio St. Barihan, Malolos, Bulacan 3000', 'Neth Baltazar', 'None', 'Bonifacio St. Barihan, Malolos, Bulacan 3000', 'Carlo Baltazar', 'Electrician', 'Bonifacio St. Barihan, Malolos, Bulacan 3000', '09433359287', 'Returnee', 'Regular', 'incomplete', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', 'To be Submitted', NULL, '1', '2019-2020', NULL, '2019-08-08 22:38:49', '2019-08-08 22:38:49');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_code`, `description`, `year`, `sections`, `archived`, `created_at`, `updated_at`) VALUES
(1, 'HRS', 'Hotel Restaurant', '2', '5,7', NULL, '2019-03-12 16:00:00', '2019-03-14 03:56:30'),
(2, 'CCS', 'Contact Center Services', '2', '2,2', NULL, '2019-03-12 20:38:34', '2019-03-14 03:56:19'),
(3, 'BSIS', 'Bachelor of Science in Information Systems', '4', '3,2,2,1', NULL, '2019-03-14 03:53:46', '2019-03-14 03:56:08'),
(4, 'BSOM', 'Bachelor of Science in Information Systems', '4', '2,2,2,2', NULL, '2019-03-14 04:01:47', '2019-03-14 04:01:47'),
(5, 'BTVTED', 'Bachelor of Science in Information Systems', '4', '1,2,2,2', NULL, '2019-03-14 04:02:09', '2019-03-14 04:02:09'),
(6, 'BSAIS', 'Bachelor of Science in Information Systems', '4', '2,1,1,3', NULL, '2019-03-14 04:02:31', '2019-03-14 04:02:31');

-- --------------------------------------------------------

--
-- Table structure for table `date_time_records`
--

CREATE TABLE `date_time_records` (
  `id` int(10) UNSIGNED NOT NULL,
  `instructors_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructors_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in` timestamp NULL DEFAULT NULL,
  `time_out` timestamp NULL DEFAULT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `date_time_records`
--

INSERT INTO `date_time_records` (`id`, `instructors_id`, `instructors_name`, `time_in`, `time_out`, `day`, `month`, `year`, `remarks`, `created_at`, `updated_at`) VALUES
(7, '2012-00288', 'Sheldon V. Arenas', '2019-05-22 20:01:03', NULL, '22', '5', '2019', NULL, '2019-05-22 12:01:03', '2019-05-22 12:01:03'),
(8, '2018-8672', 'Mylene Rose V. De Guzman', '2019-05-22 20:02:24', NULL, '22', '5', '2019', NULL, '2019-05-22 12:02:24', '2019-05-22 12:02:24'),
(9, '2008-00121', 'Anna Melisa', '2019-05-22 20:04:10', NULL, '22', '5', '2019', NULL, '2019-05-22 12:04:10', '2019-05-22 12:04:10'),
(10, '2018-020225', 'Marian Sumilang', '2019-05-25 03:19:16', '2019-05-25 03:19:48', '24', '5', '2019', NULL, '2019-05-24 19:19:16', '2019-05-24 19:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_procedures`
--

CREATE TABLE `enrollment_procedures` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enrollment_procedures`
--

INSERT INTO `enrollment_procedures` (`id`, `level`, `description`, `created_at`, `updated_at`) VALUES
(1, 'College', 'asdasd', '2019-03-06 16:00:00', '2019-03-06 16:00:00'),
(2, 'College', '<p>asdasdssssss</p>', '2019-03-07 19:26:27', '2019-03-07 19:26:27'),
(3, 'College', '<p><strong>ENROLLMENT PROCEDURES<br />\r\nCOLLEGE (1st, 2nd, 3rd&nbsp;and 4th&nbsp;Year)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 1 - VERIFICATION OF GRADES (incoming 2nd year only)</strong><br />\r\nProceed to&nbsp;<strong>Room 201 or Room 202</strong>&nbsp;and present your complete class cards and evaluation of grades for verification of grades.</p>\r\n\r\n<p><strong>STEP 2 - PAYMENT FOR PARTIAL MISCELLANOUS FEES</strong><br />\r\nProceed to&nbsp;<strong>Room 203</strong>&nbsp;to pay for the following miscellaneous fees:</p>\r\n\r\n<ul>\r\n	<li>Sentry - Php 50</li>\r\n	<li>Guidance Fee - Php 50</li>\r\n	<li>SG Fee - Php 50</li>\r\n	<li>PTCA - Php 50</li>\r\n	<li>Club Fee (all courses) - Php 50</li>\r\n	<li>Insurance Fee - Php 30</li>\r\n	<li>ALCU Fee - Php 50</li>\r\n	<li>Total Php 330</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 3 - REGISTRATION</strong><br />\r\nPresent all the requirements listed below in Table 1, 2, or 3 at Rm. 204 &amp; 205 and avail a green card/registration form.<br />\r\nHIGH SCHOOL GRADUATES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1X1 colored picture</li>\r\n	<li>High School Card (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Good Moral Certificate (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Long brown envelope</li>\r\n	<li>Interview Slip (signed by the Interviewer)</li>\r\n</ul>\r\n\r\n<p>TRANSFEREES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1x1 colored picture</li>\r\n	<li>Original Copy - Certificate of Grades or Transcript of Records (TOR) (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Transfer Credentials &amp; Honorable Dismissal Cert. (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p>RETURNEES</p>\r\n\r\n<ul>\r\n	<li>2 pcs. 1x1 colored picture</li>\r\n	<li>Student Clearance (fully accomplished)</li>\r\n	<li>Copy of Grades</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p><strong>STEP 4 - ENLISTMENT OF SUBJECTS AND VALIDATION</strong></p>\r\n\r\n<ol>\r\n	<li>Proceed to the following designated room below and copy the list of subjects.<br />\r\n	CS, BSOM, BSIS and CCM -&nbsp;<strong>Rm. 104</strong><br />\r\n	HRS, CIT and DTS -&nbsp;<strong>Rm. 105</strong></li>\r\n	<li>Ensure that the registration form is validated and signed by the evaluator.</li>\r\n</ol>\r\n\r\n<p><strong>STEP 5 - ASSESSMENT</strong><br />\r\nPresent the fully accomplished registration form at&nbsp;<strong>Rm. 106</strong>&nbsp;for assessment of tuition fees and the rest of miscellaneous fees</p>\r\n\r\n<p><strong>STEP 6 - PAYMENT TO PGB CASHIERS</strong><br />\r\nPay for tuition fee and the rest of miscellaneous fees in Windows 1, 2 or 3 at&nbsp;<strong>Rm. 108</strong>&nbsp;or at the&nbsp;<strong>Provincial Treasurer&#39;s Office in the Provincial Capitol</strong>.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Registration Form - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Laboratory Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sports and Cultural Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Library Fee - Php 30<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Total - Php 180 plus Tuition Fee</strong><br />\r\nTo those who opted to pay at Rm. 108, the left side of the court (with chairs provided) will serve as their waiting area.</p>\r\n\r\n<p><strong>STEP 7 - REGISTRAR&#39;S APPROVAL AND ISSUANCE OF CLASSCARDS</strong><br />\r\nProceed to the provided chairs at the right side of the BPC Covered Court and have the registration form be approved by the Registrar at&nbsp;<strong>Science Laboratory</strong>&nbsp;and avail the class cards from the assigned personnel.</p>\r\n\r\n<p><strong>STEP 8 - APPLICATION FOR BPC ID</strong><br />\r\nProceed to&nbsp;<strong>MIS Office (second floor)</strong>&nbsp;and pay Php 55.</p>', '2019-03-18 17:13:37', '2019-03-18 17:13:37'),
(4, 'College', '<p><strong>ENROLLMENT PROCEDURES<br />\r\nCOLLEGE (1st, 2nd, 3rd&nbsp;and 4th&nbsp;Year)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 1 - VERIFICATION OF GRADES (incoming 2nd year only)</strong><br />\r\nProceed to&nbsp;<strong>Room 201 or Room 202</strong>&nbsp;and present your complete class cards and evaluation of grades for verification of grades.</p>\r\n\r\n<p><strong>STEP 2 - PAYMENT FOR PARTIAL MISCELLANOUS FEES</strong><br />\r\nProceed to&nbsp;<strong>Room 203</strong>&nbsp;to pay for the following miscellaneous fees:</p>\r\n\r\n<ul>\r\n	<li>Sentry - Php 50</li>\r\n	<li>Guidance Fee - Php 50</li>\r\n	<li>SG Fee - Php 50</li>\r\n	<li>PTCA - Php 50</li>\r\n	<li>Club Fee (all courses) - Php 50</li>\r\n	<li>Insurance Fee - Php 30</li>\r\n	<li>ALCU Fee - Php 50&nbsp; &nbsp; &nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; Total Php 330</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 3 - REGISTRATION</strong><br />\r\nPresent all the requirements listed below in Table 1, 2, or 3 at Rm. 204 &amp; 205 and avail a green card/registration form.<br />\r\nHIGH SCHOOL GRADUATES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1X1 colored picture</li>\r\n	<li>High School Card (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Good Moral Certificate (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Long brown envelope</li>\r\n	<li>Interview Slip (signed by the Interviewer)</li>\r\n</ul>\r\n\r\n<p>TRANSFEREES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1x1 colored picture</li>\r\n	<li>Original Copy - Certificate of Grades or Transcript of Records (TOR) (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Transfer Credentials &amp; Honorable Dismissal Cert. (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p>RETURNEES</p>\r\n\r\n<ul>\r\n	<li>2 pcs. 1x1 colored picture</li>\r\n	<li>Student Clearance (fully accomplished)</li>\r\n	<li>Copy of Grades</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p><strong>STEP 4 - ENLISTMENT OF SUBJECTS AND VALIDATION</strong></p>\r\n\r\n<ol>\r\n	<li>Proceed to the following designated room below and copy the list of subjects.<br />\r\n	CS, BSOM, BSIS and CCM -&nbsp;<strong>Rm. 104</strong><br />\r\n	HRS, CIT and DTS -&nbsp;<strong>Rm. 105</strong></li>\r\n	<li>Ensure that the registration form is validated and signed by the evaluator.</li>\r\n</ol>\r\n\r\n<p><strong>STEP 5 - ASSESSMENT</strong><br />\r\nPresent the fully accomplished registration form at&nbsp;<strong>Rm. 106</strong>&nbsp;for assessment of tuition fees and the rest of miscellaneous fees</p>\r\n\r\n<p><strong>STEP 6 - PAYMENT TO PGB CASHIERS</strong><br />\r\nPay for tuition fee and the rest of miscellaneous fees in Windows 1, 2 or 3 at&nbsp;<strong>Rm. 108</strong>&nbsp;or at the&nbsp;<strong>Provincial Treasurer&#39;s Office in the Provincial Capitol</strong>.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Registration Form - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Laboratory Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sports and Cultural Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Library Fee - Php 30<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Total - Php 180 plus Tuition Fee</strong><br />\r\nTo those who opted to pay at Rm. 108, the left side of the court (with chairs provided) will serve as their waiting area.</p>\r\n\r\n<p><strong>STEP 7 - REGISTRAR&#39;S APPROVAL AND ISSUANCE OF CLASSCARDS</strong><br />\r\nProceed to the provided chairs at the right side of the BPC Covered Court and have the registration form be approved by the Registrar at&nbsp;<strong>Science Laboratory</strong>&nbsp;and avail the class cards from the assigned personnel.</p>\r\n\r\n<p><strong>STEP 8 - APPLICATION FOR BPC ID</strong><br />\r\nProceed to&nbsp;<strong>MIS Office (second floor)</strong>&nbsp;and pay Php 55.</p>', '2019-03-19 10:31:49', '2019-03-19 10:31:49'),
(5, 'College', '<p><strong>ENROLLMENT PROCEDURES<br />\r\nCOLLEGE (1st, 2nd, 3rd&nbsp;and 4th&nbsp;Year)sample</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 1 - VERIFICATION OF GRADES (incoming 2nd year only)</strong><br />\r\nProceed to&nbsp;<strong>Room 201 or Room 202</strong>&nbsp;and present your complete class cards and evaluation of grades for verification of grades.</p>\r\n\r\n<p><strong>STEP 2 - PAYMENT FOR PARTIAL MISCELLANOUS FEES</strong><br />\r\nProceed to&nbsp;<strong>Room 203</strong>&nbsp;to pay for the following miscellaneous fees:</p>\r\n\r\n<ul>\r\n	<li>Sentry - Php 50</li>\r\n	<li>Guidance Fee - Php 50</li>\r\n	<li>SG Fee - Php 50</li>\r\n	<li>PTCA - Php 50</li>\r\n	<li>Club Fee (all courses) - Php 50</li>\r\n	<li>Insurance Fee - Php 30</li>\r\n	<li>ALCU Fee - Php 50&nbsp; &nbsp; &nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; Total Php 330</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 3 - REGISTRATION</strong><br />\r\nPresent all the requirements listed below in Table 1, 2, or 3 at Rm. 204 &amp; 205 and avail a green card/registration form.<br />\r\nHIGH SCHOOL GRADUATES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1X1 colored picture</li>\r\n	<li>High School Card (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Good Moral Certificate (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Long brown envelope</li>\r\n	<li>Interview Slip (signed by the Interviewer)</li>\r\n</ul>\r\n\r\n<p>TRANSFEREES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1x1 colored picture</li>\r\n	<li>Original Copy - Certificate of Grades or Transcript of Records (TOR) (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Transfer Credentials &amp; Honorable Dismissal Cert. (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p>RETURNEES</p>\r\n\r\n<ul>\r\n	<li>2 pcs. 1x1 colored picture</li>\r\n	<li>Student Clearance (fully accomplished)</li>\r\n	<li>Copy of Grades</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p><strong>STEP 4 - ENLISTMENT OF SUBJECTS AND VALIDATION</strong></p>\r\n\r\n<ol>\r\n	<li>Proceed to the following designated room below and copy the list of subjects.<br />\r\n	CS, BSOM, BSIS and CCM -&nbsp;<strong>Rm. 104</strong><br />\r\n	HRS, CIT and DTS -&nbsp;<strong>Rm. 105</strong></li>\r\n	<li>Ensure that the registration form is validated and signed by the evaluator.</li>\r\n</ol>\r\n\r\n<p><strong>STEP 5 - ASSESSMENT</strong><br />\r\nPresent the fully accomplished registration form at&nbsp;<strong>Rm. 106</strong>&nbsp;for assessment of tuition fees and the rest of miscellaneous fees</p>\r\n\r\n<p><strong>STEP 6 - PAYMENT TO PGB CASHIERS</strong><br />\r\nPay for tuition fee and the rest of miscellaneous fees in Windows 1, 2 or 3 at&nbsp;<strong>Rm. 108</strong>&nbsp;or at the&nbsp;<strong>Provincial Treasurer&#39;s Office in the Provincial Capitol</strong>.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Registration Form - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Laboratory Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sports and Cultural Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Library Fee - Php 30<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Total - Php 180 plus Tuition Fee</strong><br />\r\nTo those who opted to pay at Rm. 108, the left side of the court (with chairs provided) will serve as their waiting area.</p>\r\n\r\n<p><strong>STEP 7 - REGISTRAR&#39;S APPROVAL AND ISSUANCE OF CLASSCARDS</strong><br />\r\nProceed to the provided chairs at the right side of the BPC Covered Court and have the registration form be approved by the Registrar at&nbsp;<strong>Science Laboratory</strong>&nbsp;and avail the class cards from the assigned personnel.</p>\r\n\r\n<p><strong>STEP 8 - APPLICATION FOR BPC ID</strong><br />\r\nProceed to&nbsp;<strong>MIS Office (second floor)</strong>&nbsp;and pay Php 55.</p>', '2019-04-12 07:05:48', '2019-04-12 07:05:48'),
(6, 'College', '<p><strong>ENROLLMENT PROCEDURES<br />\r\nCOLLEGE (1st, 2nd, 3rd&nbsp;and 4th&nbsp;Year)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 1 - VERIFICATION OF GRADES (incoming 2nd year only)</strong><br />\r\nProceed to&nbsp;<strong>Room 201 or Room 202</strong>&nbsp;and present your complete class cards and evaluation of grades for verification of grades.</p>\r\n\r\n<p><strong>STEP 2 - PAYMENT FOR PARTIAL MISCELLANOUS FEES</strong><br />\r\nProceed to&nbsp;<strong>Room 203</strong>&nbsp;to pay for the following miscellaneous fees:</p>\r\n\r\n<ul>\r\n	<li>Sentry - Php 50</li>\r\n	<li>Guidance Fee - Php 50</li>\r\n	<li>SG Fee - Php 50</li>\r\n	<li>PTCA - Php 50</li>\r\n	<li>Club Fee (all courses) - Php 50</li>\r\n	<li>Insurance Fee - Php 30</li>\r\n	<li>ALCU Fee - Php 50&nbsp; &nbsp; &nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; Total Php 330</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>STEP 3 - REGISTRATION</strong><br />\r\nPresent all the requirements listed below in Table 1, 2, or 3 at Rm. 204 &amp; 205 and avail a green card/registration form.<br />\r\nHIGH SCHOOL GRADUATES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1X1 colored picture</li>\r\n	<li>High School Card (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Good Moral Certificate (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Long brown envelope</li>\r\n	<li>Interview Slip (signed by the Interviewer)</li>\r\n</ul>\r\n\r\n<p>TRANSFEREES</p>\r\n\r\n<ul>\r\n	<li>2pcs. 1x1 colored picture</li>\r\n	<li>Original Copy - Certificate of Grades or Transcript of Records (TOR) (original copy)</li>\r\n	<li>BC-NSO Authenticated (Xerox copy)</li>\r\n	<li>Transfer Credentials &amp; Honorable Dismissal Cert. (original copy)</li>\r\n	<li>National Career Assessment Exam (NCAE Result) (xerox copy)</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p>RETURNEES</p>\r\n\r\n<ul>\r\n	<li>2 pcs. 1x1 colored picture</li>\r\n	<li>Student Clearance (fully accomplished)</li>\r\n	<li>Copy of Grades</li>\r\n	<li>Evaluation of Grades from the Registrar&#39;s Office</li>\r\n</ul>\r\n\r\n<p><strong>STEP 4 - ENLISTMENT OF SUBJECTS AND VALIDATION</strong></p>\r\n\r\n<ol>\r\n	<li>Proceed to the following designated room below and copy the list of subjects.<br />\r\n	CS, BSOM, BSIS and CCM -&nbsp;<strong>Rm. 104</strong><br />\r\n	HRS, CIT and DTS -&nbsp;<strong>Rm. 105</strong></li>\r\n	<li>Ensure that the registration form is validated and signed by the evaluator.</li>\r\n</ol>\r\n\r\n<p><strong>STEP 5 - ASSESSMENT</strong><br />\r\nPresent the fully accomplished registration form at&nbsp;<strong>Rm. 106</strong>&nbsp;for assessment of tuition fees and the rest of miscellaneous fees</p>\r\n\r\n<p><strong>STEP 6 - PAYMENT TO PGB CASHIERS</strong><br />\r\nPay for tuition fee and the rest of miscellaneous fees in Windows 1, 2 or 3 at&nbsp;<strong>Rm. 108</strong>&nbsp;or at the&nbsp;<strong>Provincial Treasurer&#39;s Office in the Provincial Capitol</strong>.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Registration Form - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Laboratory Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sports and Cultural Fee - Php 50<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Library Fee - Php 30<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Total - Php 180 plus Tuition Fee</strong><br />\r\nTo those who opted to pay at Rm. 108, the left side of the court (with chairs provided) will serve as their waiting area.</p>\r\n\r\n<p><strong>STEP 7 - REGISTRAR&#39;S APPROVAL AND ISSUANCE OF CLASSCARDS</strong><br />\r\nProceed to the provided chairs at the right side of the BPC Covered Court and have the registration form be approved by the Registrar at&nbsp;<strong>Science Laboratory</strong>&nbsp;and avail the class cards from the assigned personnel.</p>\r\n\r\n<p><strong>STEP 8 - APPLICATION FOR BPC ID</strong><br />\r\nProceed to&nbsp;<strong>MIS Office (second floor)</strong>&nbsp;and pay Php 55.</p>', '2019-04-12 07:09:53', '2019-04-12 07:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `grading_matrices`
--

CREATE TABLE `grading_matrices` (
  `id` int(10) UNSIGNED NOT NULL,
  `range_start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_end` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numerical` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grading_sections`
--

CREATE TABLE `grading_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `students` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grading_sheets`
--

CREATE TABLE `grading_sheets` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_load_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numerical` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grading_students`
--

CREATE TABLE `grading_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'TRUE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grading_students`
--

INSERT INTO `grading_students` (`id`, `student_id`, `last_name`, `first_name`, `middle_name`, `regular`, `section`, `school_year`, `semester`, `active`, `created_at`, `updated_at`) VALUES
(19, '2019-0001', 'Q', 'JAKE', 'Q', 'TRUE', 'A', '2018-2019', '1', 'TRUE', '2019-10-25 02:28:31', '2019-10-25 02:28:31'),
(20, '2019-0002', 'W', 'JAZZ', 'W', 'TRUE', 'A', '2018-2019', '1', 'TRUE', '2019-10-25 02:28:31', '2019-10-25 02:28:31'),
(21, '2019-0003', 'E', 'JEFF', 'E', 'TRUE', 'A', '2018-2019', '1', 'TRUE', '2019-10-25 02:28:31', '2019-10-25 02:28:31'),
(22, '2019-0004', 'R', 'JEN', 'R', 'TRUE', 'A', '2018-2019', '1', 'TRUE', '2019-10-25 02:28:31', '2019-10-25 02:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `grading_subject_loads`
--

CREATE TABLE `grading_subject_loads` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instructor_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `irreg_students` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grading_subject_loads`
--

INSERT INTO `grading_subject_loads` (`id`, `subject_id`, `instructor_id`, `course`, `year`, `section`, `school_year`, `irreg_students`, `created_at`, `updated_at`) VALUES
(1, '1', '12018-00001', 'BSIS', '1', 'A', '2018-2019', '2019-00001', '2019-10-27 16:00:00', '2019-10-27 16:00:00'),
(2, '1', '2012-00288', 'BSIS', '1', 'A', '2018-2019', '2019-00001', '2019-10-27 16:00:00', '2019-10-27 16:00:00'),
(4, '4', '2012-00288', 'BSIS', '1', 'C', '2019-2020', NULL, '2019-10-28 20:01:40', '2019-10-28 20:01:40'),
(5, '3', '2012-00288', 'BSIS', '1', 'C', '2019-2020', NULL, '2019-10-28 22:35:21', '2019-10-28 22:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE `instructors` (
  `id` int(10) UNSIGNED NOT NULL,
  `instructors_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_student` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `educational_attainment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`id`, `instructors_id`, `name`, `birthdate`, `address`, `email`, `contact_admin`, `contact_student`, `educational_attainment`, `description`, `employment_type`, `time`, `picture`, `password`, `archived`, `created_at`, `updated_at`) VALUES
(1, '12018-00001', '1Jake Raymundo', '106/26/1998', '1Balayong Malolos Bulacan', 'jake@bpc.org', '109067612358', '109067612358', '1MIY', '1None', NULL, NULL, NULL, '$2y$10$aftIPhYGdkKmV6J09RbNdea/NS4sDg/V.QtgTh2FeyL/7xnNZDvMe', NULL, '2019-03-11 22:24:33', '2019-03-11 22:36:53'),
(2, '76855-1740', 'Electa Swaniawski', '1988-12-26 06:20:47', '8728 Langosh Camp Suite 801\nLangoshville, IN 64138-7146', 'ehamill@hotmail.com', '1-396-879-0035 x22927', '1-226-792-0235', 'Streetcar Operator', NULL, NULL, NULL, NULL, 'YVgcJXG7@LgJw-.3t/*6', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:32'),
(3, '12481-5024', 'Madeline Moore', '1997-11-09 08:03:55', '8809 Grant Pass Suite 548\nWest Jarred, ND 09595-2276', 'ron76@brakus.com', '1-582-707-4443', '(810) 308-4985', 'Gas Plant Operator', NULL, NULL, NULL, NULL, 'GBd4``e_:0Zi|)M/', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:33'),
(4, '13479-6391', 'Effie Considine', '1943-01-18 14:59:35', '658 Shea Spurs Apt. 693\nLawrencefort, SC 06411-3537', 'myrl51@torphy.com', '658-808-9769 x018', '(882) 664-0470', 'Dental Laboratory Technician', NULL, NULL, NULL, NULL, '@A.t1@nde7I(,N', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:34'),
(5, '70538-2703', 'Diamond Lang', '1998-07-09 23:13:14', '523 Lloyd Mall Suite 510\nKatharinaport, AR 72177', 'mclaughlin.eunice@oconnell.com', '917-564-8282', '471.778.7795 x67768', 'Airframe Mechanic', NULL, NULL, NULL, NULL, '7>qB.sST4!{j5z=5R4u', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:42'),
(6, '30172-2039', 'Daniella Funk', '1922-04-07 03:15:45', '68977 Kohler Greens Suite 647\nWest Roryville, NE 80908-9421', 'oma.blick@gmail.com', '591-389-7155 x91886', '+1 (997) 301-2118', 'Computer Programmer', NULL, NULL, NULL, NULL, 'v<>q!\'7KJ', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:42'),
(7, '85836-2213', 'Nikita Smitham', '1967-05-02 03:33:54', '541 Bradtke Spur Apt. 783\nNorth Olenberg, MI 88078-2190', 'hoeger.woodrow@hotmail.com', '(260) 468-7541 x54426', '+1-658-994-5098', 'Sociologist', NULL, NULL, NULL, NULL, '@L\\`#RgM(7FL@=~', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:42'),
(8, '39997', 'Ismael Emmerich', '2015-10-09 09:26:21', '836 Dovie Parks Suite 949\nGaylordburgh, OH 01917-5442', 'keeling.reta@hotmail.com', '454-561-5057 x326', '1-684-742-5119 x039', 'Claims Taker', NULL, NULL, NULL, NULL, '.[^6@Mlf7@w', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:56:43'),
(9, '85073', 'Leonardo Mills', '2003-02-17 15:40:58', '31755 Brendon Hill\nNorth Marquisstad, HI 85408-8097', 'gcarter@kerluke.info', '+1.791.895.5135', '656-597-5724', 'Dredge Operator', NULL, NULL, NULL, NULL, 'xS*c<C3e[X.BQeUV&R', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(10, '32763-1902', 'America Ritchie', '1995-10-13 11:50:58', '5362 Edmond Ports Suite 582\nSylvanside, NH 52272-6890', 'amelie.marvin@yahoo.com', '592-202-6992 x237', '+17036054793', 'Government Property Inspector', NULL, NULL, NULL, NULL, '.[$av.X', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(11, '55096', 'Dr. Orval Schinner', '1924-02-22 04:47:36', '633 Yundt Passage Apt. 906\nHarmonhaven, OK 10561', 'phyllis41@gmail.com', '(437) 922-7322 x67706', '770.908.3671', 'Director Of Business Development', NULL, NULL, NULL, NULL, 'tq3)z#@', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(12, '06214-3902', 'Jamar Purdy', '1925-12-03 04:43:28', '2479 Barton Summit\nFlossieview, TN 60091-4731', 'nader.johan@hotmail.com', '709-318-0987 x4336', '(427) 981-9007', 'Fitness Trainer', NULL, NULL, NULL, NULL, 'nLNywAs`EI\\', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(13, '74073', 'Dr. Zola Bartoletti', '1926-09-25 15:10:55', '272 Bosco Landing Apt. 549\nPort Reginaldport, MA 98784-4485', 'nnicolas@rosenbaum.com', '382.861.7862 x9202', '(658) 297-1511', 'Woodworking Machine Operator', NULL, NULL, NULL, NULL, '2[:Nd6o>#S\\O=yeTWj', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(14, '91683', 'Felton Predovic', '1930-05-08 06:49:38', '23647 Ali Plain Apt. 124\nNorth Gabriellatown, WI 20675', 'llehner@kuhic.com', '753.865.5253', '475.319.3236 x0549', 'Chemist', NULL, NULL, NULL, NULL, 'X3ex>j~reV8', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(15, '28980', 'Miss Adah Emard', '1978-04-26 18:59:53', '748 Beverly Haven Apt. 968\nEast Favian, NC 10638-1468', 'gyundt@hahn.com', '913.337.0011 x6207', '(559) 505-4309 x45763', 'Outdoor Power Equipment Mechanic', NULL, NULL, NULL, NULL, ']DcELWmqs?8]', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(16, '19524-4167', 'Retta Fritsch I', '1988-10-18 02:13:34', '8175 Runolfsdottir Mills\nTillmanbury, TX 90480-1588', 'domenica20@yahoo.com', '464.918.5577 x6259', '+18382511333', 'Licensed Practical Nurse', NULL, NULL, NULL, NULL, '*V2I,\"\\i-{$DPJ', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:57:04'),
(17, '16115', 'Kailyn Wintheiser', '1968-08-13 12:50:28', '772 Josh Mountain\nCroninchester, WA 01274-7833', 'myriam.buckridge@yahoo.com', '272.731.8659', '1-647-867-2386', 'Computer Security Specialist', NULL, NULL, NULL, NULL, 'aT0?}fN?p*g#', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:57:03'),
(18, '95124', 'Toney Kirlin Jr.', '2006-08-19 08:54:27', '5831 Owen Turnpike Suite 586\nWest Gertrudechester, GA 50483-1591', 'mann.delores@bruen.net', '+16199033543', '443-405-2583', 'Typesetter', NULL, NULL, NULL, NULL, 'fZ4\\2HiTj\\|RS^;-P~vY', 'ARCHIVED', '2019-03-12 01:49:38', '2019-03-15 20:57:03'),
(19, '28859', 'Dewayne Lockman', '1922-10-18 14:11:10', '246 Madyson Spring Apt. 484\nNorth Richie, WV 07126', 'kerluke.jamey@carter.com', '+1.313.428.6681', '1-589-774-7540 x38098', 'Warehouse', NULL, NULL, NULL, NULL, '$G6C%T2J\\6y7=', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(20, '73878-1633', 'Jarret Wintheiser', '1983-03-14 00:52:25', '1146 Alivia Forge\nLake Luluburgh, CA 97276-0069', 'umann@gutmann.com', '524-721-8832 x38217', '1-310-662-2798 x633', 'Social Worker', NULL, NULL, NULL, NULL, '|:y~D4j^9o:9/VF%E5', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(21, '92859', 'King Predovic', '2008-07-07 20:40:43', '3348 Leffler Island Suite 445\nNorth Laron, WI 17707-1785', 'jwelch@hermiston.com', '+1-912-627-1791', '747-924-3925 x762', 'Marketing Manager', NULL, NULL, NULL, NULL, 'qf]gd!g#aJf6C\\MkQ<N_', NULL, '2019-03-12 01:49:38', '2019-03-12 01:49:38'),
(22, '2018-020225', 'Marian Sumilang', '06/26/1997', 'Same', 'sumilang2018@gmail.com', '09559371500', '09559371500', 'Marketing Manager', 'CUTEST', 'REGULAR', '10-7', NULL, '$2y$10$96/OdFTBMD/9LDVkqmSQ4udhVuPGZ1trHxKsNsLlHdQhDaCEcf.ne', NULL, '2019-03-13 08:37:05', '2019-05-24 19:06:13'),
(23, '132-255222', 'Sumilang Marc Jherico', '06/26/1998', 'Same', 'marcjhericosumilang@gmail.com', '+1-912-627-1791', '+1-912-627-1791', 'Marketing Manager', 'NONE', NULL, NULL, NULL, '$2y$10$OIipvwMdBqJMVj0QrwEoDeyMSUgJm1NrjT2DrKqmYnt27tHAZDV.C', NULL, '2019-03-13 08:48:13', '2019-03-13 08:48:13'),
(26, '121212', 'jake', '06/26/1998', 'Same', 'marcjhericosumilang@gmail.com1', '12', '12', '12', '12', NULL, NULL, NULL, '$2y$10$ODYTLn9PLObLltg5M1VXGeET3tmV6BsbVNeaAHXLhvhKzJ.4HFpw6', NULL, '2019-03-13 09:00:34', '2019-03-13 09:00:34'),
(27, '111111111', 'Mam Anne', '06/26/1998', 'Same', 'registar@bulacanpolytechniccollege.org', '11111', '11111', '1111', '1111', 'REGULAR', '9-6', NULL, '$2y$10$z14ycKhjax4jME0VM6lJdupdPzn50E/OcXcj1EG.KYfjz0p100W.m', 'ARCHIVED', '2019-03-15 20:40:35', '2019-03-15 20:56:17'),
(28, '2222222', 'Marc Jherico Sumilang', '06/26/1998', 'Same', 'super@admin.com', '22', '22', '2', '22', 'REGULAR', '9-6', NULL, '$2y$10$ZUwN90o1SHp1hnde1Ki9tOp3BNEKOSNd54RGICc5rdf0OGbyFR1eG', 'ARCHIVED', '2019-03-15 20:44:44', '2019-03-15 20:56:14'),
(29, '2008-00121', 'Anna Melisa', '06/26/1998', 'Same', 'jake@bpc.org2222', '323', '23232', '32323', '2323', 'PART-TIME', 'PART-TIME', NULL, '$2y$10$f8rYwqlE8ZmkCOwuSkZHa.bPHWSDhsMlRzJdjf1GNltZQF77I0R8K', 'ARCHIVED', '2019-03-15 20:46:08', '2019-03-15 20:56:25'),
(30, '2018-8672', 'Mylene Rose V. De Guzman', '01/19/2000', 'sto. nino iba hagonoy bulacan', 'deguzmanmymy@gmail.com', '09365939955', '09365939955', 'bachelor of science office management', 'MIS officer', 'PART-TIME', 'PART-TIME', NULL, '$2y$10$OXT4q/8sdCVc1yCPkKM6gu2aNX/Ws7YaBCeyzXzxE/KGeDv.Yopg2', NULL, '2019-03-19 08:46:21', '2019-03-19 08:46:21'),
(32, '2012-00288', 'Sheldon V. Arenas', '04/28/1983', 'Malolos', 'sheldon.laxa@gmail.com', '09056784444', '09559301500', 'MIT', 'NONE', 'Full Time', NULL, NULL, '$2y$10$0IU5453rYfDDDCpNFNvNtu.4/ASwCwkBjatQp9aqDqkOVDrHpsmuu', NULL, '2019-03-19 10:34:55', '2019-03-19 10:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `instructors_logs`
--

CREATE TABLE `instructors_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `instructors_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instructors_logs`
--

INSERT INTO `instructors_logs` (`id`, `instructors_id`, `action`, `details`, `created_at`, `updated_at`) VALUES
(1, '2018-020225', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-13 09:05:10', '2019-03-13 09:05:10'),
(2, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-13 09:07:46', '2019-03-13 09:07:46'),
(3, 'INSTRUCTOR', 'LOGGED OUT', 'Name: Sumilang Mary Jane', '2019-03-13 09:07:50', '2019-03-13 09:07:50'),
(4, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-13 09:09:01', '2019-03-13 09:09:01'),
(5, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-14 05:36:11', '2019-03-14 05:36:11'),
(6, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-14 20:17:03', '2019-03-14 20:17:03'),
(7, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Mary Jane', '2019-03-15 04:14:09', '2019-03-15 04:14:09'),
(8, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sheldon V. Arenas', '2019-03-26 22:20:18', '2019-03-26 22:20:18'),
(9, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Marc Jherico', '2019-04-12 07:20:25', '2019-04-12 07:20:25'),
(10, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Marc Jherico', '2019-04-12 07:42:20', '2019-04-12 07:42:20'),
(11, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Marc Jherico', '2019-04-12 09:00:49', '2019-04-12 09:00:49'),
(12, 'INSTRUCTOR', 'LOGGED IN', 'Name: Marian Sumilang', '2019-05-24 19:20:24', '2019-05-24 19:20:24'),
(13, 'INSTRUCTOR', 'LOGGED OUT', 'Name: Marian Sumilang', '2019-05-24 19:21:20', '2019-05-24 19:21:20'),
(14, 'INSTRUCTOR', 'LOGGED IN', 'Name: Sumilang Marc Jherico', '2019-10-23 05:39:28', '2019-10-23 05:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `item_name`, `brand_name`, `qty`, `status`, `description`, `last_location`, `new_location`, `created_at`, `updated_at`) VALUES
(1, 'Sample', 'Sample', '12', 'Working', 'None', 'Shop', 'MIS', '2019-03-14 20:10:43', '2019-03-14 20:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `admin_id`, `action`, `details`, `created_at`, `updated_at`) VALUES
(1, '1', 'REGISTERED', 'Name: Mam Anne', '2019-03-07 16:28:54', '2019-03-07 16:28:54'),
(2, '1', 'UPDATED', 'Name: Maam Anna', '2019-03-07 16:31:10', '2019-03-07 16:31:10'),
(3, '1', 'DELETED', 'Name: Maam Anna', '2019-03-07 16:32:05', '2019-03-07 16:32:05'),
(4, '1', 'REGISTERED', 'Name: Maam Anna', '2019-03-07 16:34:48', '2019-03-07 16:34:48'),
(5, '1', 'REGISTERED', 'Name: Dr. Rosemarie S. Guirre', '2019-03-07 16:35:33', '2019-03-07 16:35:33'),
(6, '1', 'UPDATED', 'Name: Dr. Rosemarie S. Guirre', '2019-03-07 16:40:22', '2019-03-07 16:40:22'),
(7, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-07 18:57:55', '2019-03-07 18:57:55'),
(8, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-07 18:58:53', '2019-03-07 18:58:53'),
(9, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-07 18:59:05', '2019-03-07 18:59:05'),
(10, '4', 'LOGGED-IN', 'Name: Dr. Rosemarie S. Guirre', '2019-03-07 18:59:30', '2019-03-07 18:59:30'),
(11, '4', 'LOGGED-OUT', 'Name: Dr. Rosemarie S. Guirre', '2019-03-07 18:59:50', '2019-03-07 18:59:50'),
(12, '1', 'LOGGED-IN', 'Name: Super Admin', '2019-03-07 19:06:22', '2019-03-07 19:06:22'),
(13, '1', 'LOGGED-OUT', 'Name: Super Admin', '2019-03-07 19:06:41', '2019-03-07 19:06:41'),
(14, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-07 19:13:00', '2019-03-07 19:13:00'),
(15, '3', 'CHANGED PASSWORD', 'Name: Maam Anna', '2019-03-07 19:13:39', '2019-03-07 19:13:39'),
(16, '3', 'CHANGED PASSWORD', 'Name: Maam Anna', '2019-03-07 19:14:46', '2019-03-07 19:14:46'),
(17, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-07 19:15:31', '2019-03-07 19:15:31'),
(18, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-07 19:15:50', '2019-03-07 19:15:50'),
(19, '3', 'UPDATED COLLEGE PROCEDURES', 'NAME: Maam Anna', '2019-03-07 19:26:27', '2019-03-07 19:26:27'),
(20, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-07 19:36:36', '2019-03-07 19:36:36'),
(21, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-08 23:42:39', '2019-03-08 23:42:39'),
(22, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-09 00:53:23', '2019-03-09 00:53:23'),
(23, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-09 23:38:20', '2019-03-09 23:38:20'),
(24, '1', 'LOGGED-IN', 'Name: Super Admin', '2019-03-11 21:46:08', '2019-03-11 21:46:08'),
(25, '1', 'REGISTERED', 'Name: Engr. Arman Giron', '2019-03-11 21:47:05', '2019-03-11 21:47:05'),
(26, '1', 'REGISTERED', 'Name: Mam Beth', '2019-03-11 21:47:46', '2019-03-11 21:47:46'),
(27, '1', 'LOGGED-OUT', 'Name: Super Admin', '2019-03-11 22:05:25', '2019-03-11 22:05:25'),
(28, '5', 'LOGGED-IN', 'Name: Engr. Arman Giron', '2019-03-11 22:05:39', '2019-03-11 22:05:39'),
(29, '5', 'LOGGED-OUT', 'Name: Engr. Arman Giron', '2019-03-12 00:05:15', '2019-03-12 00:05:15'),
(30, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-12 00:23:56', '2019-03-12 00:23:56'),
(31, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-12 04:50:42', '2019-03-12 04:50:42'),
(32, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-12 20:32:57', '2019-03-12 20:32:57'),
(33, '5', 'LOGGED-IN', 'Name: Engr. Arman Giron', '2019-03-13 02:54:27', '2019-03-13 02:54:27'),
(34, '1', 'LOGGED-IN', 'Name: Super Admin', '2019-03-13 02:55:08', '2019-03-13 02:55:08'),
(35, '5', 'LOGGED-OUT', 'Name: Engr. Arman Giron', '2019-03-13 04:51:11', '2019-03-13 04:51:11'),
(36, '5', 'LOGGED-IN', 'Name: Engr. Arman Giron', '2019-03-13 08:35:39', '2019-03-13 08:35:39'),
(37, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: Sumilang Mary Jane', '2019-03-13 08:45:33', '2019-03-13 08:45:33'),
(38, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Sumilang Mary Jane', '2019-03-13 08:46:21', '2019-03-13 08:46:21'),
(39, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: jake', '2019-03-13 08:48:14', '2019-03-13 08:48:14'),
(40, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: jake', '2019-03-13 09:00:34', '2019-03-13 09:00:34'),
(41, '5', 'LOGGED-OUT', 'Name: Engr. Arman Giron', '2019-03-13 09:11:26', '2019-03-13 09:11:26'),
(42, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-13 09:11:35', '2019-03-13 09:11:35'),
(43, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-13 16:14:28', '2019-03-13 16:14:28'),
(44, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-14 00:56:09', '2019-03-14 00:56:09'),
(45, '6', 'LOGGED-OUT', 'Name: Mam Beth', '2019-03-14 03:51:41', '2019-03-14 03:51:41'),
(46, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-14 03:51:54', '2019-03-14 03:51:54'),
(47, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-14 04:13:25', '2019-03-14 04:13:25'),
(48, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-14 04:13:35', '2019-03-14 04:13:35'),
(49, '6', 'LOGGED-OUT', 'Name: Mam Beth', '2019-03-14 05:18:09', '2019-03-14 05:18:09'),
(50, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-14 05:19:13', '2019-03-14 05:19:13'),
(51, '6', 'LOGGED-OUT', 'Name: Mam Beth', '2019-03-14 05:30:51', '2019-03-14 05:30:51'),
(52, '1', 'LOGGED-IN', 'Name: Super Admin', '2019-03-14 20:03:29', '2019-03-14 20:03:29'),
(53, '1', 'REGISTERED', 'Name: Maam You Know Who', '2019-03-14 20:05:15', '2019-03-14 20:05:15'),
(54, '1', 'LOGGED-OUT', 'Name: Super Admin', '2019-03-14 20:05:23', '2019-03-14 20:05:23'),
(55, '7', 'LOGGED-IN', 'Name: Maam You Know Who', '2019-03-14 20:05:34', '2019-03-14 20:05:34'),
(56, '7', 'LOGGED-OUT', 'Name: Maam You Know Who', '2019-03-14 20:11:58', '2019-03-14 20:11:58'),
(57, '5', 'LOGGED-IN', 'Name: Engr. Arman Giron', '2019-03-15 19:56:21', '2019-03-15 19:56:21'),
(58, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: jake', '2019-03-15 20:00:28', '2019-03-15 20:00:28'),
(59, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: Mam Anne', '2019-03-15 20:40:35', '2019-03-15 20:40:35'),
(60, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: Mam Anne', '2019-03-15 20:44:16', '2019-03-15 20:44:16'),
(61, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: Marc Jherico Sumilang', '2019-03-15 20:44:44', '2019-03-15 20:44:44'),
(62, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: Maam Anna', '2019-03-15 20:46:08', '2019-03-15 20:46:08'),
(63, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Marc Jherico Sumilang', '2019-03-15 20:56:14', '2019-03-15 20:56:14'),
(64, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Mam Anne', '2019-03-15 20:56:17', '2019-03-15 20:56:17'),
(65, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Maam Anna', '2019-03-15 20:56:25', '2019-03-15 20:56:25'),
(66, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Electa Swaniawski', '2019-03-15 20:56:32', '2019-03-15 20:56:32'),
(67, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Madeline Moore', '2019-03-15 20:56:33', '2019-03-15 20:56:33'),
(68, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Effie Considine', '2019-03-15 20:56:34', '2019-03-15 20:56:34'),
(69, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Diamond Lang', '2019-03-15 20:56:42', '2019-03-15 20:56:42'),
(70, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Daniella Funk', '2019-03-15 20:56:42', '2019-03-15 20:56:42'),
(71, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Nikita Smitham', '2019-03-15 20:56:43', '2019-03-15 20:56:43'),
(72, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Ismael Emmerich', '2019-03-15 20:56:43', '2019-03-15 20:56:43'),
(73, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Toney Kirlin Jr.', '2019-03-15 20:57:03', '2019-03-15 20:57:03'),
(74, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Kailyn Wintheiser', '2019-03-15 20:57:03', '2019-03-15 20:57:03'),
(75, 'VPAF', 'ARCHIVED AN INSTRUCTOR', 'Name: Retta Fritsch I', '2019-03-15 20:57:04', '2019-03-15 20:57:04'),
(76, '5', 'LOGGED-OUT', 'Name: Engr. Arman Giron', '2019-03-15 22:34:21', '2019-03-15 22:34:21'),
(77, '1', 'LOGGED-IN', 'Name: Super Admin', '2019-03-15 22:34:33', '2019-03-15 22:34:33'),
(78, '1', 'REGISTERED', 'Name: Rebecca I don\'t know', '2019-03-15 22:35:45', '2019-03-15 22:35:45'),
(79, '1', 'LOGGED-OUT', 'Name: Super Admin', '2019-03-15 22:36:50', '2019-03-15 22:36:50'),
(80, '8', 'LOGGED-IN', 'Name: Rebecca I don\'t know', '2019-03-15 22:37:03', '2019-03-15 22:37:03'),
(81, '8', 'LOGGED-OUT', 'Name: Rebecca I don\'t know', '2019-03-15 23:06:40', '2019-03-15 23:06:40'),
(82, '4', 'LOGGED-IN', 'Name: Dr. Rosemarie S. Guirre', '2019-03-15 23:06:50', '2019-03-15 23:06:50'),
(83, '4', 'LOGGED-OUT', 'Name: Dr. Rosemarie S. Guirre', '2019-03-15 23:17:07', '2019-03-15 23:17:07'),
(84, '8', 'LOGGED-IN', 'Name: Rebecca I don\'t know', '2019-03-15 23:17:16', '2019-03-15 23:17:16'),
(85, '8', 'LOGGED-OUT', 'Name: Rebecca I don\'t know', '2019-03-16 01:59:17', '2019-03-16 01:59:17'),
(86, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-16 01:59:34', '2019-03-16 01:59:34'),
(87, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-16 02:11:45', '2019-03-16 02:11:45'),
(88, '8', 'LOGGED-IN', 'Name: Rebecca I don\'t know', '2019-03-16 02:11:57', '2019-03-16 02:11:57'),
(89, '8', 'LOGGED-OUT', 'Name: Rebecca I don\'t know', '2019-03-16 02:32:31', '2019-03-16 02:32:31'),
(90, '5', 'LOGGED-IN', 'Name: Engr. Arman Giron', '2019-03-16 02:32:45', '2019-03-16 02:32:45'),
(91, '5', 'LOGGED-OUT', 'Name: Engr. Arman Giron', '2019-03-16 03:19:19', '2019-03-16 03:19:19'),
(92, '6', 'LOGGED-IN', 'Name: Mam Beth', '2019-03-16 03:26:09', '2019-03-16 03:26:09'),
(93, '4', 'LOGGED-IN', 'Name: Dr. Rosemarie S. Guirre', '2019-03-17 00:18:39', '2019-03-17 00:18:39'),
(94, '4', 'LOGGED-OUT', 'Name: Dr. Rosemarie S. Guirre', '2019-03-17 02:04:32', '2019-03-17 02:04:32'),
(95, '4', 'LOGGED-IN', 'Name: Dr. Rosemarie S. Guirre', '2019-03-17 02:04:44', '2019-03-17 02:04:44'),
(96, '4', 'LOGGED-OUT', 'Name: Dr. Rosemarie S. Guirre', '2019-03-17 02:06:52', '2019-03-17 02:06:52'),
(97, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-17 16:24:10', '2019-03-17 16:24:10'),
(98, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: marcjhericosumilang@gmail.com', '2019-03-17 16:49:48', '2019-03-17 16:49:48'),
(99, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-18 05:56:48', '2019-03-18 05:56:48'),
(100, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-18 06:13:51', '2019-03-18 06:13:51'),
(101, '4', 'LOGGED-IN', 'Name: Dr. Rosemarie S. Guirre', '2019-03-18 06:14:02', '2019-03-18 06:14:02'),
(102, '4', 'LOGGED-OUT', 'Name: Dr. Rosemarie S. Guirre', '2019-03-18 06:17:19', '2019-03-18 06:17:19'),
(103, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-18 06:17:41', '2019-03-18 06:17:41'),
(104, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-18 13:45:49', '2019-03-18 13:45:49'),
(105, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-18 17:11:35', '2019-03-18 17:11:35'),
(106, '3', 'LOGGED-IN', 'Name: Maam Anna', '2019-03-18 17:11:59', '2019-03-18 17:11:59'),
(107, '3', 'UPDATED COLLEGE PROCEDURES', 'NAME: Maam Anna', '2019-03-18 17:13:37', '2019-03-18 17:13:37'),
(108, '3', 'LOGGED-OUT', 'Name: Maam Anna', '2019-03-18 17:16:12', '2019-03-18 17:16:12'),
(109, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: marcjhericosumilang@gmail.com', '2019-03-19 07:59:59', '2019-03-19 07:59:59'),
(110, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: marcjhericosumilang@gmail.com', '2019-03-19 08:02:12', '2019-03-19 08:02:12'),
(111, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: marcjhericosumilang@gmail.com', '2019-03-19 08:02:40', '2019-03-19 08:02:40'),
(112, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: marcjhericosumilang@gmail.com', '2019-03-19 08:08:04', '2019-03-19 08:08:04'),
(113, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: jeremiahfern06@gmail.com', '2019-03-19 08:42:43', '2019-03-19 08:42:43'),
(114, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: Mylene Rose V. De Guzman', '2019-03-19 08:46:21', '2019-03-19 08:46:21'),
(115, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: jeremiahfern06@gmail.com', '2019-03-19 08:48:13', '2019-03-19 08:48:13'),
(116, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: juliabeatrisse98@gmail.com', '2019-03-19 08:58:59', '2019-03-19 08:58:59'),
(117, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: vpaulo1312@gmail.com', '2019-03-19 10:17:07', '2019-03-19 10:17:07'),
(118, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: magbitangminerva@gmail.com', '2019-03-19 10:20:09', '2019-03-19 10:20:09'),
(119, '3', 'UPDATED COLLEGE PROCEDURES', 'NAME: Maam Anna', '2019-03-19 10:31:50', '2019-03-19 10:31:50'),
(120, 'VPAF', 'CREATED AN INSTRUCTOR', 'Name: Sheldon V. Arenas', '2019-03-19 10:34:55', '2019-03-19 10:34:55'),
(121, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: Sheldon V. Arenas', '2019-03-19 10:35:09', '2019-03-19 10:35:09'),
(122, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: swordsmanmarc@gmail.com', '2019-04-12 07:05:06', '2019-04-12 07:05:06'),
(123, '3', 'UPDATED COLLEGE PROCEDURES', 'NAME: Maam Anna', '2019-04-12 07:05:48', '2019-04-12 07:05:48'),
(124, '3', 'UPDATED COLLEGE PROCEDURES', 'NAME: Maam Anna', '2019-04-12 07:09:53', '2019-04-12 07:09:53'),
(125, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: sumilangmarc1999@gmail.com', '2019-04-12 08:55:25', '2019-04-12 08:55:25'),
(126, 'REGISTRAR', 'ENROLLED A STUDENT', 'Email: rcapule049410@gmail.com', '2019-05-24 19:01:49', '2019-05-24 19:01:49'),
(127, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: Marian Sumilang', '2019-05-24 19:05:58', '2019-05-24 19:05:58'),
(128, 'VPAF', 'UPDATED AN INSTRUCTOR', 'Name: Marian Sumilang', '2019-05-24 19:06:13', '2019-05-24 19:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_03_06_023521_create_admins_table', 1),
(9, '2017_03_06_053834_create_admin_role_table', 1),
(10, '2018_03_06_023523_create_roles_table', 1),
(11, '2018_10_04_100116_create_rooms_table', 1),
(12, '2018_10_04_101434_create_subjects_table', 1),
(13, '2018_10_09_024339_create_s_y_s_table', 1),
(14, '2018_10_16_094334_create_misc_fees_table', 1),
(15, '2018_10_16_120045_create_tuit_fees_table', 1),
(16, '2018_10_18_033204_create_college_applications_table', 1),
(17, '2018_10_22_040554_create_scholarships_table', 1),
(18, '2018_10_29_085752_create_enrollment_procedures_table', 1),
(19, '2018_11_16_092535_create_date_time_records_table', 1),
(20, '2019_03_08_001914_create_logs_table', 2),
(21, '2019_01_23_130624_create_inventories_table', 3),
(22, '2019_01_31_072927_create_instructors_table', 3),
(23, '2019_03_08_021720_create_logs_table', 3),
(24, '2019_03_12_052934_create_schedules_table', 4),
(25, '2019_03_12_054455_create_rooms_table', 5),
(26, '2019_03_13_041157_create_courses_table', 6),
(27, '2019_03_13_163143_create_instructors_logs_table', 7),
(28, '2019_03_16_060324_create_books_table', 8),
(29, '2019_03_17_012423_create_date_time_records_table', 9),
(30, '2019_10_22_145420_create_grading_students_table', 10),
(31, '2019_10_24_151527_create_grading_matrices_table', 11),
(32, '2019_10_24_151858_create_grading_sections_table', 11),
(33, '2019_10_24_153652_create_grading_sheets_table', 11),
(34, '2019_10_24_153856_create_grading_subject_loads_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `misc_fees`
--

CREATE TABLE `misc_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `misc_fees`
--

INSERT INTO `misc_fees` (`id`, `level`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'College', 'SG', 150, '2019-03-08 23:11:42', '2019-03-08 16:00:00'),
(2, 'College', 'Library', 150, '2019-03-08 23:11:42', '2019-03-08 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'super', '2019-03-07 15:32:53', '2019-03-07 15:32:53'),
(2, 'registrar', '2019-03-07 16:03:13', '2019-03-07 16:03:13'),
(3, 'mis', '2019-03-07 16:35:50', '2019-03-07 16:35:50'),
(4, 'vpaa', '2019-03-07 16:36:04', '2019-03-07 16:36:04'),
(5, 'vpaf', '2019-03-07 16:36:17', '2019-03-07 16:36:17'),
(6, 'librarian', '2019-03-07 16:36:47', '2019-03-07 16:36:47'),
(7, 'inventory-custodian', '2019-03-07 16:37:30', '2019-03-07 16:37:30'),
(8, 'vpaa-assistant', '2019-03-07 16:37:57', '2019-03-07 16:37:57'),
(10, 'vpaf-assistant', '2019-03-07 16:38:26', '2019-03-07 16:38:26'),
(11, 'mis-assistant', '2019-03-07 16:38:53', '2019-03-07 16:38:53'),
(12, 'librarian-assistant', '2019-03-07 16:39:07', '2019-03-07 16:39:07'),
(13, 'registrar-assistant', '2019-03-07 16:39:37', '2019-03-07 16:39:37'),
(14, 'instructor', '2019-03-13 02:56:55', '2019-03-13 02:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_no`, `floor`, `building`, `description`, `map`, `archived`, `created_at`, `updated_at`) VALUES
(1, '111', '1', '1', '1', NULL, 'ARCHIVED', '2019-03-11 23:27:48', '2019-03-11 23:30:07'),
(2, '203', '2', 'MAIN', 'AUDIO ROOM', NULL, NULL, '2019-03-11 23:30:03', '2019-03-12 01:41:12'),
(3, '201', '2', 'MAIN', 'LECTURE ROOM', NULL, NULL, '2019-03-11 23:30:13', '2019-03-19 10:49:34'),
(4, '101', '1', 'MAIN', 'REGULAR', NULL, NULL, '2019-03-12 01:37:02', '2019-03-12 01:41:23'),
(5, '102', '1', 'MAIN', 'REGULAR', NULL, NULL, '2019-03-12 01:37:19', '2019-03-12 01:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_building` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructor_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructor_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_raw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `course`, `year`, `section`, `subject_code`, `subject_description`, `semester`, `school_year`, `room_building`, `room_no`, `room_description`, `instructor_id`, `instructor_name`, `time`, `time_raw`, `day`, `color`, `archive`, `created_at`, `updated_at`) VALUES
(2, 'HRS', '1', 'A', 'IS 102', 'CLOUD', '1', '2019-2020', 'MAIN', '201', 'COMLAB', '2018-020225', 'Sumilang Mary Jane', '7:00 AM,7:30 AM,8:00 AM', '700am,730am,800am', 'tuesday,thursday', '99cc00', NULL, '2019-03-13 19:51:47', '2019-03-13 21:26:54'),
(3, 'CCS', '1', 'A', 'IS 102', 'CLOUD', '1', '2019-2020', 'MAIN', '102', 'REGULAR', '2018-020225', 'Sumilang Mary Jane', '7:00 AM,7:30 AM,8:00 AM', '700am,730am,800am', 'tuesday,thursday', '99cc00', NULL, '2019-03-13 19:53:11', '2019-03-13 21:26:18'),
(4, 'HRS', '1', 'B', 'IS 102', 'CLOUD', '1', '2019-2020', 'MAIN', '101', 'REGULAR', '2018-020225', 'Sumilang Mary Jane', '7:00 AM,7:30 AM,8:00 AM', '700am,730am,800am', 'tuesday', '99cc00', NULL, '2019-03-13 19:51:47', '2019-03-13 21:26:54'),
(5, 'CCS', '1', 'A', 'IS 101', 'MATH', '1', '2019-2020', 'MAIN', '102', 'REGULAR', '2018-020225', 'Sumilang Mary Jane', '7:00 AM,7:30 AM,8:00 AM', '700am,730am,800am', 'tuesday,thursday', '99cc00', NULL, '2019-03-13 19:53:11', '2019-03-13 21:26:18'),
(6, 'BSIS', '1', 'C', 'IS 105', 'Object Oriented Programming', '1', '2019-2020', 'MAIN', '201', 'COMLAB', '2018-020225', 'Sumilang Mary Jane', '7:30 AM,7:00 AM,8:00 AM', '730am,700am,800am', 'monday,wednesday', '99cc00', NULL, '2019-03-14 05:23:03', '2019-03-14 05:23:03'),
(7, 'BSIS', '1', 'C', 'MATH 102', 'ALGEBRA', '1', '2019-2020', 'MAIN', '201', 'COMLAB', '132-255222', 'jake', '7:30 AM,7:00 AM,8:00 AM', '730am,700am,800am', 'saturday', '99cc00', NULL, '2019-03-14 05:27:01', '2019-03-14 05:27:01'),
(8, 'BSIS', '1', 'A', 'IS 102', 'CLOUD', '1', '2019-2020', 'MAIN', '201', 'COMLAB', '2018-020225', 'Sumilang Mary Jane', '8:30 AM,9:00 AM,9:30 AM', '830am,900am,930am', 'saturday', '99cc00', NULL, '2019-03-14 05:29:28', '2019-03-14 05:29:28'),
(9, 'BSIS', '1', 'C', 'IS 102', 'CLOUD', '1', '2019-2020', 'MAIN', '201', 'COMLAB', '132-255222', 'jake', '9:30 AM,10:00 AM,10:30 AM,11:00 AM', '930am,1000am,1030am,1100am', 'wednesday', '99cc00', NULL, '2019-03-14 05:30:11', '2019-03-14 05:30:11'),
(10, 'BSIS', '1', 'A', 'IS 103', 'HTML', '1', '2019-2020', 'MAIN', '102', 'REGULAR', '73878-1633', 'Jarret Wintheiser', '7:00 AM,7:30 AM,8:00 AM,8:30 AM', '700am,730am,800am,830am', 'monday,wednesday', '99cc00', NULL, '2019-03-19 08:35:29', '2019-03-19 08:35:29'),
(11, 'BSIS', '1', 'A', 'FIS 111', 'Fundamentals of Information Systems', '1', '2019-2020', 'MAIN', '201', 'LECTURE ROOM', '132-255222', 'Sumilang Marc Jherico', '9:30 AM,10:00 AM,10:30 AM', '930am,1000am,1030am', 'thursday', '99cc00', NULL, '2019-03-19 10:52:27', '2019-03-19 10:52:27'),
(12, 'BSIS', '1', 'A', 'IS 105', 'Object Oriented Programming', '1', '2019-2020', 'MAIN', '201', 'LECTURE ROOM', '74073', 'Dr. Zola Bartoletti', '12:00 PM,12:30 PM,1:00 PM,1:30 PM', '1200,1230,100,130', 'tuesday,friday', '99cc00', NULL, '2019-09-13 14:31:52', '2019-09-13 14:31:52'),
(13, 'HRS', '1', 'A', 'FIS 111', 'Fundamentals of Information Systems', '1', '2019-2020', 'MAIN', '201', 'LECTURE ROOM', '74073', 'Dr. Zola Bartoletti', '1:30 PM,2:00 PM,2:30 PM', '130,200,230', '', '99cc00', NULL, '2019-09-13 14:33:32', '2019-09-13 14:33:32');

-- --------------------------------------------------------

--
-- Table structure for table `scholarships`
--

CREATE TABLE `scholarships` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_portal_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `application_form` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `card` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cor1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cor2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copy_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_id2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `renewal_form` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `letter` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school_years`
--

CREATE TABLE `school_years` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_year` int(11) NOT NULL,
  `end_year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school_years`
--

INSERT INTO `school_years` (`id`, `start_year`, `end_year`, `semester`, `created_at`, `updated_at`) VALUES
(1, 2018, 2019, 1, '2019-03-08 16:00:00', '2019-03-08 16:00:00'),
(2, 2019, 2020, 1, '2019-03-09 04:21:00', '2019-03-09 04:21:00');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `units` int(11) NOT NULL,
  `prerequisite` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `course`, `year`, `semester`, `code`, `title`, `description`, `units`, `prerequisite`, `archived`, `created_at`, `updated_at`) VALUES
(1, 'BSIS', 1, 1, 'FIS 111', 'Fundamentals of Information Systems', 'Introduction to bla bla bla......', 3, 'IS 103', NULL, '2019-03-12 06:12:47', '2019-10-23 05:49:56'),
(2, 'BSIS', 1, 1, 'MATH 102', 'ALGEBRA', 'COMPUTATIONS', 3, 'NONE', NULL, '2019-03-12 06:41:57', '2019-03-12 06:41:57'),
(3, 'BSIS', 1, 1, 'IS 102', 'CLOUD', 'COMPUTING', 3, 'NONE', NULL, '2019-03-13 09:12:32', '2019-03-13 09:12:32'),
(4, 'BSIS', 1, 1, 'IS 105', 'Object Oriented Programming', 'Using Java runtime console application', 3, 'NONE', NULL, '2019-03-14 05:20:50', '2019-03-14 05:20:50'),
(5, 'BSIS', 1, 1, 'IS 103', 'HTML', 'web designing', 3, 'NONE', NULL, '2019-03-19 08:31:31', '2019-03-19 08:33:16'),
(7, 'BSIS', 1, 1, 'IS CP1 111', 'Computer Programming 1', 'Intro to programming', 3, 'NONE', NULL, '2019-03-19 10:24:20', '2019-03-19 10:24:20');

-- --------------------------------------------------------

--
-- Table structure for table `tuit_fees`
--

CREATE TABLE `tuit_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `cash_installment` int(11) NOT NULL,
  `archived` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tuit_fees`
--

INSERT INTO `tuit_fees` (`id`, `course`, `year`, `semester`, `per_unit`, `cash_installment`, `archived`, `created_at`, `updated_at`) VALUES
(1, 'BSIS', 1, 1, 150, 160, NULL, '2019-03-07 16:00:00', '2019-03-30 13:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `archived` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `provider`, `provider_id`, `email_verified_at`, `archived`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'jake', 'marcjhericosumilang@gmail.com1', NULL, NULL, '2019-03-08 22:42:36', NULL, '$2y$10$72tH.YFim7L5cuoxtGkhe.SaavQKl0rSX/MNqhoSmZQsA1v82JeyC', 'PDRyxUXJmcxftFHKpxP1MZF9AB85Sq5XqagZtlaE6zfeqOKS7I5PQBc0ZtSy', '2019-03-08 22:41:02', '2019-03-08 22:42:36'),
(6, 'Jeff', 'jeff@gmail.com', NULL, NULL, '2019-03-08 22:42:36', NULL, '$2y$10$72tH.YFim7L5cuoxtGkhe.SaavQKl0rSX/MNqhoSmZQsA1v82JeyC', NULL, '2019-03-08 22:41:02', '2019-03-08 22:42:36'),
(7, 'Marc Jherico Sumilang', 'marcjhericosumilang@gmail.com3', NULL, NULL, '2019-03-09 00:23:01', NULL, '$2y$10$rBvJ5NWL1rFvwOpOOciXVurjcHwhQhDm2qX4nHomWDKG96hJhbX.G', '5vanLxe1VHUdBDDlB3abn2ceRJdTZUcBmr0K7TJczc4UVFMibpWabgNW288Z', '2019-03-09 00:11:09', '2019-03-09 00:23:01'),
(8, 'Meliodas', 'sumilang2018@gmail.com', NULL, NULL, '2019-03-09 00:23:01', NULL, '$2y$10$qiLXqp1PNGrMKUXe0O.AyuW4KdtNstP33fFdZjQWTZzfgPCANIPrC', 'ZY55se9EQ20OoWfI0oe0I3rjJVg84EiQuwogRo7URYvQDxoqTCrRA2JLTjV9', '2019-03-09 15:45:22', '2019-03-09 15:45:22'),
(9, 'jake', 'mis@bulacanpolytechniccollege.org3', NULL, NULL, NULL, NULL, '$2y$10$w7P0KvLHToXbzF95VkDK7.igS3s9N7XOTiUYm1JeuLpps2ZSWJejW', 'RMCCtPKJgKVBaY0b3m3reNscYmrFlVNt9vzT3FTZ5jy5Ny1P7ssmMkSdfdt0', '2019-03-11 07:28:57', '2019-03-11 07:28:57'),
(10, 'jake', 'marcjhericosumilang@gmail.com212', NULL, NULL, '2019-03-09 00:23:01', NULL, '$2y$10$I4wF7qBL8xUEkrQ1McV7iO79nv3PRtRXK/ZuCkAcWaWgnxkAHkmZS', 'zTFcE9gKkGJTA1zZWsYEEHrAdTxjOKK5aahlXf84fHdNFoKIZwANNcUFgDBl', '2019-03-11 07:31:21', '2019-03-11 07:31:21'),
(20, 'vpaa-assistant', 'mis@bulacanpolytechniccollege.org', NULL, NULL, '2019-03-09 00:23:01', NULL, '$2y$10$C2sGxLUAWZ0mUfoBl89nd.kTpCi1C.uNs1FKE/F2iFXifV7yd0e66', NULL, '2019-03-15 08:22:54', '2019-03-15 08:22:54'),
(21, 'Marc Jherico Sumilang', 'marcjhericosumilang@gmail.com42', NULL, NULL, '2019-03-19 07:07:04', NULL, '$2y$10$ufBUNZfnUtfOCdVhIstGJemNNezEprS4slui3n1Bf0zvv6Hia6rae', 'ybu4PsZ5vrMv1r9TZrUiwSjn0DsSn6JUfsicS0ECJ4pzurix2emR4T8S88Kt', '2019-03-19 07:01:33', '2019-03-19 07:07:04'),
(22, 'Jeremiah Samson Fernandez', 'jeremiahfern06@gmail.com', NULL, NULL, '2019-03-19 07:20:29', NULL, '$2y$10$02XnOh6fkZ9R0jK.nlNskO2itrwxPIPsauNYuqF9Myns.hFd/djLq', 'lfBNX0YAcD1adbLMtaLsesMNN17Gc7hc6qw2dstmhmv326AYodg0qhJmm8Gu', '2019-03-19 07:18:56', '2019-03-19 07:20:29'),
(23, 'Christian', 'grashellacute1@gmail.com', NULL, NULL, '2019-03-19 08:34:06', NULL, '$2y$10$g1Zo.zSsElSrNjbZKkdzoORSsMqth63REm4JGb74pzKEj/wg/RbM6', NULL, '2019-03-19 08:32:19', '2019-03-19 08:34:06'),
(24, 'Beatrisse Julia C. Bantug', 'juliabeatrisse98@gmail.com', NULL, NULL, '2019-03-19 08:52:56', NULL, '$2y$10$N50sEmvD9dZD.ZgSU.ftp.jyda41UCrl/jTevRcLGRKMtm1clv5EG', 'xefsIkjJnm0PFenm4gfsMWXGm3o5rZoxLmwhqoNzZNT79dIR2TQ2pQjnJfCW', '2019-03-19 08:51:37', '2019-03-19 08:52:56'),
(25, 'Joshua Logrosa', 'logrosajoshua1013@gmail.com', NULL, NULL, '2019-03-19 08:57:02', NULL, '$2y$10$aAuXtgWjzq9f40rWmp0rkenF814HAXtueVHPLKKIATjR3t6w0A5ga', 'umhqJAr7dFjWnWATeOsFrlwXk0WCYxEZl68hD6teN85WQ2BrVKabhpDWkN9Y', '2019-03-19 08:55:09', '2019-03-19 08:57:02'),
(26, 'Roanne Capule', 'rcapule049410@gmail.com', NULL, NULL, '2019-03-19 09:19:07', NULL, '$2y$10$FTJi/PPcqpr6GNfkJrU9U.L8TJmQ1KrzTCKmaRNvx.SSe..f1Hwzu', NULL, '2019-03-19 09:16:44', '2019-03-19 09:19:07'),
(27, 'sheldon arenas', 'sheldon.laxa@gmail.com', NULL, NULL, '2019-03-19 09:59:34', NULL, '$2y$10$yFsTkEN6Jm1D0w3cAA6AtuNznu77eKhromptHAgA0LFhWSai4CWZ6', NULL, '2019-03-19 09:53:41', '2019-03-19 09:59:34'),
(28, 'Paulo', 'vpaulo1312@gmail.com', NULL, NULL, '2019-03-19 09:56:05', NULL, '$2y$10$NjnZITrw7KlaGJ0pxvBmb.eeFTPP5DEbkAfQhjWR9laMBFnr4jxsC', NULL, '2019-03-19 09:55:18', '2019-03-19 09:56:05'),
(29, 'Minerva Magbitang', 'magbitangminerva@gmail.com', NULL, NULL, '2019-03-19 09:58:20', NULL, '$2y$10$BWErNJSO/48feZ3wv7vUzOXMcQ4ZpUmaj9RH.EZpC0ONM8Vt2FJMy', 'oTVwtB9O5Zi87Eej2thOEZCpcUWQx7uoZnFxCxQrets4Jtvcw0nL1Z6erv1q', '2019-03-19 09:57:06', '2019-03-19 09:58:20'),
(30, 'Jefferson Raymundo', 'swordsmanmarc@gmail.com', NULL, NULL, '2019-04-12 06:57:27', NULL, '$2y$10$EhRu54qOquXLoqyxuXzLkeZjCc6Aek.zYKM.5FQrdjkmV8aKw6R9m', 'Tkhbbre8RG2zivWtbZCf9Cv4wEtU4qQO6flylFSHKXB9dCVGS9ykeujhfyLu', '2019-04-12 06:56:25', '2019-04-12 06:57:27'),
(31, 'Milky Tolentino', 'mikkitytea@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$iwG.031R6Yi846lSnpopIOHReXquuVBZePjoMpquoNDiKjlZ8Xs7K', 'EAFikFz7yrwTobH3v3A9z97UTx0s8vWUPSRiMBKWJ8eodr9fDurbnfz7MlZ4', '2019-04-12 07:54:31', '2019-04-12 07:54:31'),
(32, 'Edison Pineda', 'dimevision0010@gmail.com', NULL, NULL, '2019-04-12 08:51:58', NULL, '$2y$10$nGUFpIPw9/gCc8kqzHI/YetylnGJ5RLe3yEUbSQM7J1aBugIOzoVi', '4q9oqVA0Kl2L8Ny3ZiCoeNzNPb9xPXKFN7V11nKttLXrVcYDQQqWZMI2wMZW', '2019-04-12 08:50:40', '2019-04-12 08:51:58'),
(33, 'Beverlyn', 'beverlynamos@yahoo.com', NULL, NULL, NULL, NULL, '$2y$10$KeM3L0J0FJf62.EUgR.qP.bAsbDYQklNKD9lV43WqSF6PyCREtujy', 'LYMtDr8LclGdDe7GgozjFurBbRZQctl7i9XVp6m71u7yLDDDQctVuzfwW29n', '2019-04-12 08:50:57', '2019-04-12 08:50:57'),
(34, 'Juls Co', 'cojuls00@gmail.com', NULL, NULL, '2019-04-12 08:52:46', NULL, '$2y$10$HCNpiNRuBndqqw.yQsrRgeAn8qf5sfeSfMSI0rwKK1D4QNaOL6aoi', NULL, '2019-04-12 08:51:06', '2019-04-12 08:52:46'),
(35, 'Jimuel', 'vakla1234@yahoo.com', NULL, NULL, NULL, NULL, '$2y$10$tMIlLSKDtY2tZJHDIpWrfeUKPJ0zMeVwh88lyiaYGt68K5rqOjtDG', 'QunaxEiXQkYXcHlIYkpUtfElzUmMPBuREXxJ1ABTFvfd7GFoL9mEbGs3Urqk', '2019-04-12 08:51:10', '2019-04-12 08:51:10'),
(36, 'JohnVincent', 'labordiojv@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$EjE848WOJb2jlT4Nl/RanOLnMlcsS4neKUvrqOe5uHsTk3NSv2jlK', NULL, '2019-04-12 08:51:35', '2019-04-12 08:51:35'),
(37, 'Ailene De Raya', 'lhyn.deraya@gmail.com', NULL, NULL, '2019-04-12 08:55:54', NULL, '$2y$10$lGi1rtCjroAC5trNkdIlee2TnK.2t5HAB0FHCNtws.Qtzu0L/kRje', 'vQWH9EexNRoRVSthQsY0pvVOTyLMxgeSsFUTxCc2SLPLlryRX8h9usNvfe9r', '2019-04-12 08:51:51', '2019-04-12 08:55:54'),
(38, 'Angelo B Santos', 'nullvoid023@gmail.com', NULL, NULL, '2019-04-12 08:53:38', NULL, '$2y$10$7iSEATZ/kdrD5G7Iw4ebRe8M.2HvOGWNOtvingdv4Su5AgrgmikIG', 'cjQnxm72BBCKHF9nRVlmRcZwWp7iaroxhoNS8NXnrqcekoWFOr7LNpysJY3m', '2019-04-12 08:51:57', '2019-04-12 08:53:38'),
(39, 'Jeff Sumilang', 'sumilangmarc1999@gmail.com', NULL, NULL, '2019-04-12 08:52:45', NULL, '$2y$10$B1SwGglP9ngLhfaVCcYoB.Bb8jo2hgezryi1qfXk9cH02K4071PTW', 'izENU42kEnagDjmDC1tzHS7ENLqV4FUrT5Zeoy0Xe0K3YFnRvJt9W8f40JbD', '2019-04-12 08:52:00', '2019-04-12 08:52:45'),
(40, 'RONALD FORTES PADAOAN', 'ronaldpadaoan@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$Ky/yoGxK12bRE2qIUEOgouaPbEq0wTv8k3Gc8JBmqrCMxBhSw2rS6', NULL, '2019-04-12 08:52:12', '2019-04-12 08:52:12'),
(41, 'ladylie', 'cladylie@yahoo.com', NULL, NULL, NULL, NULL, '$2y$10$/Md4iVvH7aoE2aHiDwZnEuAkTvfHF4FwbZBqctfHuUTN6r2YX4ta2', 'KsG9JhZvqPP178a3MQNB5Ii5OQ0jIGYeDQjCEGRA0Hm2YLzW6WOCD2I1k2SZ', '2019-04-12 08:52:19', '2019-04-12 08:52:19'),
(42, 'Jericho', 'joieshy23@gmail.com', NULL, NULL, '2019-04-12 08:54:09', NULL, '$2y$10$n5O/ANcgHGJRIGPv9UbXfO4UmxpXfqFtbdSNgBRK9OMYd2cav1Fta', 'NBcbbWXJeuERERDIHMjTNz4GaDCBG9Rl3HU9xgs4euviDbbibKI8p95THdGp', '2019-04-12 08:52:44', '2019-04-12 08:54:09'),
(43, 'Mike Russel Nigoza', 'mikerussel@yahoo.com', NULL, NULL, NULL, NULL, '$2y$10$eTsgxWCAOiea0sZn4r7jbudbK9a0g9hV2BoDC7SHru0usGvIyy00W', 'TaWczeSLapVyQpc6kuOnb8IaqT72VqC4fZ1e4qd1s2iDsXgVEGIBkgiLFtBy', '2019-04-12 08:57:14', '2019-04-12 08:57:14'),
(44, 'allyana may salonga', 'sassiyanangel1995@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$/qbe1EP.rlPknmEiuB1LSeZHXMdL.dTOwm43KkDz6aUVYL4I.oIL6', 'zJhcgv9vKdm77zlW4SI1AyZtuLlC1h52K9BJHhPKJNa9TixTjb80QqsQJad8', '2019-04-12 08:58:54', '2019-04-12 08:58:54'),
(45, 'Shaneen', 'me@marcsumilang.com', NULL, NULL, NULL, NULL, '$2y$10$PmvggB0PM0T5j9UpWp6s1eGj2VHrsLmsMkENE0lo28s.RL.fIVYN2', 'DMr5idVX5GKz2NLKnxnoO77LWzfx8HjZDbGqk5oowIws2Kmq1M1wlpGJG3Ra', '2019-05-21 13:44:43', '2019-05-21 13:44:43'),
(46, 'Marc Sumilang', 'marcjhericosumilang@gmail.com', NULL, NULL, '2019-05-24 18:59:27', NULL, '$2y$10$UalTACVtr0HTlbYrOLqru.WNCRkTWBDg0vvOklJWru04W/OQlcsgC', 'Us5jgE5fkKFVvkVgZkAKfNVI2AMXbpItT9yhFVXx8B7CRDpNmzGUUk77Lk6q', '2019-05-21 14:00:39', '2019-05-24 18:59:27'),
(47, 'FGajzyLQKSMi', 'sowerbyakawatsonhoward@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$tsVguszEn2QpnDSmgmwCiuE7TH7aIgvxi9fWph.B4phfLLCV7cC1i', NULL, '2019-08-01 23:35:57', '2019-08-01 23:35:57'),
(48, 'Charles', 'popeyeyel@gmail.com', NULL, NULL, '2019-08-08 22:33:04', NULL, '$2y$10$.h4oZHyD38/u6hk.Rar38ORaD7ZxatcxTFdnfUDpwYWhZyo12wJP6', 'eh9tYqfKvl5Of12cQsBba8TolXq8tOV0ReDnQad9u7x8FAcVLkCW3oyrTpHe', '2019-08-08 22:30:57', '2019-08-08 22:33:04'),
(49, 'RomanosOG', 'romanilinin88@mail.ru', NULL, NULL, NULL, NULL, '$2y$10$.zgMmC669o3OFqd3oJxXmeSxN9S7Zoeha6BYpmaie7CwYPlCB4Va2', NULL, '2019-08-31 11:22:38', '2019-08-31 11:22:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_role_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college_applications`
--
ALTER TABLE `college_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `date_time_records`
--
ALTER TABLE `date_time_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollment_procedures`
--
ALTER TABLE `enrollment_procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_matrices`
--
ALTER TABLE `grading_matrices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_sections`
--
ALTER TABLE `grading_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_sheets`
--
ALTER TABLE `grading_sheets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_students`
--
ALTER TABLE `grading_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_subject_loads`
--
ALTER TABLE `grading_subject_loads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `instructors_logs`
--
ALTER TABLE `instructors_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `misc_fees`
--
ALTER TABLE `misc_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarships`
--
ALTER TABLE `scholarships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_years`
--
ALTER TABLE `school_years`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `tuit_fees`
--
ALTER TABLE `tuit_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `college_applications`
--
ALTER TABLE `college_applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `date_time_records`
--
ALTER TABLE `date_time_records`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `enrollment_procedures`
--
ALTER TABLE `enrollment_procedures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `grading_matrices`
--
ALTER TABLE `grading_matrices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grading_sections`
--
ALTER TABLE `grading_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grading_sheets`
--
ALTER TABLE `grading_sheets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grading_students`
--
ALTER TABLE `grading_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `grading_subject_loads`
--
ALTER TABLE `grading_subject_loads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `instructors_logs`
--
ALTER TABLE `instructors_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `misc_fees`
--
ALTER TABLE `misc_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `scholarships`
--
ALTER TABLE `scholarships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `school_years`
--
ALTER TABLE `school_years`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tuit_fees`
--
ALTER TABLE `tuit_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
