<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/rooms' , 'Api\Admin\RoomController');
Route::apiResource('/subjects' , 'Api\Admin\SubjectController');

//MIS
Route::apiResource('/sys' , 'Api\MIS\SYSController');
Route::get('/sys-all' , 'Api\MIS\SYSController@sysAll');
Route::get('/verify-users' , 'MIS\VerifyPortalController@getUsers');
Route::get('/user-verified/{id}' , 'MIS\VerifyPortalController@verifyUser');
Route::get('/user-archive/{id}' , 'MIS\VerifyPortalController@archieveUser');


Route::apiResource('/miscfees' , 'Api\Registrar\MiscFeeController');
Route::apiResource('/tuitfees' , 'Api\Registrar\TuitFeeController');
Route::apiResource('/college-application' , 'Api\Registrar\CollegeApplicationController');

//librarian
Route::apiResource('/books' , 'Api\Librarian\BooksController');

Route::post('/student/submit-application' , 'Api\Registrar\CollegeApplicationController@store');
Route::post('/registrar/enrollment-procedure' , 'Registrar\EnrollmentProcedureController@store');
Route::post('/student/requirement' , 'Student\StudentRequirementController@store');
Route::put('/update_requirements/{email}' , 'Api\Registrar\RequirementsController@update');


Route::apiResource('/dtr' , 'Api\VPAF\DTRController');
Route::apiResource('/manage_students' , 'Grading\StudentController');

Route::apiResource('/manage_inventory' , 'InventoryController');
Route::apiResource('/instructors' , 'Api\Instructors\InstructorController');
Route::apiResource('/rooms' , 'Api\VPAF\RoomsController');
Route::apiResource('/course' , 'Api\Registrar\CourseController');
Route::apiResource('/schedules' , 'Api\VPAA\SchedulesController');

Route::get('/instructors/subject_load/{id}' , 'Api\Instructors\InstructorController@get_subject_loads');
Route::get('/schedules/{instructor_id}' , 'Api\Instructors\InstructorController@view_by_instructor');
Route::get('/manage_inventory/{item_name}/{brand_name}/{status}/{description}/{last_location}/{new_location}' , 'InventoryController@filter');

//registrar
Route::post('/registrar/add-student' , 'Registrar\EnrollmentProcedureController@addStudent');


Route::post('/import_excel/import', 'Grading\StudentController@uploadCsv');

//Grading
Route::apiResource('/subject_loads' , 'Grading\SubjectLoadController');


