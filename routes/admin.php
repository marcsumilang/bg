<?php
Route::group([
    'namespace'  => 'App\Http\Controllers',
    'middleware' => 'web',
    'prefix'     => config('multiauth.prefix', 'admin')
], function () {

    //archieves
    Route::get('archieved-subjects', 'ArchievedController@searchSubjectArchieved')->name('admin.archievedSubjects')->middleware('role:vpaa');
    Route::get('archieved-tuitions', 'ArchievedController@searchTuitionArchieved')->name('admin.archievedTuitions')->middleware('role:registrar');
    Route::get('archieved-miscfee', 'ArchievedController@searchMiscFeeArchieved')->name('admin.archievedMiscFee')->middleware('role:registrar');
    
    Route::get('archieved-student-portal', 'ArchievedController@searchStudentPortal')->name('mis.archievedStudentPortal')->middleware('role:mis');

    //MIS
    Route::get('verify-portal', 'MIS\VerifyPortalController@index')->name('mis.verify-portal')->middleware('role:mis');
    Route::get('restore-user/{id}', 'MIS\VerifyPortalController@restore')->middleware('role:mis');

    Route::get('enrollment-procedure', 'Registrar\EnrollmentProcedureController@index')->name('registrar.enrollmentProcedure')->middleware('role:registrar');



    Route::get('super/change-pass', 'PagesController@superAdminChangePass');
    Route::get('change-pass', 'PagesController@adminChangePass');
    Route::post('update-pass', 'PagesController@adminUpdatePass')->name('admin-change-pass');


    Route::get('masterlist', 'MasterlistController@index');
    Route::get('students-id', 'MIS\StudentIdController@index');
    Route::apiResource('download-section', 'MasterlistController');

    //inventory
    Route::get('manage_inventory', 'PagesController@viewInventory')->name('inventory.index')->middleware('role:inventory-custodian');

    //instructors
    Route::get('instructors', 'PagesController@viewInstructors')->middleware('role:vpaf');
    Route::get('instructors/archived', 'Api\Instructors\InstructorController@viewInstructorsArchived')->middleware('role:vpaf');

    //Rooms
    Route::get('rooms', 'PagesController@viewRooms')->middleware('role:vpaf');
    Route::get('rooms/archived', 'Api\VPAF\RoomsController@viewArchived')->middleware('role:vpaf');

    //vpaa
    Route::get('schedules', 'PagesController@viewSchedules')->middleware('role:vpaa');
    Route::get('manage_student', 'PagesController@viewManageStudent')->middleware('role:vpaa');
    Route::get('subject_loads', 'PagesController@viewManageSubjectLoads')->middleware('role:vpaa');
    

    //librarian
    Route::get('books', 'PagesController@viewBooks')->middleware('role:librarian');

    //Registrar    
    Route::get('enrolled_students', 'PagesController@view_enrolled')->middleware('role:registrar');
    Route::get('application_forms', 'PagesController@view_application_form')->middleware('role:registrar');
    Route::get('view_application/{email}', 'PagesController@view_application');
    Route::get('courses', 'PagesController@viewCourses')->middleware('role:registrar');
    Route::get('courses/archived', 'Api\Registrar\CourseController@viewArchived')->middleware('role:registrar');
});



Route::group([
    'namespace'  => 'Bitfumes\Multiauth\Http\Controllers',
    'middleware' => 'web',
    'prefix'     => config('multiauth.prefix', 'admin'),
], function () {
    Route::GET('/home', 'AdminController@index')->name('admin.home');
    Route::GET('/super/dashboard', 'AdminController@super')->name('admin.super');
    // Login and Logout
    Route::GET('/', 'LoginController@showLoginForm')->name('admin.login');
    Route::POST('/', 'LoginController@login');
    Route::POST('/logout', 'LoginController@logout')->name('admin.logout');

    // Password Resets
    Route::POST('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::GET('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::POST('/password/reset', 'ResetPasswordController@reset');
    Route::GET('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');

    // Register Admins
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'RegisterController@register');
    Route::get('/{admin}/edit', 'RegisterController@edit')->name('admin.edit');
    Route::delete('/{admin}', 'RegisterController@destroy')->name('admin.delete');
    Route::patch('/{admin}', 'RegisterController@update')->name('admin.update');

    // Admin Lists
    Route::get('/show', 'AdminController@show')->name('admin.show');

    // Admin Roles
    Route::post('/{admin}/role/{role}', 'AdminRoleController@attach')->name('admin.attach.roles');
    Route::delete('/{admin}/role/{role}', 'AdminRoleController@detach');

    // Roles
    Route::get('/roles', 'RoleController@index')->name('admin.roles');
    Route::get('/role/create', 'RoleController@create')->name('admin.role.create');
    Route::post('/role/store', 'RoleController@store')->name('admin.role.store');
    Route::delete('/role/{role}', 'RoleController@destroy')->name('admin.role.delete');
    Route::get('/role/{role}/edit', 'RoleController@edit')->name('admin.role.edit');
    Route::patch('/role/{role}', 'RoleController@update')->name('admin.role.update');
   

    Route::get('subjects', function () {
        return view('admin.subjects');
    })->name('admin.subjects')->middleware('role:vpaa');
    
    //MIS
    Route::get('school-year', function () {
        return view('mis.sys');
    })->name('mis.sys')->middleware('role:mis');
    
  
    //REGISTRAR
    Route::get('misc-fee', function () {
        return view('registrar.miscfee');
    })->name('registrar.miscfee')->middleware('role:registrar');

    Route::get('tuit-fee', function () {
        return view('registrar.tuitfee');
    })->name('registrar.tuitfee')->middleware('role:registrar');

    Route::get('college-applications', function () {
        return view('registrar.collegeApplications');
    })->name('registrar.collegeApplications')->middleware('role:registrar');
    
    //VPAF
    Route::get('college-applications', function () {
        return view('registrar.collegeApplications');
    })->name('registrar.collegeApplications')->middleware('role:registrar');
   
    

    Route::get('scholarship', 'Scholarship\ScholarshipController@index')->name('scholarship.index')->middleware('role:scholarship');

  

    // Route::get('subjects','SubjectController')->name('admin.subjects')->middleware('role:admin');

    Route::get('/{any}', function () {
        return abort(404);
    });
});
