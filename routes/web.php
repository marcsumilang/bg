<?php
Route::get('google', function () {

    return view('google');

});

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');

Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');


Route::get('/', function () {

    return view('pages.landing');

});

Route::get('/about-developers', function () {

    return view('pages.about-developers');

});
Route::get('/argon', function () {
    return view('pages.argon');
});

Route::get('/send_mail', 'PagesController@sendTestMail');
Route::get('/enroll_again/{email}', 'Api\Registrar\CollegeApplicationController@enrollAgain');

Route::get('/consent', 'AgreementsController@consent');
Route::get('/privacy_policy', 'AgreementsController@privacy_policy');
Route::get('/terms_of_service', 'AgreementsController@terms_of_service');

Route::get('/enrollment/college', 'PagesController@CollegeEnrollment');
Route::get('/enrollment/college/application-form', 'PagesController@CollegeApplication');
Route::get('subjects/{course}/{year}', 'SearchController@searchSubjectCourse');
Route::get('subjects/{course}/{year}/{semester}', 'SearchController@searchSubjectCourseYearSemester');
Route::get('tuitfees/{course}/{year}/{semester}', 'SearchController@searchTuitionCourseYearSemester');
Route::get('miscfee/{level}', 'SearchController@searchMiscFeeLevel');

Route::get('college-applications/{course}/{year}', 'SearchController@searchCollegeApplicationsByCourseYear');

Route::get('college-applications/sys/{semester}/{school_year}', 'SearchController@searchCollegeApplicationsBySYS');

Auth::routes(['verify' => true]);

//Instructor portal
Route::get('/instructors/login', 'Api\Instructors\InstructorController@viewLogin')->name('ins_login');
Route::get('/instructors/logout', 'Api\Instructors\InstructorController@logout')->name('ins_login');
Route::post('/instructors/validate', 'Api\Instructors\InstructorController@validate_login');
Route::get('/instructors/home', 'Api\Instructors\InstructorController@view_home');
Route::get('/instructors/gradesheet/{id}', 'Api\Instructors\InstructorController@gradeSheet');
Route::get('/instructors/my_dtr', 'Api\Instructors\InstructorController@my_dtr');
Route::get('/instructors/students/masterlist', 'Api\Instructors\InstructorController@view_masterlist');
Route::get('/instructors/grading_criteria', 'Api\Instructors\InstructorController@view_criteria');
Route::get('/instructors/class_records', 'Api\Instructors\InstructorController@view_class_records');
Route::get('/instructors/change-pass', 'Api\Instructors\InstructorController@view_change_pass');
Route::post('/instructors/update-pass', 'Api\Instructors\InstructorController@updatePass')->name('update-pass');

//student portal
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/applications', 'Student\StudentCollegeApplicationController@index')->middleware('verified');
Route::get('/mysubjects', 'Student\StudentSubjectController@index')->middleware('verified');
Route::get('/student-info', 'PagesController@studentInfo')->middleware('verified');
Route::get('/oncampus', 'PagesController@oncampus')->middleware('verified');
Route::get('/change-pass', 'PagesController@changePass');
Route::post('/update-pass', 'PagesController@updatePass')->name('update-pass');
Route::get('/requirements', 'Student\StudentRequirementController@index')->middleware('verified');
Route::get('/getmyinfo/{id}', 'Student\StudentSectionController@getMyInfo');
Route::get('/my-section', 'Student\StudentSectionController@index')->middleware('verified');
Route::put('/sectioning/{id}/{section}', 'Student\StudentSectionController@updateSection');
Route::get('/getbatch/{course}/{year}/{section}/{school_year}/{semester}', 'Student\StudentSectionController@viewBatchSections');
Route::get('/search_instructors', 'SearchController@student_searchInstructor');

//main page
Route::get('/miscellaneous-fees', 'PagesController@miscFees')->name('miscFee');
Route::get('/tuition-fees', 'PagesController@tuitFees')->name('tuitFee');
Route::get('/subjects', 'PagesController@subjects')->name('subjects');

Route::get('send_test_email', 'PagesController@sendTestMail');
Route::get('view_pdf', 'PdfController@viewPdf');
Route::get('download_pdf', 'PdfController@downloadPdf');
Route::get('download_student_list', 'Student\StudentSectionController@pdfBatchSections');
Route::get('download_student_list/{course}/{year}/{section}/{school_year}/{semester}', 'Student\StudentSectionController@pdfBatchSections');
Route::get('view/schedules/{name}', 'PdfController@print_instructor_scedule');


Route::get('mmm', 'Api\Registrar\CollegeApplicationController@store');
// Route::get('mmm', function(){
//     return Auth::user();
// });

// Route::get('send_test_email', function(){
// 	try{
// 		$user = 'BPC MIS';
// 		$data = array('name' => 'Marc Jherico Sumilang', 'email' => 'marcjhericosumilang@gmail.com');
// 		Mail::send('Sending emails with Mailgun and Laravel is easy!', function($message)
// 		{
// 			$message->subject('Mailgun and Laravel are awesome!');
// 			$message->from('bulacanpolytechniccollege.org', 'BPC MIS');
// 			$message->to('marcjhericosumilang@gmail.com');
// 		} use($data){

// 		});
// 		return 'EMAIL SENT';
// 	}catch(Exception $e){
// 		return 'Sending Failed';
// 	}
// });


// Route::get('facebook', function () {
//     return view('facebook');
// });
// Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
// Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('auth/{provider}', 'Auth\SocialController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\SocialController@handleProviderCallback');
Route::get('sample_mail', 'PagesController@sendEmail');